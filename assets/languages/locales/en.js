export default {
    login:{
        login:'Login',
        logout:'Logout',
        merchant:'MERCHANT',
        forgot:'Forgot your password?',
        lang:'ENG',
        username:'Username',
        password:'Password'
    },
    home:{
        scan:'SCAN QR',
        scan_info:'Payment Privilege Voucher Reservation Voucher',
        history:'HISTORY',
        history_info:'Click here for view more history',
    },
    ManagerProfile:{
        language:"Language",
        change_password:"Change your password",
        change_branch:"Change Branch",
        work_history:"Work History"
    },
    header:{
        scan_qr:"SCAN QR",
        use_voucher:"USE VOUCHER CODE",
        payment:"PAYMENT",
        history:"HISTORY",
        history_voucher:"HISTORY VOUCHER",
        history_more_date:"SELECT DATE",
        forgot:"FOTGOT PASSWORD",
        profile:"PROFILE",
        language:"LANGUAGE",
        change:"CHANGE PASSWORD",
        change_branch:"CHANGE BRANCH",
        branch_detail:"BRANCH DETAIL",
        work_history:"WORK HISTORY",
        menu_all:"All",
        menu_today:"Today",
        menu_yesterday:"Yesterday",
        menu_more_date:"More date",
        add:"Add",
        done:"Done",
        voucher: "Voucher",
    },
    branch_select:{
        forgotText:"Please select your branch to work",
        done:"DONE"
    },
    change_password:{
        ChangePassword_Text:"For safety your information, we need to confirm your exiting pass word for to be continued",
        existing_password:"Existing password",
        next:"NEXT",
    },
    change_password_success:{
        title_1:"CHANGE PASSWORD SUCCESS",
        detail_1:"We changeed you password",
        detail_2:"You can use the new password now",
        done:"DONE"
    },
    forgot_password:{
        title_1:"Please input your username",
        next:"NEXT"
    },
    forgot_password_success:{
        title_1:"RESET PASSWORD SUCCESS",
        detail_1:"We reset and send new password to your superviser already.",
        detail_2:"Please contact them for get your password",
        next:"NEXT"
    },
    payment:{
        title_1:"AMOUNT OF PAYMENT",
        confirm:"CONFIRM",
        done: "DONE"
    },
    payment_success:{
        title_1:"PAYMENT SUCCESS",
        payment_id:"Payment ID:",
        order_number:"Order Number:",
        point:"Points",
        done:"DONE"
    },
    payment_fail:{
        title_1:"PAYMENT FAILED",
        payment_id:"Payment ID:",
        detail_1:"Sorry Something is wrong",
        retry:"RETRY"
    },
    privilege_success:{
        modal_title1:"Privilege Detail",
        period:"Period : ",
        title1:"PRIVILEGE",
        detail_1:"More Details",
        done:"DONE"
    },
    reservation:{
        modal_model:"Model : ",
        modal_payment_id:"Payment ID : ",
        modal_phone:"Phone : ",
        modal_country:"Country : ",
        modal_passport:"Passport : ",
        modal_flight_number:"Flight Number : ",
        modal_credit_card:"Credit Card : ",
        modal_price_ex_vat:"Price (Exclude VAT)",
        modal_vat:"VAT",
        modal_point_used:"AOT Point Used",
        modal_point:"AOT Points",
        modal_from:"From",
        modal_to:"To",
        modal_booking_date:"Booking Date",
        modal_confirm:"CONFIRM",

        title_1:"RESERVATION",
        modal:"Model : ",
        from:"From",
        to:"To",
        booking_date:"Booking Date",
        more_detail:"More Details",
        confirm:"CONFIRM"
    },
    reservation_success:{
        title_1:"RESERVATION  SUCCESS",
        payment_id:"Payment ID : ",
        modal:"Modal : ",
        points:" Points",
        done:"DONE"
    },
    enter_barcode:{
        detail_1:"Please input voucher code",
        next:"NEXT",
        code: "Code",
    },
    scan_barcode:{
        use_voucher:"Use Voucher Code",
        use_photo:"Select Image",
    },
    roles: {
        role: "Roles",
    },
    date: {
        all: "All",
        today: "Today",
        yesterday: "Yesterday",
        more_date: "More Date",
        select_date: "Select Date",
    },
    scanQRCode: {
        ask: "Do you want to use this code?",
        confirm: "Confirm",
        cancel: "Cancel",
    },
    logout: {
        question: "Do you want to logout",
        ok: "Confirm",
        cancel: "Cancel"
    }
}
