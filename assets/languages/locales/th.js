export default {
    login:{
        login:'เข้าสู่ระบบ',
        logout:'ออกจากระบบ',
        lang:'ไทย',
        merchant:'MERCHANT',
        forgot:'ลืมรหัสผ่าน',
        username:'ชื่อผู้ใช้',
        password:'รหัสผ่าน'
    },
    home:{
        scan:'สแกนคิวอาร์โค้ด',
        scan_info:'คูปองสิทธิพิเศษ',
        history:'ประวัติ',
        history_info:'กดที่นี่เพื่อดูประวัติการใช้งาน',
    },
    ManagerProfile:{
        language:"ภาษา",
        change_password:"เปลี่ยนรหัสผ่าน",
        change_branch:"เปลื่อน Branch",
        work_history:"ประวัติงาน"
    },
    header:{
        scan_qr:"สแกนคิวอาร์โค้ด",
        use_voucher:"ใช้บัตรกำนัน",
        payment:"การชำระเงิน",
        history:"ประวัติ",
        history_voucher:"ประวัติการใช้บัตรกำนัน",
        history_more_date:"เลือกวัน",
        forgot:"ลืมรหัสผ่าน",
        profile:"ข้อมูลส่วนตัว",
        language:"ภาษา",
        change:"เปลี่ยนรหัสผ่าน",
        change_branch:"เปลี่ยนสาขา",
        branch_detail:"ข้อมูลสาขา",
        work_history:"ประวัติการทำงาน",
        menu_all:"ทั้งหมด",
        menu_today:"วันนี้",
        menu_yesterday:"เมื่อวาน",
        menu_more_date:"วันอื่นๆ ",
        add:"เพิ่ม",
        done:"สำเร็จ",
        voucher: "บัตรกำนัล",
    },
    branch_select:{
        forgotText:"เลือกสาขาที่ต้องการใช้งาน",
        done:"เสร็จ"
    },
    change_password:{
        ChangePassword_Text:"เพื่อความปลอดภัยของข้อมูลของคุณเราต้องยืนยันรหัสผ่านที่มีอยู่ของคุณเพื่อให้สามารถดำเนินการต่อได้",
        existing_password:"รหัสผ่านเดิม",
        next:"ต่อไป",
    },
    change_password_success:{
        title_1:"เปลี่ยนรหัสผ่านสำเร็จ",
        detail_1:"ระบบได้ทำการเปลี่ยนรหัสผ่านเรียบร้อยแล้ว",
        detail_2:"คุณสามารถใช้รหัสผ่านใหม่ได้ทันที",
        done:"สำเร็จ"
    },
    forgot_password:{
        title_1:"กรุณาใส่ชื่อผู้ใช้ของคุณ",
        next:"ต่อไป"
    },
    forgot_password_success:{
        title_1:"ตั้งรหัสผ่านใหม่สำเร็จ",
        detail_1:"เรารีเซ็ตและส่งรหัสผ่านใหม่ให้หัวหน้างานของคุณแล้ว",
        detail_2:"กรุณาติดต่อหัวหน้างานเพื่อรับรหัสผ่านของคุณ",
        next:"ต่อไป"
    },
    payment:{
        title_1:"จำนวนเงินที่ต้องชำระ",
        confirm:"ยืนยัน",
        done: "เสร็จ"
    },
    payment_success:{
        title_1:"การชำระเงินสำเร็จ",
        payment_id:"Payment ID:",
        order_number:"Order Number:",
        point:"Points",
        done:"เสร็จ"
    },
    payment_fail:{
        title_1:"การชำระเงินล้มเหลว",
        payment_id:"Payment ID:",
        detail_1:"ขออภัยเกิดข้อผิดพลาด",
        retry:"ลองใหม่อีกครั้ง"
    },
    privilege_success:{
        modal_title1:"รายละเอียดสิทธิพิเศษ",
        period:"ช่วงเวลา : ",
        title1:"สิทธิพิเศษ",
        detail_1:"รายละเอียดเพิ่มเติม",
        done:"เสร็จ"
    },
    reservation:{
        modal_model:"Model : ",
        modal_payment_id:"Payment ID : ",
        modal_phone:"เบอร์โทรศัพท์ : ",
        modal_country:"ประเทศ : ",
        modal_passport:"หนังสือเดินทาง : ",
        modal_flight_number:"หมายเลขเที่ยวบิน : ",
        modal_credit_card:"รหัสบัตรเครดิต : ",
        modal_price_ex_vat:"Price (Exclude VAT)",
        modal_vat:"VAT",
        modal_point_used:"AOT Point Used",
        modal_point:"AOT Points",
        modal_from:"จาก",
        modal_to:"ถึง",
        modal_booking_date:"วันที่จอง",
        modal_confirm:"ยืนยัน",

        title_1:"การจอง",
        modal:"Model : ",
        from:"จาก",
        to:"ถึง",
        booking_date:"วันที่จอง",
        more_detail:"รายละเอียดเพิ่มเติม",
        confirm:"ยืนยัน"
    },
    reservation_success:{
        title_1:"บันทึกข้อมูลสำเร็จ",
        payment_id:"Payment ID : ",
        modal:"Modal : ",
        points:" คะแนน",
        done:"เสร็จ"
    },
    enter_barcode:{
        detail_1:"กรุณาใส่รหัสคูปอง",
        next:"ต่อไป",
        code: "รหัส",
    },
    scan_barcode:{
        use_voucher:"ใช้รหัสคูปอง",
        use_photo:"เลือกรูปภาพ",
    },
    roles: {
        role: "หน้าที่",
    },
    date: {
        all: "ทั้งหมด",
        today: "วันนี้",
        yesterday: "เมื่อวาน",
        more_date: "กำหนดเอง",
        select_date: "เลือกวัน",
    },
    scanQRCode: {
        ask: "คุณต้องการใช่ Code นี้ใช่หรือไม่",
        confirm: "ยืนยัน",
        cancel: "ยกเลิก",
    },
    logout: {
        question: "คุณต้องการออกจากระบบหรือไม่",
        ok: "ออกจากระบบ",
        cancel: "ยกเลิก"
    }
}
