import I18n from 'i18n-js';
import en from './locales/en';
import th from './locales/th';

I18n.defaultLocale = "en";
I18n.locale = "en";

I18n.translations = {
  en,
  th
};

export default I18n;