module.exports = {
    project: {
      ios: {},
      android: {}, // grouped into "project"
    },
    assets: ["./assets/fonts/"], // stays the same
    dependencies: {
        'react-native-qr-decode-image-camera': {
            platforms: {
                android: null,
            },
        },
    },
  };
