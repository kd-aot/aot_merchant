import React, { Component } from "react"
import { Alert, View , Text , Image , TouchableOpacity} from "react-native"
import { Container, Header, Left, Body, Right, Title, Subtitle, Button, Icon, Footer,FooterTab ,Content} from 'native-base'
import { connect } from "react-redux"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../Style/style'
import I18n from '../../assets/languages/i18n';

class UnderlineComponent extends Component {
    constructor(props){
        super(props)
    }
    Log_UserOut(){
        Alert.alert(
            I18n.t('logout.question'),
            "",
            [
                {
                    text: I18n.t('logout.cancel'),
                    style: "cancel"
                },
                { text: I18n.t('logout.ok'), onPress: () => {
                        AsyncStorage.setItem("usertoken","");
                        AsyncStorage.getAllKeys()
                            .then(keys => {
                                console.log(keys)
                                AsyncStorage.multiRemove(keys)
                            });
                        Actions.login({type: "reset"})
                    }}
            ],
            { cancelable: false }
        );
    }
    render() {
        return (
            <View style={{flex:0.1,flexDirection:'row'}}>
                <View style={{flex:0.8,flexDirection:'column-reverse'}}>
                    <Text style={styles.Underline_textAIR}>{this.props.branch_name} </Text>
                    <Text style={styles.Underline_textAL}>{this.props.merchant_name} </Text>
                </View>
                <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                    {this.props.rightbtn &&
                    <TouchableOpacity onPress={()=> this.Log_UserOut()}>
                        <Image source={require('../../assets/images/Underline.png')} style={styles.LogoutButton} />
                    </TouchableOpacity> }
                </View>
            </View>
        )
    }
}

export default UnderlineComponent
