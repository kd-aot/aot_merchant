import React, { Component } from "react"
import { View,Image,Text,TouchableOpacity } from "react-native"
import { Container, Header, Left, Body, Right, Title, Subtitle, Button, Icon} from 'native-base'
import { connect } from "react-redux"
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import I18n from '../../assets/languages/i18n';
import * as actions from "../Actions";
import { Actions } from "react-native-router-flux";
import styles from '../Style/style';
class HeaderComponent extends Component {
    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = () => {
        this._menu.hide();
    };

    showMenu = () => {
        this._menu.show();
    };
    constructor(props){
        super(props)
    }

    onProfile(){
        console.warn('onProfile',this.props.userinfo.is_merchant_hq)
        if(this.props.userinfo.is_merchant_hq == true){
            Actions.Manager_profile()
        }else{
            Actions.Staff_profile()
        }

    }
    render() {
        return (
                <Header transparent>

                {this.props.leftbtn && <Left style={styles.Header_LeftP}>
                    <Button transparent onPress={()=> {
                        if (Actions.currentScene === "privilege_success" || Actions.currentScene === "reservation"|| Actions.currentScene === "payment") {
                            this.props.checkOpenScanQrCode("open");
                        }
                        Actions.pop()
                    }}>
                        <Icon name='arrow-back' style={{color:'#43B7E8'}} />
                    </Button>
                </Left>}
                {this.props.leftbtnProfile && <Left style={styles.Header_LeftP}>
                    <Button transparent onPress={()=> Actions.pop()}>
                        <Icon name='arrow-back' style={{color:'#43B7E8'}} />
                    </Button>
                </Left>}
                {this.props.leftbtnLang && <Left style={styles.Header_LeftP}>
                    <Button transparent onPress={()=> this.onProfile()}>
                        <Icon name='arrow-back' style={{color:'#43B7E8'}} />
                    </Button>
                </Left>}
                {this.props.leftAOTicon && <Left>
                    <View style={styles.Header_AOT_icon_container}>
                    <Image source={require('../../assets/images/headerIcon2.jpg')}
                        style={styles.Header_AOT_icon}  />
                    <Text style={styles.Header_AOT_text}>MERCHANT</Text>
                     </View>
                </Left>}

                {this.props.title &&<Body style={{justifyContent:'center',alignItems:'center',flex:1}}>
                    {this.props.scanQR && <Title style={styles.Header_TitleText}>{I18n.t('header.scan_qr')}</Title>}
                    {this.props.useVoucher1 &&  <Title style={styles.Header_TitleText}>{I18n.t('header.use_voucher')}</Title>}
                    {this.props.payment &&  <Title style={styles.Header_TitleText}>{I18n.t('header.payment')}</Title>}
                    {this.props.history &&  <Title style={styles.Header_TitleText}>{I18n.t('header.history')}</Title>}
                    {this.props.historyVoucher && <Title style={styles.Header_TitleText}>{I18n.t('header.history_voucher')}</Title>}
                    {this.props.historyMoreDT && <Title style={styles.Header_TitleText}>{I18n.t('header.history_more_date')}</Title>}
                    {this.props.forgot &&  <Title style={styles.Header_TitleText}>{I18n.t('header.forgot')}</Title>}
                    {this.props.profile &&  <Title style={styles.Header_TitleText}>{I18n.t('header.profile')}</Title>}
                    {this.props.language &&  <Title style={styles.Header_TitleText}>{I18n.t('header.language')}</Title>}
                    {this.props.change &&  <Title style={styles.Header_TitleText}>{I18n.t('header.change')}</Title>}
                    {this.props.changeBranch &&  <Title style={styles.Header_TitleText}>{I18n.t('header.change_branch')}</Title>}
                    {this.props.branchDetail &&  <Title style={styles.Header_TitleText}>{I18n.t('header.branch_detail')}</Title>}
                    {this.props.workHistory &&  <Title style={styles.Header_TitleText}>{I18n.t('header.work_history')}</Title>}
                </Body>}

                <Right style={styles.Header_RightP}>
                    {this.props.forgothome && <Button transparent onPress={()=> Actions.login()}>
                        <Image source={require('../../assets/images/headerHome.png')}
                         style={styles.Header_AOT_iconRight}  />
                    </Button>}
                    {this.props.home && <Button transparent onPress={()=> Actions.home({type: "reset"})}>
                        <Image source={require('../../assets/images/headerHome.png')}
                         style={styles.Header_AOT_iconRight}  />
                    </Button>}
                    {this.props.done && <Button transparent onPress={()=> this.onProfile()}>
                        <Text style={styles.Header_textdone}>{I18n.t('header.done')}</Text>
                    </Button>}
                    {this.props.Profile &&<Button transparent onPress={()=> this.onProfile()}>
                        <Image source={require('../../assets/images/headerMenIcon.png')}
                         style={styles.Header_AOT_iconRight}  />
                    </Button>}
                    {this.props.Dropdown &&<Button transparent onPress={this.showMenu}>
                    <Image source={require('../../assets/images/dropdown.png')}
                        style={styles.Header_AOT_iconRight}  />
                    </Button>}
                    {this.props.Add &&<Button transparent onPress={()=> Actions.pop()}>
                        <Text style={styles.Header_history_Add}>{I18n.t('header.add')}</Text>
                    </Button>}

                         <Menu ref={this.setMenuRef}>
                            <MenuItem onPress={this.hideMenu}>{I18n.t('header.menu_all')}</MenuItem>
                            <MenuDivider />
                            <MenuItem onPress={this.hideMenu}>{I18n.t('header.menu_today')}</MenuItem>
                            <MenuDivider />
                            <MenuItem onPress={this.hideMenu}>{I18n.t('header.menu_yesterday')}</MenuItem>
                            <MenuDivider />
                            <MenuItem onPress={this.hideMenu}>{I18n.t('header.menu_more_date')}</MenuItem>
                         </Menu>

                </Right>
                </Header>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo:state.user.userinfo
    }
  }
export default connect(mapStateToProps,actions)(HeaderComponent)

