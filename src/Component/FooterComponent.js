import React, { Component } from "react"
import { Dimensions, View, StyleSheet, Text } from "react-native"
import I18n from '../../assets/languages/i18n';
import * as actions from "../Actions";
import { Actions } from "react-native-router-flux";
import {connect} from 'react-redux';
const {width, height} = Dimensions.get('window');

class FooterComponent extends Component {

    render() {
        return(
            <View style={styles.box}>
                <Text style={{fontSize: 12, textAlign: "center", color: "#a6a6a6"}}>Powered by Knowledge Discovery</Text>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps,actions)(FooterComponent)



const styles = StyleSheet.create({
    box: {
        bottom: height < 812 ? height * 0.02 : height * 0.025,
    }
});
