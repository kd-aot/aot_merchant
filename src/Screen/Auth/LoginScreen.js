import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput , Alert , TouchableOpacity ,Image,Dimensions,BackHandler,ScrollView } from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Picker, Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/Octicons';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
// import getToken from "../../Actions/checkAction"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import {MaterialIndicator} from 'react-native-indicators';
import FooterComponent from '../../Component/FooterComponent';

const Dim = Dimensions.get("window")
EyeSize = Dim.width/100*6



class LoginScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:true,
            EyeIcon:"eye-closed",
            languageSelected: I18n.locale,
            flagImage:true,
            // user_email:"test_branch@test.com",
            // user_email:"test_aot_pass@kd.dev",
            // user_password:"Q1w2e3r4",
            user_email:"",
            user_password:"",
            refresh_token: "",
            AppBanner:"",
            waiting: false,
        }
    }
    componentDidMount(){
        I18n.locale === "th" ?
        this.setState({ flagImage:false }) : this.setState({ flagImage:true })
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
          );
        this.importAppBanner()
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log("fail",this.props.loginFail)
        if (this.props.loginFail !== prevProps.loginFail) {
            this.setState({waiting: false});
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
      }
    onChangeLanguage(){
        if (I18n.locale  === "en"){
            console.warn(this.state.languageSelected)
            this.setState({flagImage:false})
            I18n.locale = "th"
        }else {
            console.warn(this.state.languageSelected )
            this.setState({flagImage:true})
            I18n.locale = "en"
        }
    }
    async importAppBanner() {
        await fetch('https://aot-service.staging.kdlab.dev/', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
          },
          body: JSON.stringify({query:`query {  getMerchantAppBanner {    small    large  }}`})
          })
          .then(response => response.json())
          .then(data => this.setState({ AppBanner : data.data.getMerchantAppBanner.small}))
      }
    backAction = () => {
        Alert.alert("Are you sure you want to out?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel"
          },
          { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };
      showPassword(){
        this.setState({show:false})
        this.setState({EyeIcon:"eye"})
        if(this.state.show==false){
            this.setState({show:true});
            this.setState({EyeIcon:"eye-closed"})
        };
    };
    onSubmit(){
        this.setState({waiting: true})
        console.warn("submit")
        var data = {
            user_email : this.state.user_email,
            user_password : this.state.user_password
        }
        console.warn(data)
        this.props.Login2(data)
    }
    render() {
        return (
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                        <ScrollView style={styles.login_container} showsVerticalScrollIndicator={false} >
                        {/* <DropdownLanguage language={languageSelected} onChangeLanguage={this.onChangeLanguage.bind(this)}></DropdownLanguage> */}
                            <View style={{flex:0.05,justifyContent:'flex-end',alignItems:'flex-end'}}>
                                <TouchableOpacity style={styles.LoginLanguageBox} onPress={()=>{this.onChangeLanguage()}}>
                                    <Image source={this.state.flagImage === true ? require('../../../assets/images/flag_UK.png') : require('../../../assets/images/flag_TH.png')}
                                    style={styles.LoginFlag} />
                                    <Text style={I18n.locale === "en" ? styles.LoginLangTextEN : styles.LoginLangTextTH }>{I18n.t('login.lang')}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{height:30}} />
                            <View style={{flex:0.27 ,alignItems:'center',justifyContent:'center'}}>
                                <Image source={require('../../../assets/images/headerIcon2.jpg')}
                                style={styles.LoginAOTLogo} />
                                <Text style={styles.login_text_merchant}>{I18n.t('login.merchant')}</Text>
                            </View>
                            <View style={styles.LoginInput}>
                                <Item rounded style={styles.loginItemMargin}>
                                    <Input style={styles.login_placeholderText} placeholderTextColor='#C4C4C6' placeholder={I18n.t('login.username')} value={this.setState.user_email} onChangeText={(text) => {this.setState({user_email:text})}} />
                                </Item>
                                <Item rounded style={styles.loginItemMargin}>
                                    <Input style={styles.login_placeholderText} placeholderTextColor='#C4C4C6' placeholder={I18n.t('login.password')} value={this.setState.user_password} onChangeText={(text) => {this.setState({user_password:text})}} secureTextEntry={this.state.show} />
                                    <TouchableOpacity onPress={()=>{this.showPassword();}} style={{flex:0.15}}>
                                        <Icon name={this.state.EyeIcon} size={EyeSize} />
                                    </TouchableOpacity>
                                </Item>
                            </View>
                            {
                                this.state.user_email && this.state.user_password
                                ?   <View style={styles.login_loginBtn}>
                                        {
                                            this.state.waiting
                                                ?   <View style={{padding:5,width:'80%',height:'100%' }}>
                                                    <View style={styles.login_botton}>
                                                        <MaterialIndicator color='white' count={5} size={25}/>
                                                    </View>
                                                </View>
                                                :   <TouchableOpacity style={{padding:5,width:'80%',height:'100%' }} onPress={()=> this.onSubmit()} >
                                                    <View style={styles.login_botton}>
                                                        <Text style={I18n.locale === "en" ? styles.textButtonEN : styles.textButtonTH}>{I18n.t('login.login')}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                        }
                                        <TouchableOpacity  onPress={()=>  Actions.forgot_password()} >
                                            <Text  style={I18n.locale === "en" ? styles.login_forgotEN : styles.login_forgotTH}>{I18n.t('login.forgot')}</Text>
                                        </TouchableOpacity>
                                    </View>
                                :   <View style={styles.login_loginBtn}>
                                        <View style={{padding:5,width:'80%',height:'100%' }}>
                                            <View style={[styles.login_botton, {backgroundColor: "#C8CBCD"}]}>
                                                <Text style={I18n.locale === "en" ? styles.textButtonEN : styles.textButtonTH}>{I18n.t('login.login')}</Text>
                                            </View>
                                        </View>
                                        <TouchableOpacity  onPress={()=>  Actions.forgot_password()} >
                                            <Text  style={I18n.locale === "en" ? styles.login_forgotEN : styles.login_forgotTH}>{I18n.t('login.forgot')}</Text>
                                        </TouchableOpacity>
                                    </View>
                            }
                            {/* <View style={{flex:0.3,zIndex:-1}} /> */}
                            <View style={styles.login_ads_banner_container}>
                                <Image source={{uri:this.state.AppBanner}} style={styles.login_ads_banner} />
                            </View>
                        </ScrollView>
                    <FooterComponent/>
                </LinearGradient>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        loginFail: state.user.loginFail,
    }
  }
export default connect(mapStateToProps,actions)(LoginScreen)
