import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form,Icon} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';

import AsyncStorage from '@react-native-community/async-storage'
import FooterComponent from '../../Component/FooterComponent';
class BranchSelectScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            checked:false,
            branch_list:[],
            branch_id:''
        }
        // I18n.locale = 'th'
    }

    componentDidMount(){
        this.props.getBranchAll()
    }
    static getDerivedStateFromProps(props, state) {
        if (props.branch_list) {
          return {
            branch_list: props.branch_list,
            // avatarSource: props.userinfo.user_avatar
          };
        }
        return null;
      }
    selectBranch(item){
        this.setState({branch_id: item.id})
        // if(this.state.checked===true){
        //     Actions.home()
        // }
    }
    async BranchSelect(){
        console.warn('branch_id', this.state.branch_id)
        var branch_id = this.state.branch_id
        await AsyncStorage.setItem('branch_id',branch_id.toString())
        Actions.home()
    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftAOTicon={true} />
                    <View style={styles.home_container}>
                    <View style={{flex:0.9}}>
                        <Text style={styles.forgotText}>{I18n.t('branch_select.forgotText')}</Text>
                            <View style={styles.selectBranchBoxContainer}>
                                {
                                    this.state.branch_list.map((item,index)=>{
                                        console.warn('branch_id check',item.id, this.state.branch_id)
                                        return(
                                            <TouchableOpacity key={index} style={styles.selectBranchBox} onPress={()=>this.selectBranch(item)}>
                                                <View style={styles.selectBranchBox2}>
                                                    <Text style={styles.forgot_BranchBoxText}>{item.name}</Text>
                                                    {item.id == this.state.branch_id &&<View style={styles.selectIconBox}>
                                                        <Icon name={'md-checkmark'} style={{color:'black'}} />
                                                    </View>}
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }


                            </View>
                            <View style={styles.selectBottomView}>
                                <TouchableOpacity style={styles.history_calendarbtn} onPress={()=>this.BranchSelect()}>
                                    <Text style={styles.history_calendarbtnText}>{I18n.t('branch_select.done')}</Text>
                                </TouchableOpacity>
                            </View>
                    </View>
                    <View style={{flex:0.1,flexDirection:'row'}}>
                        <View style={styles.BranchSelectFooter}>
                            <Text style={styles.Underline_textAL}>AOT LIMOUSINE</Text>
                        </View>
                    </View>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        branch_list:state.branch.branch_list
    }
  }
export default connect(mapStateToProps,actions)(BranchSelectScreen)
