import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions,FlatList} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

class ManagerHistoryScreen extends Component {
    constructor(props){
        super(props);
        // I18n.locale = 'th'
    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} Dropdown={true} workHistory={true}  />
                    <View style={{flex:1}}>
                        <View style={{flex:0.9,alignItems:'center'}}>
                            <FlatList
                                data={[1]}
                                ref={(ref) => (this.FlatListRef = ref)}
                                renderItem={({ item }) => {
                                    return (
                                    <View style={styles.workHistoryBox}>
                                        <View style={styles.workHistoryBox2}>
                                        <Text style={styles.workHistoryTextName}>Ephraim Weaver</Text>
                                        <Text style={styles.workHistoryTextStatus}>Login</Text>
                                        </View>
                                        <View style={styles.workHistoryBox2}>
                                        <Text style={styles.workHistoryText2}>Suvarnabhumi Airport</Text>
                                        <Text style={styles.workHistoryText2}>14:12:59</Text>
                                        </View>
                                        <View style={styles.workHistoryWhiteSpace} />
                                    </View>
                                    );
                                }}
                                />
                        </View>
                        <View style={styles.ProfileFooterH}>
                            <View style={styles.ProfileFooter2}>
                                <Text style={styles.Underline_textAL}>AOT LIMOUSINE</Text>
                                <Text style={styles.Underline_textAIR}>AIRPORT</Text>
                            </View>
                            <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                            </View>
                        </View>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(ManagerHistoryScreen)
