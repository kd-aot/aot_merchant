import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions,ScrollView} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from "@react-native-community/async-storage";
import FooterComponent from '../../Component/FooterComponent';


const Dim = Dimensions.get("window")
Size = Dim.width/100*7

class ChangeBranchScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            check:'check',
            branch_id:''
        }
    }
    async componentDidMount(){
        var branch_id = await AsyncStorage.getItem('branch_id')
        this.setState({branch_id:branch_id})
    }

    async onSelectBranch(item){
        var id = item.id
        AsyncStorage.setItem('branch_id',id.toString())
        this.setState({branch_id: item.id})

        // ควร call api เพื่อ update branch id ที่ userinfo
    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent done={true} changeBranch={true} title={true} leftbtn={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9,alignItems:'center',paddingTop:35}}>
                            <ScrollView>
                                {
                                    this.props.branch_list.map((item,index)=>{
                                        return(
                                            <TouchableOpacity key={index} style={styles.CBranchBox} onPress={()=> this.onSelectBranch(item)}>
                                                <View style={styles.CBranchBox1}>
                                                    <View style={styles.CBranchBox11}>
                                                        <Text style={styles.CBtext}>{item.name}</Text>
                                                    </View>
                                                    <TouchableOpacity style={styles.LoginLanguageBox}  onPress={()=> Actions.BranchDetail({data:item})}>
                                                        <Text style={styles.LoginLangTextEN}>More Detail</Text>
                                                    </TouchableOpacity>
                                                </View>
                                                <View style={styles.CBranchBox2}>
                                                    {this.state.branch_id == item.id && <View style={styles.CBcheckbox}>
                                                        <Icon name={this.state.check} size={Size} />
                                                    </View>}
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }

                            </ScrollView>
                        </View>
                        <View style={styles.ProfileFooter}>
                            <View style={styles.ProfileFooter2}>
                                <Text style={styles.Underline_textAL}>AOT LIMOUSINE</Text>
                                <Text style={styles.Underline_textAIR}>AIRPORT</Text>
                            </View>
                            <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                            </View>
                        </View>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        branch_list:state.branch.branch_list
    }
  }
export default connect(mapStateToProps,actions)(ChangeBranchScreen)
