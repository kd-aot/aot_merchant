import React, { Component } from "react"

import { View , Text , TouchableOpacity ,Image,Modal,ScrollView,Dimensions } from "react-native"
import { Container, Header, Content, Picker, Form , Item , Input} from "native-base";
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
const Dim = Dimensions.get("window")
Size = Dim.width/100*7

class LanguageChangeScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            checkEN:'check',
            checkTH:''
        }
    }
    componentDidMount(){
        console.warn(I18n.locale)
        I18n.locale === "th" ?
        this.setState({ checkTH:'check',checkEN:'' }) : this.setState({ checkEN:'check',checkTH:'' })
    }
    changeLanguageTH(){
        this.setState({ checkTH:'check',checkEN:'' })
        I18n.locale = "th"
    }
    changeLanguageEN(){
        this.setState({ checkEN:'check',checkTH:'' })
        I18n.locale = "en"
    }
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} title={true} language={true} leftbtnLang={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9,alignItems:'center'}}>
                            <View style={styles.profileBigboxStaff}>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=>this.changeLanguageEN()} >
                                    <View style={styles.profileMiniBox}>
                                        <Text style={styles.profileTextLanguage}>English</Text>
                                    </View>
                                    <View style={styles.profileMiniBox}>
                                        <Text style={styles.profileTextMenu2}>English</Text>
                                        <View style={styles.profileLanguageBox}>
                                            <Icon name={this.state.checkEN} size={Size} />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=>this.changeLanguageTH()} >
                                    <View style={styles.profileMiniBox}>
                                        <Text style={styles.profileTextLanguage}>ไทย</Text>
                                    </View>
                                    <View style={styles.profileMiniBox}>
                                        <Text style={styles.profileTextMenu2}>Thai</Text>
                                        <View style={styles.profileLanguageBox}>
                                            <Icon name={this.state.checkTH} size={Size} />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <UnderlineComponent />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(LanguageChangeScreen)

