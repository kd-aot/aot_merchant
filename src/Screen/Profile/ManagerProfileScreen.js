import React, { Component } from "react"

import { View , Text , TouchableOpacity ,Image,Modal,ScrollView } from "react-native"
import { Container, Header, Content, Icon, Picker, Form , Item , Input} from "native-base";
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import FooterComponent from '../../Component/FooterComponent';

class MannegerProfileScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            lang:'',
            //API
            user_firstname:'',
            user_middlename:'',
            user_lastname:'',
            data:null,
            avatarSource:''
        }
        // I18n.locale = 'th'
    }
    componentDidMount(){
        console.warn(I18n.locale)
        I18n.locale === "th" ?
        this.setState({ lang:'Thai' }) : this.setState({ lang:'English' })
        // this.props.getuserinfo()
    }
    Log_UserOut(){
        this.props.Log_UserOut()
    }
    static getDerivedStateFromProps(props, state) {
        if (props.userinfo) {
          return {
            data: props.userinfo,
            // avatarSource: props.userinfo.user_avatar
          };
        }
        return null;
      }

      componentWillUpdate(){

      }

    changeImage(){
        const options = {
            title: 'Select Avatar',
            // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
            maxWidth: 900,
            maxHeight: 900,
          };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
              console.log('User cancelled image picker');
            } else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            } else {
            //   const source = { uri: response.uri };
              this.props.change_imgProfile(response.data)
              // You can also display the image using data:
              // const source = { uri: 'data:image/jpeg;base64,' + response.data };

            //   this.setState({
            //     avatarSource: source,
            //   });
            }
          });
    }
    render() {
        console.warn('data getDerivedStateFromProps', this.state.data)
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} title={true} profile={true} leftbtnProfile={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9,alignItems:'center'}}>
                            <View style={styles.ProfileBox}>
                                {/* <Image source={require('../../../assets/images/profilePic.png')} style={styles.ProfileImage} /> */}
                                <Image source={{uri:this.state.avatarSource}} style={styles.ProfileImage} />
                                <TouchableOpacity style={styles.ProfileBox2} onPress={()=> this.changeImage()}>
                                    <Image source={require('../../../assets/images/Pen*.png')} style={styles.ProfileSubImage} />
                                </TouchableOpacity>
                            </View>
                                <Text style={styles.profileName}>{this.state.data.user_firstname+" "+this.state.data.user_lastname}</Text>
                                <Text style={styles.profileRole}>{I18n.t('roles.role')}</Text>
                                <Text style={styles.profile_branchNshop}>{this.state.data.merchant_name+" "+this.state.data.branch_name}</Text>
                            <View style={styles.profileBigbox}>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=> Actions.ChangeBranchScreen()}>
                                    <View style={styles.profileMiniBox}>
                                        <View style={styles.ProfileIconbox}>
                                        <Image source={require('../../../assets/images/icon_ChangeBranch.png')} style={styles.ProfileIcon} />
                                        </View>

                                        <Text style={styles.profileTextMenu}>{I18n.t('ManagerProfile.change_branch')}</Text>
                                    </View>
                                    <Icon name='arrow-back' style={styles.arrow} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=> Actions.ManagerHistoryScreen()}>
                                    <View style={styles.profileMiniBox}>

                                        <View style={styles.ProfileIconbox}>
                                        <Image source={require('../../../assets/images/icon_WorkHistory.png')} style={styles.ProfileIcon3} />
                                        </View>

                                        <Text style={styles.profileTextMenu}>{I18n.t('ManagerProfile.work_history')}</Text>
                                    </View>
                                    <Icon name='arrow-back'  style={styles.arrow} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=> Actions.ChangePasswordScreen()}>
                                    <View style={styles.profileMiniBox}>

                                        <View style={styles.ProfileIconbox}>
                                        <Image source={require('../../../assets/images/Icon_ChangeYourPassword.png')} style={styles.ProfileIcon2} />
                                        </View>

                                        <Text style={styles.profileTextMenu}>{I18n.t('ManagerProfile.change_password')}</Text>
                                    </View>
                                    <Icon name='arrow-back' style={styles.arrow} />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.profileBox2} onPress={()=> Actions.LanguageChangeScreen()}>
                                    <View style={styles.profileMiniBox}>
                                        <View style={styles.ProfileIconbox}>
                                        <Image source={require('../../../assets/images/Icon_Language.png')} style={styles.ProfileIcon3} />
                                        </View>
                                        <Text style={styles.profileTextMenu}>{I18n.t('ManagerProfile.language')}</Text>
                                    </View>
                                    <View style={styles.profileMiniBox}>
                                        <Text style={styles.profileTextLanguage}>{this.state.lang}</Text>
                                        <Icon name='arrow-back' style={styles.arrow} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={styles.history_logoutbtnRed} onPress={()=> this.Log_UserOut()} >
                                <Text style={styles.history_calendarbtnText}>{I18n.t("login.logout")}</Text>
                            </TouchableOpacity>
                        </View>
                        <UnderlineComponent />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo:state.user.userinfo
    }
  }
export default connect(mapStateToProps,actions)(MannegerProfileScreen)

