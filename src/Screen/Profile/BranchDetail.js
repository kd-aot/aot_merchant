import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions, ScrollView} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import { SUB_FONT } from '../../Style/font'
import FooterComponent from '../../Component/FooterComponent';
const Dim = Dimensions.get('window')
class BranchDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            data_detail:[]
        }
    }
    componentDidMount(){
        console.warn('BranchDetail',this.props.data)
        this.props.getBranchDetail(this.props.data.id)
    }
    static getDerivedStateFromProps(props, state) {
        if (props.branch_detail) {
          return {
            data_detail: props.branch_detail,
            // avatarSource: props.userinfo.user_avatar
          };
        }
        return null;
      }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} branchDetail={true} title={true} leftbtn={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0,alignItems:'center'}}>
                            <View style={styles.branchDetail_Box}>
                                <Text style={styles.branchDetail_Text1}>AOT LIMOUSINE</Text>
                                <Text style={styles.branchDetail_Text2}>{this.props.data.name}</Text>
                            </View>
                        </View>
                        <ScrollView style={{flex:1,marginTop:10}}>
                            {
                                this.state.data_detail.map((item,index)=>{
                                    return(
                                        <View key={index} style={{flexDirection:'row',padding:Dim.width/100*3,backgroundColor:'#ffffff',borderRadius:Dim.width/100*3}}>
                                            <View style={{flex:2,padding:Dim.width/100*3}}>
                                                <Image source={{uri:item.user_avatar}} style={[styles.ProfileImage,{width:Dim.width>600?Dim.width/100*6 : Dim.width/100*15,height:Dim.width>600?Dim.width/100*6 :Dim.width/100*15,}]} />
                                            </View>
                                            <View style={{flex:4,padding:Dim.width/100*3,justifyContent:'center'}}>
                                                <Text style={{fontFamily:SUB_FONT,fontSize:Dim.width/380*16,fontWeight:'400',color:'#333333'}}>{`${item.user_firstname} ${item.user_lastname}`}</Text>
                                                <Text style={{fontFamily:SUB_FONT,fontSize:Dim.width/380*14,fontWeight:'400',color:'#dddddd'}}>position</Text>
                                            </View>
                                            <View style={{flex:4,padding:Dim.width/100*3,justifyContent:'center'}}>
                                                <Text style={{fontFamily:SUB_FONT,fontSize:Dim.width/380*16,fontWeight:'400',color:'#333333'}}>{`${item.user_phone}`}</Text>
                                                <Text style={{fontFamily:SUB_FONT,fontSize:Dim.width/380*14,fontWeight:'400',color:'#dddddd'}}>{`${item.user_email}`}</Text>
                                            </View>
                                        </View>
                                    )
                                })
                            }
                        </ScrollView>
                        <UnderlineComponent />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        branch_detail:state.branch.branch_detail
    }
  }
export default connect(mapStateToProps,actions)(BranchDetail)
