import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

class ForgotPasswordSuccessScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
        }
        // I18n.locale = 'th'
    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent forgothome={true} leftbtn={true} title={true} forgot={true} />

                    <View style={styles.forgot_SuccessBox}>
                        <Text style={styles.PaymentS_TextpaymentSuccess}>{I18n.t('forgot_password_success.title_1')}</Text>
                        <Image source={require('../../../assets/images/ForgotSuccess.png')}    style={styles.PasswordForgotSS} />
                        <Text style={styles.forgot_SSText2}>
                        {I18n.t('forgot_password_success.detail_1')}
                            <Text style={styles.forgot_SSText3}>
                                {I18n.t('forgot_password_success.detail_2')}
                            </Text>
                        </Text>
                        <TouchableOpacity style={styles.history_calendarbtn} onPress={()=> Actions.login()}>
                            <Text style={styles.history_calendarbtnText}>{I18n.t('forgot_password_success.next')}</Text>
                        </TouchableOpacity>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(ForgotPasswordSuccessScreen)
