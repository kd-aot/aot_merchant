import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,ScrollView} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

class ForgotPasswordScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            user_name:''
        }
        // I18n.locale = 'th'
    }
    onSubmit(){
        console.warn("submit")
        var data = {
            user_name : this.state.user_name
        }
        console.warn(data)
        this.props.Forgot_Password(data)
    }
    // Actions.forgot_password_success()
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent forgothome={true} leftbtn={true} title={true} forgot={true} />
                    <ScrollView style={{flex:1}}>
                        <View style={styles.forgetPasswordbox}>
                            <Text style={styles.forgotText}>{I18n.t('forgot_password.title_1')}</Text>
                            <Form style={{alignItems:'center'}}>
                            <View style={{width:'100%'}}>
                                <Item rounded style={styles.Password_InputBox}>
                                    <Input style={styles.ChangePassword_PlaceHolderText} value={this.setState.user_name} onChangeText={(text) => {this.setState({user_name:text})}}  placeholderTextColor='#C4C4C6' placeholder='Username' />
                                </Item>
                            </View>
                                {
                                    this.state.user_name
                                    ?   <TouchableOpacity style={styles.history_calendarbtn} onPress={()=> this.onSubmit()} >
                                            <Text style={styles.history_calendarbtnText}>{I18n.t('forgot_password.next')}</Text>
                                        </TouchableOpacity>
                                    :   <View style={[styles.history_calendarbtn, {backgroundColor: "#C8CBCD"}]}>
                                            <Text style={styles.history_calendarbtnText}>{I18n.t('forgot_password.next')}</Text>
                                        </View>
                                }
                            </Form>
                        </View>
                    </ScrollView>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(ForgotPasswordScreen)
