import React, {Component} from 'react';

import {View, Text, TouchableOpacity, Image, Modal, ScrollView, Dimensions} from 'react-native';
import {Container, Header, Content, Icon, Picker, Form, Item, Input} from 'native-base';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import UnderlineComponent from '../../Component/UnderlineComponent';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import {SUB_FONT} from '../../Style/font';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';
const Dim = Dimensions.get("window");

class ReservationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            branch_id: null,
        };
    }

    onSubmit() {
        var voucher_id = this.props.voucher.payment_id;
        var branch_id =  this.state.branch_id ? this.state.branch_id : this.props.userinfo.branch_id;
        this.props.useMerchantVoucherReservation(this.props.code, branch_id);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.voucher) {
            return {
                voucher: props.voucher,
            };
        }
        return null;
    }

    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
    }

    render() {
        var used_at_date = moment.utc(this.props.voucher.booking_date).format('DD/MM/YYYY');
        var used_at_time = moment.utc(this.props.voucher.booking_date).format('HH:mm:ss');
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true} leftbtn={true}/>
                    <ScrollView style={[styles.home_container]}>
                        <Modal
                            transparent={true}
                            visible={this.state.show}
                        >
                            {/* <View style={styles.Privilege_modalContainer}>
                                <View style={styles.home_container}>
                                    <TouchableOpacity onPress={()=>{this.setState({show:false})}} style={{height:'10%',alignItems:'flex-end',justifyContent:'flex-end'}}>
                                        <Image source={require('../../../assets/images/closeModal.png')}
                                        style={styles.Privilege_modalClose} />
                                    </TouchableOpacity>
                                    <View style={styles.Privilege_modalBox}>
                                            <View style={styles.reservation_modalScrollContainer}>
                                            <ScrollView showsVerticalScrollIndicator={false}>
                                                <Text style={styles.reservation_modalTextH1}>voucherName </Text>
                                                <Text style={styles.reservation_modalTextH2}>Model : nameOfmodel </Text>
                                                <Text style={styles.reservation_modalTextH1big}>first_name  last_name}</Text>
                                                <Text style={styles.reservation_modalTextWhite}>Payment ID : payment Id </Text>
                                                <Text style={styles.reservation_modalTextWhite}>booking bookingtime </Text>
                                                <Text style={styles.reservation_modalText}>Phone : phonenumber </Text>
                                                <Text style={styles.reservation_modalText}>Country : country</Text>
                                                <Text style={styles.reservation_modalText}>Passport : passport</Text>
                                                <Text style={styles.reservation_modalText}>Flight Number :flight</Text>

                                                <View style={styles.reservation_modalliner} />

                                                <View style={styles.reservation_modalDetail}>
                                                    <View style={styles.reservation_modalDetailSub1}>
                                                        <Text style={styles.reservation_modalTextWhite}>Credit Card : Credit card number </Text>
                                                        <Text style={styles.reservation_modalTextS26}>money.00 THB </Text>
                                                    </View>
                                                    <View style={styles.reservation_modalDetailSub2}>
                                                        <View style={styles.reservation_modalDetailSub2line}>
                                                            <Text style={styles.reservation_modalTextWhite}>Price (Exclude VAT)</Text>
                                                            <Text style={styles.reservation_modalTextBold}>price THB</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalDetailSub2line}>
                                                            <Text style={styles.reservation_modalTextWhite}>VAT</Text>
                                                            <Text style={styles.reservation_modalTextBold}>money THB</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalDetailSub2line2}>
                                                            <Text style={styles.reservation_modalTextWhite16SB}>AOT Point Used</Text>
                                                            <Text style={styles.reservation_modalTextBold18}>point AOT Points</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalDetail2}>
                                                            <View style={styles.reservation_modalDetail2sub1}>
                                                                <Text style={styles.reservation_textH1}>From</Text>
                                                                <Text style={styles.reservation_textH2}>sometime</Text>
                                                            </View>
                                                            <View style={styles.reservation_modalDetail2sub1}>
                                                                <Text style={styles.reservation_textH1}>To</Text>
                                                                <Text style={styles.reservation_textH2}>sometime}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    <View style={styles.reservation_modalliner} />

                                                    <View style={styles.reservation_DT}>
                                                        <View style={{height:'30%',flexDirection:'row',alignItems:'flex-end',justifyContent:'space-around'}}>
                                                            <Text style={styles.reservation_text3}>Booking Date</Text>
                                                            <Text style={styles.reservation_text3}>somedate</Text>
                                                        </View>
                                                        <View style={{alignItems:'center'}}>
                                                            <Text style={styles.reservation_textTime}>bookinghour</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.reservation_modalConfirmBtn}>
                                                        <TouchableOpacity style={styles.reservation_DoneButton} onPress={()=> {this.onSubmit();{this.setState({show:false})}} } >
                                                                <Text style={styles.textButton}>CONFIRM</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </ScrollView>
                                        </View>
                                    </View>
                                </View>
                            </View> */}

                            <View style={[styles.Privilege_modalContainer, {paddingTop: Dim.height * 0.1}]}>
                                <View style={styles.home_container}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({show: false});
                                    }} style={{height: Dim.height * 0.03, width: Dim.width * 0.1, alignItems: 'center', justifyContent: 'center', alignSelf: 'flex-end'}}>
                                        <Image source={require('../../../assets/images/closeModal.png')}
                                               style={styles.Privilege_modalClose}/>
                                    </TouchableOpacity>
                                    <View style={styles.Privilege_modalBox}>
                                        <View style={styles.reservation_modalScrollContainer}>
                                            <ScrollView showsVerticalScrollIndicator={false}>
                                                <Text
                                                    style={styles.reservation_modalTextH1}>{this.props.voucher.name} </Text>
                                                <Text style={styles.reservation_modalTextH2}>Model
                                                    : {this.props.voucher.model} </Text>
                                                <Text
                                                    style={styles.reservation_modalTextH1big}>{this.props.voucher.first_name + ' ' + this.props.voucher.last_name}</Text>
                                                <Text style={styles.reservation_modalTextWhite}>Payment ID
                                                    : {this.props.voucher.payment_id} </Text>
                                                <Text
                                                    style={styles.reservation_modalTextWhite}>{used_at_date + ' ' + used_at_time} </Text>
                                                <Text style={styles.reservation_modalText}>Phone
                                                    : {this.props.voucher.phone} </Text>
                                                <Text style={styles.reservation_modalText}>Country
                                                    : {this.props.voucher.country}</Text>
                                                <Text style={styles.reservation_modalText}>Passport
                                                    : {this.props.voucher.passport}</Text>
                                                <Text style={styles.reservation_modalText}>Flight Number
                                                    : {this.props.voucher.flight_number}</Text>
                                                <View style={styles.reservation_modalliner}/>
                                                <View style={styles.reservation_modalDetail}>
                                                    <View style={styles.reservation_modalDetailSub1}>
                                                        <Text style={styles.reservation_modalTextWhite}>Credit Card
                                                            : {this.props.voucher.credit_card} </Text>
                                                        <Text
                                                            style={styles.reservation_modalTextS26}>{this.props.voucher.total}
                                                            THB </Text>
                                                    </View>
                                                    <View style={styles.reservation_modalDetailSub2}>
                                                        <View style={styles.reservation_modalDetailSub2line}>
                                                            <Text style={styles.reservation_modalTextWhite}>Price
                                                                (Exclude VAT)</Text>
                                                            <Text
                                                                style={styles.reservation_modalTextBold}>{this.props.voucher.amount} THB</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalDetailSub2line}>
                                                            <Text style={styles.reservation_modalTextWhite}>VAT</Text>
                                                            <Text
                                                                style={styles.reservation_modalTextBold}>{this.props.voucher.vat} THB</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalDetailSub2line2}>
                                                            <Text style={styles.reservation_modalTextWhite16SB}>AOT
                                                                Point Used</Text>
                                                            <Text
                                                                style={styles.reservation_modalTextBold18}>{this.props.voucher.point_used} AOT
                                                                Points</Text>
                                                        </View>
                                                        <View style={styles.reservation_modalliner}/>
                                                        <View>
                                                            <View style={{flexDirection: "row", marginTop: 10}}>
                                                                <Text style={{
                                                                    fontSize:Dim.width/380*16,
                                                                    fontFamily:SUB_FONT,
                                                                    color:'#4A4A4A',
                                                                    width: Dim.width * 0.2,
                                                                }}>From</Text>
                                                                <Text style={{
                                                                    fontSize:Dim.width/380*22,
                                                                    width:Dim.width * 0.5,
                                                                    fontFamily:SUB_FONT,
                                                                    color:'#4A4A4A',
                                                                    fontWeight:'bold'
                                                                }}>{this.props.voucher.from}</Text>
                                                            </View>
                                                            <View style={{flexDirection: "row", marginTop: 10,}}>
                                                                <Text style={{
                                                                    fontSize:Dim.width/380*16,
                                                                    fontFamily:SUB_FONT,
                                                                    color:'#4A4A4A',
                                                                    width: Dim.width * 0.2,
                                                                }}>To</Text>
                                                                <Text
                                                                    style={{
                                                                        fontSize:Dim.width/380*22,
                                                                        width:Dim.width * 0.5,
                                                                        fontFamily:SUB_FONT,
                                                                        color:'#4A4A4A',
                                                                        fontWeight:'bold',
                                                                    }}>{this.props.voucher.to}</Text>
                                                            </View>

                                                        </View>
                                                        {/*<View style={[styles.reservation_modalDetail2, {marginTop: 10}]}>*/}
                                                        {/*    <View style={styles.reservation_modalDetail2sub1}>*/}
                                                        {/*        <Text style={styles.reservation_textH1}>From</Text>*/}
                                                        {/*        <Text*/}
                                                        {/*            style={styles.reservation_textH2_model}>{this.props.voucher.from}</Text>*/}
                                                        {/*    </View>*/}
                                                        {/*    <View style={[styles.reservation_modalDetail2sub1, {borderWidth: 1}]}>*/}
                                                        {/*        <Text style={styles.reservation_textH1}>To</Text>*/}
                                                        {/*        <Text*/}
                                                        {/*            style={styles.reservation_textH2_model}>{this.props.voucher.to}</Text>*/}
                                                        {/*    </View>*/}
                                                        {/*</View>*/}
                                                    </View>

                                                    <View style={[styles.reservation_DT, {marginTop: 20, padding: 20,}]}>
                                                        <View style={{
                                                            // height: '30%',
                                                            flexDirection: 'row',
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                        }}>
                                                            <Text style={styles.reservation_text3}>Booking Date</Text>
                                                            <Text style={styles.reservation_text3}>{used_at_date}</Text>
                                                        </View>
                                                        <View style={{alignItems: 'center'}}>
                                                            <Text
                                                                style={styles.reservation_textTime}>{used_at_time}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.reservation_modalConfirmBtn}>
                                                        <TouchableOpacity style={styles.reservation_DoneButton}
                                                                          onPress={() => {
                                                                              this.onSubmit();
                                                                              {
                                                                                  this.setState({show: false});
                                                                              }
                                                                          }}>
                                                            <Text style={styles.textButton}>CONFIRM</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </ScrollView>
                                        </View>
                                    </View>
                                </View>
                            </View>

                        </Modal>
                        <Text style={styles.PaymentS_TextpaymentSuccess}>RESERVATION</Text>
                        <View style={[styles.reservation_main_box]}>
                            <View style={{
                                // flex: 1,
                                backgroundColor: 'white',
                                borderRadius: 20,
                                padding: 20,
                                // justifyContent: 'center',
                            }}>

                                {/* <View style={styles.Privilege_box}>
                                    <Text style={styles.enterBarcode_text}>first_name last_name</Text>
                                    <Text style={styles.reservation_textModal}>Model : Model name</Text>
                                    <View style={styles.reservation_liner2} />
                                    <View style={{height:'30%'}}>
                                        <View style={{flex:0.35,flexDirection:'row',alignItems:'flex-end'}}>
                                            <Text style={styles.reservation_textH1}>From</Text>
                                            <Text style={styles.reservation_textH2}>somewhere</Text>
                                        </View>
                                        <View style={{flex:0.3,flexDirection:'row'}}>
                                            <View style={styles.reservation_arrow}/>
                                            <Image source={require('../../../assets/images/paymentArrow.png')} style={styles.PaymentS_icon} />
                                        </View>
                                        <View style={{flex:0.35,flexDirection:'row',alignItems:'flex-end'}}>
                                            <Text style={styles.reservation_textH1}>To</Text>
                                            <Text style={styles.reservation_textH2}>somewhare? </Text>
                                        </View>
                                    </View>
                                    <View style={styles.reservation_detail}>
                                        <View style={{height:'30%',flexDirection:'row',alignItems:'flex-end',justifyContent:'space-around'}}>
                                            <Text style={styles.reservation_text3}>Booking Date</Text>
                                            <Text style={styles.reservation_text3}>somedate </Text>
                                        </View>
                                        <View style={{alignItems:'center'}}>
                                            <Text style={styles.reservation_textTime}>sometime</Text>
                                        </View>
                                    </View>

                                    <View style={{ height:'30%',alignItems:'center',justifyContent:'center',paddingBottom:50}}>
                                        <TouchableOpacity style={styles.reservation_detailbotton} onPress={()=>{this.setState({show:true})}} >
                                            <Text style={styles.reservation_buttonText}>More Details</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View> */}

                                <View style={[styles.Privilege_box]}>
                                    <Text
                                        style={[styles.enterBarcode_text]}>{this.props.voucher.first_name + ' ' + this.props.voucher.last_name}</Text>
                                    <Text style={styles.reservation_textModal}>Model : {this.props.voucher.model}</Text>
                                    <View style={styles.reservation_liner2}/>
                                    <View>
                                        <View style={{flexDirection: "row", marginTop: 10}}>
                                            <Text style={{
                                                fontSize:Dim.width/380*16,
                                                fontFamily:SUB_FONT,
                                                color:'#4A4A4A',
                                                width: Dim.width * 0.2,
                                            }}>From</Text>
                                            <Text style={{
                                                fontSize:Dim.width/380*22,
                                                width:Dim.width * 0.5,
                                                fontFamily:SUB_FONT,
                                                color:'#4A4A4A',
                                                fontWeight:'bold'
                                            }}>{this.props.voucher.from}</Text>
                                        </View>
                                        <View style={{flexDirection: "row", marginTop: 10,}}>
                                            <Text style={{
                                                fontSize:Dim.width/380*16,
                                                fontFamily:SUB_FONT,
                                                color:'#4A4A4A',
                                                width: Dim.width * 0.2,
                                            }}>To</Text>
                                            <Text
                                                style={{
                                                    fontSize:Dim.width/380*22,
                                                    width:Dim.width * 0.5,
                                                    fontFamily:SUB_FONT,
                                                    color:'#4A4A4A',
                                                    fontWeight:'bold',
                                                }}>{this.props.voucher.to}</Text>
                                        </View>
                                    </View>
                                    {/*<View style={{marginTop: 10}}>
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={[styles.reservation_textH1, {marginTop: 3}]}>From</Text>
                                            <Text style={styles.reservation_textH2}>{this.props.voucher.from}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <View style={styles.reservation_arrow}/>
                                            <Image source={require('../../../assets/images/paymentArrow.png')}
                                                   style={styles.PaymentS_icon}/>
                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={[styles.reservation_textH1, {marginTop: 3}]}>To</Text>
                                            <Text style={styles.reservation_textH2}>{this.props.voucher.to}</Text>
                                        </View>
                                    </View>*/}
                                    <View style={[styles.reservation_detail, {padding: 10}]}>
                                        <View style={{
                                            // height: '30%',
                                            flexDirection: 'row',
                                            alignItems: 'flex-end',
                                            justifyContent: 'space-around',
                                            marginTop: 10,
                                        }}>
                                            <Text style={styles.reservation_text3}>Booking Date</Text>
                                            <Text style={styles.reservation_text3}>{used_at_date} </Text>
                                        </View>
                                        <View style={{alignItems: 'center'}}>
                                            <Text style={styles.reservation_textTime}>{used_at_time}</Text>
                                        </View>
                                    </View>

                                    <View style={{
                                        // height: '30%',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        // paddingBottom: 50,
                                        padding: 10,
                                    }}>
                                        <TouchableOpacity style={styles.reservation_detailbotton} onPress={() => {
                                            this.setState({show: true});
                                        }}>
                                            <Text style={styles.reservation_buttonText}>More Details</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>


                                <TouchableOpacity style={[styles.Privilege_DoneButton, {marginHorizontal: 30}]} onPress={() => this.onSubmit()}>
                                    <Text style={styles.textButton}>CONFIRM</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <UnderlineComponent leftbtn={false} />
                        <View>
                            <View style={{flex:1,flexDirection:'column-reverse'}}>
                                <Text style={styles.Underline_textAIR}>{this.props.userinfo.merchant_name}</Text>
                                <Text style={styles.Underline_textAL}>{this.props.userinfo.branch_name}</Text>
                            </View>
                        </View>
                    </ScrollView>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        voucher: state.voucher.voucher,
        userinfo:state.user.userinfo,
    };
};
export default connect(mapStateToProps, actions)(ReservationScreen);

