import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Container, Content} from 'native-base';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import UnderlineComponent from '../../Component/UnderlineComponent';
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';

class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            AppBanner: '',
            getValue: '',
            data: '',
        };
    }

    static getDerivedStateFromProps(props, state) {
        if (props.userinfo) {
            return {
                data: props.userinfo,
                // avatarSource: props.userinfo.user_avatar
            };
        }
        return null;
    }

    async componentDidMount() {
        // await console.log("check 222 : " ,this.state.data)
        this.importAppBanner();
        AsyncStorage.getItem('token').then(
            value => this.setState({getValue: value}),
        )
            .then(console.warn(this.state.getValue));
        this.props.getuserinfo();
        this.props.getBranchAll();


    }

    async importAppBanner() {
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({query: `query {  getMerchantAppBanner {    small    large  }}`}),
        })
            // .then(function (response) {
            //     response.json()
            //   })
            .then(response => response.json())
            .then(data => this.setState({AppBanner: data.data.getMerchantAppBanner.large}));
        await console.log(this.state.AppBanner);
    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true} leftAOTicon={true} Profile={true}/>
                    <View style={styles.home_container}>
                        <View style={{flex: 0.40, justifyContent: 'space-around'}}>
                            <TouchableOpacity style={styles.home_box} onPress={() => Actions.scan_barcode()}>
                                <View style={{flex: 0.6, justifyContent: 'center', alignItems: 'center'}}>
                                    <View style={{width: '80%', height: '80%', justifyContent: 'center'}}>
                                        <Text
                                            style={I18n.locale === 'en' ? styles.home_scanEN : styles.home_scanTH}>{I18n.t('home.scan')}</Text>
                                        <Text style={styles.workHistoryText2}>{I18n.t('home.scan_info')}</Text>
                                    </View>
                                </View>
                                <View style={styles.home_box_iconview}>
                                    <Image source={require('../../../assets/images/Group2x.png')}
                                           style={styles.home_icon}/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.home_box} onPress={() => Actions.history()}>
                                <View style={{flex: 0.6, justifyContent: 'center', alignItems: 'center'}}>
                                    <View style={{width: '80%', height: '80%', justifyContent: 'center'}}>
                                        <Text
                                            style={I18n.locale === 'en' ? styles.home_scanEN : styles.home_scanTH}>{I18n.t('home.history')}</Text>
                                        <Text style={styles.workHistoryText2}>{I18n.t('home.history_info')}</Text>
                                    </View>
                                </View>
                                <View style={styles.home_box_iconview}>
                                    <Image source={require('../../../assets/images/Group2x1.png')}
                                           style={styles.home_icon}/>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{flex: .50, justifyContent: 'flex-end', alignItems: 'center'}}>
                            {/* <View style={{width: 374,height: 340 ,backgroundColor:'white'}}> */}
                            <Image source={{uri: this.state.AppBanner}} style={styles.home_ads_banner}/>
                            {/* </View> */}
                        </View>
                        {/* <UnderlineComponent rightbtn={true}   /> */}
                        <UnderlineComponent rightbtn={true} branch_name={this.state.data.branch_name}
                                            merchant_name={this.state.data.merchant_name}/>

                    </View>

                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        userinfo: state.user.userinfo,
    };
};
export default connect(mapStateToProps, actions)(HomeScreen);

{/* <View style={styles.view_center}>
<View style={{flex:1,backgroundColor:'red'}}></View>
<Text>{`Welcome ${this.props.username}`}</Text>

<TouchableOpacity style={styles.submit_button} onPress={()=> Actions.scan_barcode()}>
    <Text style={styles.txt_btn}>{I18n.t('home.scan')}</Text>
</TouchableOpacity>

<TouchableOpacity style={styles.submit_button} onPress={()=> Actions.pop()}>
    <Text style={styles.txt_btn}>{I18n.t('login.logout')}</Text>
</TouchableOpacity>
</View> */
}
