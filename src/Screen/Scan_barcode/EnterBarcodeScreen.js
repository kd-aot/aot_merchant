import React, { Component } from "react"
import { View , Text , TouchableOpacity,ScrollView} from "react-native"
import { Container , Item ,Input } from 'native-base'
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';
class EnterBarcodeScreen extends Component {
    constructor(props){
        super(props)
            this.state=({
                voucher_id: '',
                branch_id: ''
            })

    }

    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
    }

    static getDerivedStateFromProps(props, state) {
        if (props.userinfo) {
          return {
            data: props.userinfo,
            // avatarSource: props.userinfo.user_avatar
          };
        }
        return null;
      }
    onSubmit(){
        var voucher_id = ""
        voucher_id = this.state.voucher_id
        let txt = "ptg";
        if(voucher_id.toLowerCase().indexOf(txt) > -1){
            console.log(voucher_id);
            this.props.useMerchantVoucherPTG(voucher_id, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id)
        } else {
            this.props.getVoucherDetail(voucher_id, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id)
        }
    }
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                <HeaderComponent home={true} leftbtn={true} title={true} useVoucher1={true} />
                    <View style={styles.home_container}>
                        <ScrollView style={{flex:0.9}}>
                            <View style={{flex:0.5,alignItems:'center'}}>
                                <View style={{flex:0.8,flexDirection:'column',justifyContent:'flex-end',alignItems:'center'}}>
                                    <Text style={styles.enterBarcode_text}>{I18n.t('enter_barcode.detail_1')}</Text>
                                    <Item rounded style={styles.Password_InputBox}>
                                        <Input placeholder={I18n.t('enter_barcode.code')}
                                        style={styles.enterBarcode_textInput}
                                        value={this.state.voucher_id}
                                        onChangeText={(text)=>this.setState({voucher_id:text})}

                                        // keyboardType="phone-pad"
                                        />
                                    </Item>
                                </View>
                                {
                                    this.state.voucher_id
                                    ?   <TouchableOpacity style={styles.enterBarcode_next} onPress={()=> this.onSubmit() } >
                                            <View style={styles.enterBarcode_nextSub}>
                                                <Text style={styles.textButton}>{I18n.t('enter_barcode.next')}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    :   <View style={styles.enterBarcode_next}>
                                            <View style={[styles.enterBarcode_nextSub, {backgroundColor: "#C8CBCD"}]}>
                                                <Text style={styles.textButton}>{I18n.t('enter_barcode.next')}</Text>
                                            </View>
                                        </View>
                                }
                            </View>
                            <View style={{flex:0.5}}></View>
                        </ScrollView>
                        <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo:state.user.userinfo
    }
  }
export default connect(mapStateToProps,actions)(EnterBarcodeScreen)

