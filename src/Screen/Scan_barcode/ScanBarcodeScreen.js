import React, { Component } from "react"
import {View, Alert, Text, TouchableOpacity, Image, Modal, Dimensions, Platform} from 'react-native';
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Container, Content } from 'native-base'
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
// import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import FooterComponent from '../../Component/FooterComponent';
import NetInfo from "@react-native-community/netinfo";
import ImagePicker from 'react-native-image-picker';
import { QRreader as QRreaderIos } from "react-native-qr-decode-image-camera";
import {QRreader} from 'react-native-qr-scanner';
import AsyncStorage from '@react-native-community/async-storage';
const Dim = Dimensions.get("window");
const options = {
    title: 'Select Image',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
    base64: true
};

class ScanBarcodeScreen extends Component {
    constructor(props){
        super(props)
        this.state={
            show:true,
            scan_flaq:true,
            checkScan: true,
            checkNet: false,
            branch_id: null,
        }
    }

    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
        const unsubscribe = NetInfo.addEventListener(state => {
            this.setState({checkNet: state.isConnected});
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.openCameraTime !== prevProps.openCameraTime) {
            if (this.props.openCamera === "open") {
                this.setState({scan_flaq: true})
            } else if (this.props.openCamera === "close") {
                this.setState({scan_flaq: false})
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        if (props.userinfo) {
          return {
            data: props.userinfo,
            // avatarSource: props.userinfo.user_avatar
          };
        }
        return null;
      }

    onScan(qr){
        if (this.state.checkNet) {
            var str = qr.data
            var n = str.indexOf(",");
            if(this.state.scan_flaq == true){
                let qrData = qr.data.split(',');
                if(n != -1){
                    /// payment
                    // Alert.alert('Code payment',qr.data)
                    this.props.scanPayment(qr.data)
                    this.setState({scan_flaq: false})
                }else{
                    let txt = "ptg";
                    if(qr.data.toLowerCase().indexOf(txt) > -1){
                        // console.log(qr.data);
                        this.setState({scan_flaq: false})
                        Alert.alert(
                            I18n.t('scanQRCode.ask'),
                            qr.data,
                            [
                                {
                                    text: I18n.t('scanQRCode.confirm'),
                                    onPress: () => {
                                        this.props.useMerchantVoucherPTG(qr.data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id);
                                    }
                                },
                                {
                                    text: I18n.t('scanQRCode.cancel'),
                                    onPress: () => this.setState({scan_flaq: true}),
                                    style: 'cancel'
                                },
                            ],
                            { cancelable: false }
                        );
                    } else {
                        this.setState({scan_flaq: false});
                        this.props.getVoucherDetail(qr.data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id)
                    }
                    /// voucher
                    // Alert.alert('Code voucher',qr.data)
                    //
                }
            }
        } else {
            if (this.state.scan_flaq) {
                this.setState({scan_flaq: false})
                Alert.alert(
                    "No Internet/Poor Connection",
                    "",
                    [
                        { text: "OK", onPress: () => {
                                this.setState({scan_flaq: true})
                            }}
                    ],
                    { cancelable: false }
                );
            }
        }

    }

    selectImage() {
        this.setState({scan_flaq: false});
        ImagePicker.launchImageLibrary(options, response => {
            // console.warn(response);
            // Alert.alert(response.toString());

            if(response.uri){
                var path = response.path;
                if(!path){
                    path = response.uri;
                }

                if (Platform.OS === "ios") {
                    QRreaderIos(path)
                        .then(data => {
                            // Alert.alert(data);
                            var check = data.indexOf(",");
                            // console.log(data)
                            if(check !== -1){
                                /// payment
                                this.props.scanPayment(data)
                                this.setState({scan_flaq: false})
                            } else {
                                let txt = "ptg";
                                if(data.toLowerCase().indexOf(txt) > -1){
                                    // console.log(data);
                                    this.setState({scan_flaq: false})
                                    Alert.alert(
                                        I18n.t('scanQRCode.ask'),
                                        data,
                                        [
                                            {
                                                text: I18n.t('scanQRCode.confirm'),
                                                onPress: () => {
                                                    this.props.useMerchantVoucherPTG(data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id);
                                                }
                                            },
                                            {
                                                text: I18n.t('scanQRCode.cancel'),
                                                onPress: () => this.setState({scan_flaq: true}),
                                                style: 'cancel'
                                            },
                                        ],
                                        { cancelable: false }
                                    );
                                } else {
                                    this.setState({scan_flaq: false});
                                    this.props.getVoucherDetail(data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id)
                                }
                                /// voucher
                            }
                        })
                        .catch(err => {
                            Actions.privilege_fail({type: "replace"});
                        });
                } else {
                    QRreader(path)
                        .then(data => {
                            // Alert.alert(data);
                            var check = data.indexOf(",");
                            // console.log(data)
                            if(check !== -1){
                                /// payment
                                this.props.scanPayment(data)
                                this.setState({scan_flaq: false})
                            } else {
                                let txt = "ptg";
                                if(data.toLowerCase().indexOf(txt) > -1){
                                    // console.log(data);
                                    this.setState({scan_flaq: false})
                                    Alert.alert(
                                        I18n.t('scanQRCode.ask'),
                                        data,
                                        [
                                            {
                                                text: I18n.t('scanQRCode.confirm'),
                                                onPress: () => {
                                                    this.props.useMerchantVoucherPTG(data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id);
                                                }
                                            },
                                            {
                                                text: I18n.t('scanQRCode.cancel'),
                                                onPress: () => this.setState({scan_flaq: true}),
                                                style: 'cancel'
                                            },
                                        ],
                                        { cancelable: false }
                                    );
                                } else {
                                    this.setState({scan_flaq: false});
                                    this.props.getVoucherDetail(data, this.state.branch_id ? this.state.branch_id : this.state.data.branch_id)
                                }
                                /// voucher
                            }
                        })
                        .catch(err => {
                            Actions.privilege_fail({type: "replace"});
                        });
                }
            }
        });
    }
    render() {
        return (
            <Container>
                 <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                <HeaderComponent home={true} leftbtn={true} title={true} scanQR={true} />
                <View style={styles.scan_container}>
                <RNCamera
                    ref={(camera) => scanner = camera}
                    style={{flex:0.92}}
                    type={RNCamera.Constants.Type.back}
                    onBarCodeRead={ (qr) => this.onScan(qr) }
                    // barCodeTypes={['org.iso.QRCode',]}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    captureAudio={false}
                >
                     <View style={styles.scan_button}>
                            <View style={{justifyContent:'space-between',alignItems:'center', flexDirection: "row", padding: 10}}>
                                <TouchableOpacity style={{borderColor: '#FFFFFF', borderWidth: 1, width: Dim.width * 0.42, height: Dim.height * 0.07, borderRadius: Dim.width * 0.2, justifyContent: 'center', alignItems: 'center'}} onPress={()=> {Actions.enter_barcode();this.setState({show:false}) }} >
                                    <Text style={styles.textButtonScan}>{I18n.t('scan_barcode.use_voucher')}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{borderColor: '#FFFFFF', borderWidth: 1, width: Dim.width * 0.42, height: Dim.height * 0.07, borderRadius: Dim.width * 0.2, justifyContent: 'center', alignItems: 'center'}} onPress={() => this.selectImage()} >
                                    <Text style={styles.textButtonScan}>{I18n.t('scan_barcode.use_photo')}</Text>
                                </TouchableOpacity>
                                {/*<TouchableOpacity onPress={() => this.selectImage()} style={{borderColor: '#FFFFFF', borderWidth: 1, width: Dim.width * 0.2, height: Dim.width * 0.2, borderRadius: (Dim.width * 0.2)/2, justifyContent: 'center', alignItems: 'center'}}>*/}
                                {/*    <Image source={require("../../../assets/images/ic_photo.png")} style={{width: Dim.width * 0.1, height: Dim.width * 0.1}} />*/}
                                {/*</TouchableOpacity>*/}
                            </View>
                        </View>
                </RNCamera>
                    {
                        this.props.userinfo
                        ?   <View style={styles.scan_underlineContainer}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.Underline_textAL}>{this.props.userinfo.merchant_name}</Text>
                                    <Text style={styles.Underline_textAIR}>{this.props.userinfo.branch_name}</Text>
                                </View>
                                {/* <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                        <TouchableOpacity onPress={()=> Actions.pop()}><Image source={require('../../../assets/images/Underline.png')} style={{width:30,height:30}} /></TouchableOpacity>
                    </View> */}
                            </View>
                        :   null
                    }

                </View>
                     <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo:state.user.userinfo,
        openCamera: state.pay.openCamera,
        openCameraTime: state.pay.openCameraTime,
    }
  }
export default connect(mapStateToProps,actions)(ScanBarcodeScreen)

