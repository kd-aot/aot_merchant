import React, { Component } from "react"
import { Scene, Router, Stack, Actions } from "react-native-router-flux"

import Splash from './Splash'
// Authentication Screen
import LoginScreen from './Auth/LoginScreen'
import BranchSelectScreen from './Auth/BranchSelectScreen'

// ForgotPassword Screen
import ForgotPasswordScreen  from './ForgotPassword/ForgotPasswordScreen'
import ForgotPasswordSuccessScreen from './ForgotPassword/ForgotPasswordSuccessScreen'

// ChangePasswordScreen
import ChangePasswordScreen from './ChangePassword/ChangePasswordScreen'
import ChangePasswordCheckScreen from './ChangePassword/ChangePasswordCheckScreen'
import ChangePasswordSuccessScreen from './ChangePassword/ChangePasswordSuccessScreen'

// Home Screen
import HomeScreen from './Home/HomeScreen'

// Scan Barcode Screen
import ScanBarcodeScreen from './Scan_barcode/ScanBarcodeScreen'
import EnterBarcodeScreen from './Scan_barcode/EnterBarcodeScreen'

// Payment Screen
import PaymentScreen from './Payment/PaymentScreen'
import PaymentSuccessScreen from './Payment/PaymentSuccessScreen'
import PaymentFailScreen from './Payment/PaymentFailScreen'
// Privilege Screen
import PrivilegeSuccessScreen from './Privilege_voucher/PrivilegeSuccessScreen'
import PrivilegeFailScreen from './Privilege_voucher/PrivilegeFailScreen'
import PrivilegeSuccess from './Privilege_voucher/PrivilegeSuccess';
// ReservationScreen
import ReservationScreen from './Reservation_voucher/ReservationScreen'
import ReservationSuccessScreen from './Reservation_voucher/ReservationSuccesScreen'
// History Screen
import HistoryScreen from './History/HistoryScreen'
import HistoryDetailScreen from './History/HistoryDetailScreen'
import HistoryMoredataScreen from './History/HistoryMoredataScreen'
import HistoryDetailVoucherScreen from './History/HistoryDetailVoucherScreen'
import HistoryVoucherDetail from './History/HistoryVoucherDetail'

// Profile
import ManagerProfileScreen from './Profile/ManagerProfileScreen'
import StaffProfileScreen from './Profile/StaffProfileScreen'
import LanguageChangeScreen from './Profile/LanguageChangeScreen'
import BranchDetail from './Profile/BranchDetail'
import ChangeBranchScreen from './Profile/ChangeBranchScreen'
import ManagerHistoryScreen from './Profile/ManagerHistoryScreen'

export default class Routers extends Component {
    render(){
        return(
            <Router>
                <Stack key='root'>
                    {<Scene key="splah" component={Splash} hideNavBar/>}

                    {<Scene key="login" gesturesEnabled={false} component={LoginScreen} hideNavBar/>}
                    {<Scene key="branch_select" gesturesEnabled={false} component={BranchSelectScreen} hideNavBar/>}


                    {<Scene key="home" gesturesEnabled={false} component={HomeScreen} hideNavBar/>}

                    {<Scene key="scan_barcode" gesturesEnabled={false} component={ScanBarcodeScreen} hideNavBar/>}
                    {<Scene key="enter_barcode" gesturesEnabled={false} component={EnterBarcodeScreen} hideNavBar/>}

                    {<Scene key="payment" gesturesEnabled={false} component={PaymentScreen} hideNavBar/>}
                    {<Scene key="payment_success" gesturesEnabled={false} component={PaymentSuccessScreen} hideNavBar/>}
                    {<Scene key="payment_fail" gesturesEnabled={false} component={PaymentFailScreen} hideNavBar/>}

                    {<Scene key="privilege_success" gesturesEnabled={false} component={PrivilegeSuccessScreen} hideNavBar/>}
                    {<Scene key="privilege_fail" gesturesEnabled={false} component={PrivilegeFailScreen} hideNavBar/>}
                    {<Scene key="privilege_success_status" gesturesEnabled={false} component={PrivilegeSuccess} hideNavBar/>}

                    {<Scene key="reservation" gesturesEnabled={false} component={ReservationScreen} hideNavBar/>}
                    {<Scene key="reservation_success" gesturesEnabled={false} component={ReservationSuccessScreen} hideNavBar/>}

                    {<Scene key="history" gesturesEnabled={false} component={HistoryScreen} hideNavBar/>}
                    {<Scene key="history_detail" gesturesEnabled={false} component={HistoryDetailScreen} hideNavBar/>}
                    {<Scene key="history_detail_voucher" gesturesEnabled={false} component={HistoryDetailVoucherScreen} hideNavBar/>}
                    {<Scene key="history_voucher_detail" gesturesEnabled={false} component={HistoryVoucherDetail} hideNavBar/>}
                    {<Scene key="history_moredata" gesturesEnabled={false} component={HistoryMoredataScreen} hideNavBar/>}

                    {<Scene key="forgot_password" gesturesEnabled={false} component={ForgotPasswordScreen} hideNavBar/>}
                    {<Scene key="forgot_password_success" gesturesEnabled={false} component={ForgotPasswordSuccessScreen} hideNavBar/>}

                    {<Scene key="ChangePasswordScreen" gesturesEnabled={false} component={ChangePasswordScreen} hideNavBar/>}
                    {<Scene key="ChangePasswordCheckScreen" gesturesEnabled={false} component={ChangePasswordCheckScreen} hideNavBar/>}
                    {<Scene key="ChangePasswordSuccessScreen" gesturesEnabled={false} component={ChangePasswordSuccessScreen} hideNavBar/>}

                    {<Scene key="Manager_profile" gesturesEnabled={false} component={ManagerProfileScreen} hideNavBar/>}
                    {<Scene key="Staff_profile" gesturesEnabled={false} component={StaffProfileScreen} hideNavBar/>}
                    {<Scene key="LanguageChangeScreen" gesturesEnabled={false} component={LanguageChangeScreen} hideNavBar/>}
                    {<Scene key="BranchDetail" gesturesEnabled={false} component={BranchDetail} hideNavBar/>}
                    {<Scene key="ChangeBranchScreen" gesturesEnabled={false} component={ChangeBranchScreen} hideNavBar/>}
                    {<Scene key="ManagerHistoryScreen" gesturesEnabled={false} component={ManagerHistoryScreen} hideNavBar/>}
                </Stack>
            </Router>
        )
    }
}
