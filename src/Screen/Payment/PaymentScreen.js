import React, {Component} from 'react';
import {View, Text, TextInput, TouchableOpacity, ScrollView, Alert} from 'react-native';
import {Container, Header, Content, Icon, Picker, Form, Item, Input} from 'native-base';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import firestore from '@react-native-firebase/firestore';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import UnderlineComponent from '../../Component/UnderlineComponent';
import LinearGradient from 'react-native-linear-gradient';
import {Value} from 'react-native-reanimated';
import {MaterialIndicator} from 'react-native-indicators';

import {TextInputMask} from 'react-native-masked-text';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';

let limit = 10;
let unsubscribe = null;
class PaymentScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: 'THB',
            od: '',
            orderrandom: null,
            waiting: false,
            checkFail: true,
            checkPass: true,
            simple: "",
            time: null,
            branch_id: null,
        };
    }

    async queryPayment() {
        if (this.state.time === null) {
            this.setState({time: "start"});
            this.checkTimer();
        }
        unsubscribe = firestore()
            .collection('merchant-payment-room')
            .doc(this.state.od)
            .onSnapshot(snapshot => {
                // console.log('snapshot', snapshot.data());
                if (snapshot.data() && snapshot.data().is_payment) {
                    console.log(snapshot.data().is_payment);
                    if (snapshot.data().is_payment == 'approved') {
                        if (this.state.checkPass) {
                            this.setState({checkPass: false}, () => {
                                this.setState({simple: ""});
                                unsubscribe();
                                Actions.payment_success();
                            })
                        }
                    } else if (snapshot.data().is_payment == 'rejected') {
                        if (this.state.checkFail) {
                            this.setState({checkFail: false}, () => {
                                this.setState({simple: ""});
                                unsubscribe();
                                Actions.payment_fail({type: 'replace'});
                            })
                        }
                    }
                } else {
                    // setPayment('');
                }
            });
    }

    static getDerivedStateFromProps(props, state) {
        // console.log('props.order', props.order);
        if (props.order) {
            if (props.order.payload.order_number && props.orderrandom != state.orderrandom) {
                return {
                    od: props.order.payload.order_number,
                    orderrandom: props.orderrandom,
                };
            }
        }
        return null;
    }

    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log('Order : ', this.props.order);
        if (this.state.waiting) {
            if (this.props.order !== "") {
                this.queryPayment();
            }
        }
    }

    checkTimer() {
        setTimeout(() => {
            this.setState({checkFail: false}, () => {
                this.setState({simple: ""});
                unsubscribe();
                Actions.payment_fail({type: 'replace'});
            })
        }, 1080000);
    }

    ConTwoDecDigit=(digit)=>{
        return digit.indexOf(".")>0?
            digit.split(".").length>=1?
                digit.split(".")[0]+"."+digit.split(".")[1].substring(-1,2)
                : digit
            : digit
    };

    onValueChange(value: string) {
        this.setState({
            selected: value,
        });
    };

    onSetNumberToString() {

    };

    onSetStringToNumber(value) {
        let check = value.replace(/[- #*;,<>\{\}\[\]\\\/]/gi, '');
        if (check.indexOf(".") > -1) {
            limit = check.indexOf(".") + 3;
        } else {
            limit = 10;
        }
        return value;
    };

    onSubmit() {
        // console.log(this.state.userinfo);
        this.setState({waiting: true})
        var price = this.state.simple;
        var code = this.props.paymentcode;
        var branch_id = this.state.branch_id ? this.state.branch_id : this.props.userinfo.branch_id;
        this.props.sendPayment(price, code, branch_id);
    }

    // async queryPayment() {
    //     console.log('query payment');
    //     await firestore.collection('merchant-payment-room').doc('20200702182849EBA').onSnapshot(snapshot => {
    //       if (snapshot.data() && snapshot.data().is_payment) {
    //         setPayment(snapshot.data().is_payment);
    //       } else {
    //         setPayment('');
    //       }
    //     });
    //   }


    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} payment={true}/>
                    <View style={styles.home_container}>
                        <ScrollView style={{flex: 0.9}}>
                            <View style={{flex: 0.8, alignItems: 'center'}}>
                                <View style={{
                                    flex: 0.8,
                                    flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                    <Text style={styles.enterBarcode_text}>{I18n.t('payment.title_1')}</Text>
                                    <Item rounded style={styles.enterBarcode_item}>
                                        <TextInput
                                            value={this.state.simple}
                                            onChangeText={(text) => this.setState({simple: this.onSetStringToNumber(text)})}
                                            style={styles.enterBarcode_TextInput}
                                            keyboardType={"numeric"}
                                            contextMenuHidden={true}
                                            placeholder={"00.00"}
                                            placeholderTextColor={"#EAECEE"}
                                            maxLength={limit}
                                        />
                                    </Item>
                                    <View style={styles.enterBarcode_item2}>
                                        <Form>
                                            <Picker
                                                mode="dropdown"
                                                iosHeader="Choose your preferred currency"
                                                iosIcon={<Icon name="arrow-down"/>}
                                                style={styles.enterBarcode_Picker}
                                                selectedValue={this.state.selected}
                                                onValueChange={this.onValueChange.bind(this)}
                                                textStyle={styles.enterBarcode_Pickertext}
                                            >
                                                <Picker.Item label="Thai Baht(THB)" value="THB"/>
                                                {/* <Picker.Item label="US Dollar(USD)" value="USD" /> */}
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                                <View style={styles.enterBarcode_block}/>
                                <View style={{width: '70%', height: '30%'}}>
                                    {/* <TouchableOpacity style={{flex:0.7}} onPress={()=> Actions.payment_success()} > */}
                                    {
                                        this.state.waiting
                                        ?   <TouchableOpacity activeOpacity={1} style={{flex: 0.7}} >
                                                <View style={styles.enterBarcode_nextSub}>
                                                    <MaterialIndicator color='white' count={5} size={40}/>
                                                </View>
                                            </TouchableOpacity>
                                        :   this.state.simple
                                            ?   <TouchableOpacity style={{flex: 0.7}} onPress={() => {
                                                    this.onSubmit();
                                                }}>
                                                    <View style={styles.enterBarcode_nextSub}>
                                                        <Text style={styles.textButton}>{I18n.t('payment.confirm')}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            :   <View style={{flex: 0.7}}>
                                                <View style={[styles.enterBarcode_nextSub, {backgroundColor: "#C8CBCD"}]}>
                                                    <Text style={styles.textButton}>{I18n.t('payment.confirm')}</Text>
                                                </View>
                                            </View>
                                    }
                                </View>
                            </View>
                            <View style={{flex: 0.2}}></View>
                        </ScrollView>
                        <UnderlineComponent leftbtn={false}/>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        userinfo: state.user.userinfo,
        paymentcode: state.pay.payment,
        order: state.order.order_number,
        orderrandom: state.order.order_random,
    };
};
export default connect(mapStateToProps, actions)(PaymentScreen);

