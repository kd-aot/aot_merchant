import React, {Component} from 'react';

import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Container, Header, Content, Icon, Picker, Form, Item, Input} from 'native-base';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import UnderlineComponent from '../../Component/UnderlineComponent';
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';


class PaymentSuccessScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataPayload: null,
        }
    }


    // static getDerivedStateFromProps(props, state) {
    //     if (props.detail) {
    //       return {
    //         detail: props.detail,
    //       };
    //     }
    //     return null;
    // }


    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({dataPayload: this.props.order ? this.props.order.payload : null});
        // var branch_id = this.props.userinfo.branch_id;
        if (this.props.order) {
            this.props.getHistoryPaymentList(this.props.order.payload.order_number, branch_id);
        }

    }

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true}/>
                    <View style={styles.home_container}>

                        {/* for layout */}
                        {/* <View style={{flex:0.9}}>
                            <View style={styles.whiteBox}>
                            <Text style={styles.PaymentS_TextpaymentSuccess}>PAYMENT SUCCESS</Text>
                            <Image  source={require('../../../assets/images/paymentSuccess.png')} style={styles.PaymentS_image} />
                            <Text style={styles.PaymentS_TextPaymentID}>Payment ID:00040008</Text>
                            <Text style={styles.PaymentS_TextBig}>Fname Lname </Text>
                            <Image source={require('../../../assets/images/paymentArrow.png')} style={styles.PaymentS_icon} />
                            <Text style={styles.PaymentS_TextBig}>AOT Limousine</Text>

                            <Text style={styles.PaymentS_TextPrice}>1944.00 THB</Text>
                            <Text style={styles.PaymentS_TextBig}>+XX Points</Text>
                                <TouchableOpacity style={styles.PaymemtS_button} onPress={()=> Actions.home()} >
                                    <View style={styles.enterBarcode_nextSub}>
                                        <Text style={styles.textButton}>DONE</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View> */}

                        {/* for use */}
                        <View style={{flex: 1}}>
                            <View style={styles.whiteBox}>
                                <Text
                                    style={styles.PaymentS_TextpaymentSuccess}>{I18n.t('payment_success.title_1')}</Text>
                                <Image source={require('../../../assets/images/paymentSuccess.png')}
                                       style={styles.PaymentS_image}/>
                                {
                                    this.state.dataPayload
                                        ? <Text
                                            style={styles.PaymentS_TextPaymentID}>{I18n.t('payment_success.order_number')}{this.state.dataPayload.order_number} </Text>
                                        : null
                                }
                                {
                                    this.state.dataPayload
                                        ? <Text
                                            style={styles.PaymentS_TextBig}>{this.state.dataPayload.fullname}</Text>
                                        : null
                                }

                                <Image source={require('../../../assets/images/paymentArrow.png')}
                                       style={styles.PaymentS_icon}/>
                                {
                                        this.props.userinfo
                                        ?   <Text style={styles.PaymentS_TextBig}>{this.props.userinfo.merchant_name}</Text>
                                        :   null
                                }

                                {
                                    this.state.dataPayload
                                        ? <Text
                                            style={styles.PaymentS_TextPrice}>
                                            {`${parseFloat(this.state.dataPayload.price).toLocaleString(undefined, {minimumFractionDigits: 2})} THB `}
                                          </Text>
                                        : null
                                }

                                {
                                    this.state.dataPayload
                                        ? <Text
                                            style={styles.PaymentS_TextBig}>{parseInt(this.state.dataPayload.point_received) > 0 ? "+ " + this.state.dataPayload.point_received + " Points" : this.state.dataPayload.point_received + " Point"} </Text> //{I18n.t('payment_success.point')}
                                        : null
                                }

                                <TouchableOpacity style={styles.PaymemtS_button} onPress={() => Actions.home()}>
                                    <View style={styles.enterBarcode_nextSub}>
                                        <Text style={styles.textButton}>{I18n.t('payment_success.done')}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <UnderlineComponent leftbtn={false} branch_name={this.props.userinfo.branch_name}
                                            merchant_name={this.props.userinfo.merchant_name}/>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        order: state.order.order_number,
        userinfo: state.user.userinfo,
        detail: state.paydetail.paymentDetail,
    };
};
export default connect(mapStateToProps, actions)(PaymentSuccessScreen)

