import React, { Component } from "react"
import { View, Platform, PermissionsAndroid,BackHandler,Dimensions } from "react-native"
import { connect } from "react-redux"
import * as actions from "../Actions"
import { Actions } from "react-native-router-flux"
import firestore from '@react-native-firebase/firestore';
import styles from '../Style/style'
import AsyncStorage from '@react-native-community/async-storage';
var Dim = Dimensions.get('window')
class Splash extends Component {
    constructor(props){
        super(props)
        const usersCollection = firestore().collection('merchant-payment-room');
    }
    onResult(QuerySnapshot) {
        console.log('Got Users collection result.',);
        QuerySnapshot.docChange().forEach((change)=>{console.log("firestore" ,change)} )
    }
      
    onError(error) {
        console.error(error);
    }
    
      

    async componentDidMount(){
        this.props.checkiphonex(Dim)
        console.log(this.props.usersCollection)
        
        // const user = await firestore()
        // .collection('merchant-payment-room')
        // .doc('20200702113817FC8')
        // .get();
        // console.log(user)

        if (Platform.OS == 'android')
        {
        const granted = PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.CAMERA, PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION, PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE, PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE]).then((result) => {
        });
        }
        // this.checkPermission();
        // this.createNotificationListeners();
        // this.createNotificationChannel()
        // firebase.messaging().subscribeToTopic('All')
       
       

        BackHandler.addEventListener("back", () => this._handleBack())
        var token = await AsyncStorage.getItem('usertoken')
        console.warn('token',token)
        if(token != null && token != '' && token != undefined){
            if(Platform.OS == 'ios'){
                console.warn('ios')
                  setTimeout(() => 
                  {
                      Actions.home()
                  }, 100
                  )
              }else{
                console.warn('android')
                  setTimeout(() => 
                  {
                      Actions.home()
                  }, 1000
                  )
              }
        }else{
            if(Platform.OS == 'ios'){
                console.warn('ios')
                  setTimeout(() => 
                  {
                      Actions.login()
                  }, 100
                  )
              }else{
                console.warn('android')
                  setTimeout(() => 
                  {
                      Actions.login()
                  }, 1000
                  )
              }
        }
        
        
    }
    // async createNotificationListeners() {
    //     // console.warn('createNotificationListeners')
    //     this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
    //       console.log('notificationListener',notification.body)
    //       notification.android.setChannelId('alert');

    //       const notification1 = new firebase.notifications.Notification()
    //       .setTitle(notification.title)
    //       .setBody(notification.body.replace(/\\n| \\n/g, "\n"))
    //       .setData({
    //         key: true,
    //         data: {},
    //       })
    //       .android.setChannelId('alert')
    //       .android.setBigText(notification.body.replace(/(?:\r\n|\r|\n|\\n)/g, "\n"))
    //       .android.setPriority(firebase.notifications.Android.Priority.Max);

    //       firebase.notifications().displayNotification(notification1)
    //       // Alert.alert('notificationListener')
    //     });
       

    //     this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
    //       console.log('notificationDisplayedListener')
    //     });
    //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
    //       console.log('notificationOpenedListener')
          
    //     });
    //     const notificationOpen = await firebase.notifications().getInitialNotification();
    //     if (notificationOpen) {
    //         // const { title, body } = notificationOpen.notification;
    //         // this.showAlert(title, body);
    //     }
    
    //     this.messageListener = firebase.messaging().onMessage((message) => {
    //       //process data message
    //       console.log('messageListener',message)
    //       console.log(JSON.stringify(message));
    //     });
    //   }
    //   createNotificationChannel() {
    //     // Build a channel
    //     const channel = new firebase.notifications.Android.Channel('alert', 'Alert', firebase.notifications.Android.Importance.Max)
    //       .setDescription('Alert')
    //     console.log('createNotificationChannel',channel)
    //     // Create the channel
    //     firebase.notifications().android.createChannel(channel)
    //   }
    _handleBack() {
        if(Actions.currentScene == "_login" || Actions.currentScene == 'login') {
       
            BackHandler.exitApp()
        
          return true
        }
         return false
    }
    // async checkPermission() {
    //     const enabled = await firebase.messaging().hasPermission();
    //     if (enabled) {
    //         this.getToken();
    //     } else {
    //         this.requestPermission();
    //     }
    // }
    // async requestPermission() {
    //     try {
    //         await firebase.messaging().requestPermission();
    //         // User has authorised
    //         this.getToken();
    //     } catch (error) {
    //         // User has rejected permissions
    //         console.log('permission rejected');
    //     }
    // }
    // getToken() {
    //     firebase.messaging().getToken()
    //       .then(fcmToken => {
    //         if (fcmToken) {
    //             AsyncStorage.setItem("fcmToken", fcmToken, () => {
    //                 console.warn("fcmToken is " + fcmToken);
    //               }
    //             )
    //         } else {
    //           console.warn("user doesn't have a device token yet");
    //         }
    //       });
    //   }
    render() {
        return (
            <View/>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(Splash)

