import React, { Component } from "react"

import { View , Text , TouchableOpacity ,Image,Modal,ScrollView } from "react-native"
import { Container, Header, Content, Icon, Picker, Form , Item , Input} from "native-base";
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
class PrivilegeFailScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false
        }
    }

        render() {
            return (
                <Container>
                    <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                        <HeaderComponent home={true} />
                        <View style={styles.home_container}>
                            <View style={{flex:0.9}}>
                                <View style={{flex:0.8,backgroundColor:'white', borderRadius: 20,alignItems:'center',justifyContent:'center' }}>
                                <Text style={styles.PaymentS_TextpaymentSuccess}>INVALID VOUCHER</Text>
                                <Image  source={require('../../../assets/images/paymentFail.png')} style={styles.PaymentS_image} />

                                <Text style={styles.PaymentS_TextPrice}>Sorry Something is wrong</Text>
                                    <TouchableOpacity style={styles.PaymemtF_button} onPress={()=> Actions.scan_barcode()} >
                                            <Text style={styles.textButton}>RETRY</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        <UnderlineComponent leftbtn={false} />
                            <View style={styles.scan_underlineContainer}>
                                <View style={{flex:1}}>
                                    <Text style={styles.Underline_textAL}>{this.props.userinfo.merchant_name}</Text>
                                    <Text style={styles.Underline_textAIR}>{this.props.userinfo.branch_name}</Text>
                                </View>
                                {/* <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                        <TouchableOpacity onPress={()=> Actions.pop()}><Image source={require('../../../assets/images/Underline.png')} style={{width:30,height:30}} /></TouchableOpacity>
                    </View> */}
                            </View>
                        </View>
                        <FooterComponent/>
                    </LinearGradient>
                </Container>
            )
        }
    }


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo: state.user.userinfo,
    }
  }
export default connect(mapStateToProps,actions)(PrivilegeFailScreen)

