import React, { Component } from "react"

import { View , Text , TouchableOpacity ,Image,Modal,ScrollView } from "react-native"
import { Container, Header, Content, Icon, Picker, Form , Item , Input} from "native-base";
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';
class PrivilegeSuccess extends Component {
    constructor(props){
        super(props)
    }

    componentDidMount() {
        console.log(this.props.typePtg)
    }

    static getDerivedStateFromProps(props, state) {
        if (props.voucher) {
            return {
                voucher: props.voucher,
            };
        }
        return null;
    }
    render() {

        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} />
                    <View style={styles.home_container}>
                        {/* <View style={{flex:0.9}}>
                            <View style={{flex:1,backgroundColor:'white', borderRadius: 20,alignItems:'center',justifyContent:'center' }}>
                            <Text style={styles.PaymentS_TextpaymentSuccess}>RESERVATION  SUCCESS</Text>
                            <Image  source={require('../../../assets/images/paymentSuccess.png')} style={styles.PaymentS_image} />
                            <Text style={styles.reservationSS_TextPaymentID}>Payment ID:00040008</Text>
                            <Text style={styles.reservationSS_TextBig}>Modal : xxxx</Text>
                            <Text style={styles.PaymentS_TextBig}>Fname Lname </Text>
                            <Image source={require('../../../assets/images/paymentArrow.png')} style={styles.PaymentS_icon} />
                            <Text style={styles.PaymentS_TextBig}>AOT Limousine</Text>
                            <Text style={styles.PaymentS_TextPrice}>1944.00 THB</Text>
                            <Text style={styles.PaymentS_TextBig}>+XX Points</Text>
                                <TouchableOpacity style={styles.PaymemtS_button} onPress={()=> Actions.home()} >
                                    <View style={styles.enterBarcode_nextSub}>
                                        <Text style={styles.history_calendarbtnText}>DONE</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View> */}

                        <View style={{flex:0.9}}>
                            <View style={{flex:1,backgroundColor:'white', borderRadius: 20,alignItems:'center',justifyContent:'center' }}>
                                <Text style={styles.PaymentS_TextpaymentSuccess}>{this.props.typePtg ? "PTG" : "PRIVILEGE"}  SUCCESS</Text>
                                <Image  source={require('../../../assets/images/paymentSuccess.png')} style={styles.PaymentS_image} />
                                <Text style={styles.reservationSS_TextPaymentID}>Voucher Code : {this.props.code}</Text>
                                {/*<Text style={styles.reservationSS_TextBig}>Modal : {this.props.voucher.voucher.model}</Text>*/}
                                <Text style={styles.PaymentS_TextBig}>{this.props.userinfo.user_firstname+" "+this.props.userinfo.user_lastname}</Text>
                                <Image source={require('../../../assets/images/paymentArrow.png')} style={styles.PaymentS_icon} />
                                <Text style={[styles.PaymentS_TextBig, {textAlign: 'center'}]}>{this.props.typePtg ? "PTG" : this.props.voucher ? this.props.voucher.title : ""}</Text>
                                {/*<Text style={styles.PaymentS_TextPrice}>{this.props.voucher.voucher.total}.00 THB</Text>*/}
                                {/*<Text style={styles.PaymentS_TextBig}>{this.props.voucher.voucher.point_used} Points</Text>*/}
                                <TouchableOpacity style={styles.PaymemtS_button} onPress={()=> Actions.home()} >
                                    <View style={styles.enterBarcode_nextSub}>
                                        <Text style={styles.history_calendarbtnText}>DONE</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}



const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        voucher:state.voucher.voucher,
        useVoucher:state.voucher.usevoucher,
        userinfo:state.user.userinfo
    }
}
export default connect(mapStateToProps,actions)(PrivilegeSuccess)

