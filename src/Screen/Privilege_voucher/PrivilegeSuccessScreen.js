import React, { Component } from "react"

import {View, Text, TouchableOpacity, Image, Modal, ScrollView, Dimensions} from 'react-native';
import { Container, Header, Content, Icon, Picker, Form , Item , Input} from "native-base";
import { connect } from "react-redux"
import * as actions from "../../Actions"
import { Actions } from "react-native-router-flux"
import styles from '../../Style/style'
import I18n from '../../../assets/languages/i18n'
import HeaderComponent from '../../Component/HeaderComponent'
import UnderlineComponent from '../../Component/UnderlineComponent'
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment'
import HTML from 'react-native-render-html';
import {SUB_FONT} from '../../Style/font';
import FooterComponent from '../../Component/FooterComponent';
import AsyncStorage from '@react-native-community/async-storage';
const Dim = Dimensions.get("window");

class PrivilegeSuccessScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            branch_id: null,
            userinfo:'',
            image_cover:'',
            image_thumbnail:''
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (props.voucher) {
          return {
            voucher: props.voucher,
          };
        }
        return null;
      }
    async componentDidMount() {
        let branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
    }

    render() {
        console.log(this.props.voucher);
        var used_at_date = moment(this.props.voucher.redeem_start).format('DD/MM/YYYY HH:mm:ss');
        var used_at_time = moment(this.props.voucher.redeem_end).format('DD/MM/YYYY HH:mm:ss');
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true}/>
                    <View style={{flex: 1, padding: Dim.width * 0.05}}>
                        <Modal
                        transparent={true}
                        visible={this.state.show}
                        >
                            <View style={styles.Privilege_modalContainer}>
                                <View style={styles.home_container}>
                                    <TouchableOpacity onPress={()=>{this.setState({show:false})}} style={{height:'10%',alignItems:'flex-end',justifyContent:'flex-end'}}>
                                        <Image source={require('../../../assets/images/closeModal.png')}
                                        style={styles.Privilege_modalClose} />
                                    </TouchableOpacity>
                                    <View style={styles.Privilege_modalBox}>
                                        <View style={{
                                            borderTopStartRadius: 20,
                                            borderTopEndRadius: 20,
                                            overflow: 'hidden',
                                        }}>
                                            <Image source={{uri: this.props.voucher.image_cover}}
                                            style={styles.Privilege_modaloff}  />
                                        </View>
                                        <View style={{flex:1}}>
                                            <ScrollView style={[styles.Privilege_modalScrollContainer,{padding: Dim.width * 0.06}]}>
                                                <Text style={{
                                                    fontSize:Dim.width/380*22,
                                                    fontFamily:SUB_FONT,
                                                    color:'#000000',
                                                    fontWeight: "500",
                                                }}>{this.props.voucher.title} </Text>
                                                <Text style={styles.Privilege_modalTextH2}>{I18n.t('privilege_success.period')} {used_at_date} - {used_at_time} </Text>
                                                <Text style={styles.Privilege_modalTextH2sub}>{I18n.t('privilege_success.modal_title1')}</Text>
                                                <HTML
                                                    html={this.props.voucher.condition_description}
                                                    // imagesMaxWidth={width - 40}
                                                    addLineBreaks={false}
                                                />
                                                {/*<Text style={styles.Privilege_modalTextNormal}>{}</Text>*/}
                                            </ScrollView>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            {/* <View style={styles.Privilege_modalContainer}>
                                <View style={styles.home_container}>
                                    <TouchableOpacity onPress={()=>{this.setState({show:false})}} style={{height:'10%',alignItems:'flex-end',justifyContent:'flex-end'}}>
                                        <Image source={require('../../../assets/images/closeModal.png')}
                                        style={styles.Privilege_modalClose} />
                                    </TouchableOpacity>
                                    <View style={styles.Privilege_modalBox}>
                                        <View style={{flex:0.25,justifyContent:'flex-end'}}>

                                         <Image source={require('../../../assets/images/Privilege_offDetail.png')}
                                            style={styles.Privilege_modaloff}  />

                                        </View>
                                        <View style={{flex:0.75}}>
                                            <View style={styles.Privilege_modalScrollContainer}>

                                                <ScrollView>
                                                    <Text style={styles.Privilege_modalTextH1}>title} </Text>
                                                    <Text style={styles.Privilege_modalTextH2}>Period : redeem_start} - redeem_end} </Text>
                                                    <Text style={styles.Privilege_modalTextH2sub}>Privilege Detail</Text>
                                                    <Text style={styles.Privilege_modalTextNormal}>condition_description}</Text>
                                                </ScrollView>

                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View> */}

                        </Modal>
                        <Text style={styles.PaymentS_TextpaymentSuccess}>{I18n.t('privilege_success.title1')}</Text>

                        <View style={{flex: 1, backgroundColor: "#FFFFFF", borderRadius: Dim.width * 0.05, padding: Dim.width * 0.04}}>
                            <Text style={{ fontFamily:SUB_FONT, fontSize: 18, fontWeight:'600' }}>{this.props.userinfo.user_firstname+" "+this.props.userinfo.user_lastname}</Text>
                            <View style={{height: Dim.height * 0.13, marginTop: Dim.height * 0.02}}>
                                {
                                    this.props.voucher.image_cover
                                    ?   <Image source={{uri: this.props.voucher.image_cover}} style={styles.Privilege_off} />
                                    :   null
                                 }
                            </View>
                            <Text style={{fontSize:Dim.width/380*16,fontFamily:SUB_FONT,color:'#9C9C9C',marginTop: Dim.height * 0.01}}>{this.props.voucher.title}</Text>
                            <TouchableOpacity style={{borderColor: "#43B7E8", borderWidth: 1, height: Dim.height * 0.05, borderRadius: Dim.width * 0.05, alignItems: "center", justifyContent: "center", width: Dim.width * 0.4, alignSelf: "center", marginTop: Dim.height * 0.03}} onPress={()=>{this.setState({show:true})}} >
                                <Text style={styles.Privilege_buttonText}>{I18n.t('privilege_success.detail_1')}</Text>
                            </TouchableOpacity>
                            <View style={{alignItems: "center"}}>
                                <Image source={require('../../../assets/images/Privilege_Barcode.png')}
                                       style={[styles.Privilege_Barcode]} />
                                <Text style={[styles.enterBarcode_text]}>{this.props.voucher._id}</Text>
                            </View>
                            <TouchableOpacity style={[styles.Privilege_DoneButton]} onPress={()=> {
                                this.props.useMerchantVoucher(this.props.code, this.state.branch_id ? this.state.branch_id : this.props.userinfo.branch_id)
                                // Actions.payment_success()
                            }} >
                                <Text style={styles.textButton}>{I18n.t('privilege_success.done')}</Text>
                            </TouchableOpacity>
                        </View>

                        {/*<View style={{flex:0.9,paddingTop:'5%'}}>*/}
                        {/*    <View style={ styles.PrivilegeSbox}>*/}
                        {/*        <View style={styles.Privilege_box}>*/}
                        {/*            <Text style={styles.enterBarcode_text}>this.props.userinfo.user_firstname/!*this.props.userinfo.user_firstname+" "+this.props.userinfo.user_lastname*!/</Text>*/}


                        {/*            <Text style={styles.Privilege_textoff}>this.props.voucher.title/!*this.props.voucher.title*!/</Text>*/}
                        {/*            <View style={{alignItems:'center'}}>*/}
                        {/*                <TouchableOpacity style={styles.Privilege_button} onPress={()=>{this.setState({show:true})}} >*/}
                        {/*                    <Text style={styles.Privilege_buttonText}>{I18n.t('privilege_success.detail_1')}</Text>*/}
                        {/*                </TouchableOpacity>*/}
                        {/*                <Image source={require('../../../assets/images/Privilege_Barcode.png')}*/}
                        {/*                style={[styles.Privilege_Barcode]} />*/}
                        {/*                <Text style={[styles.enterBarcode_text]}>/!*this.props.voucher._id*!/</Text>*/}

                        {/*                <TouchableOpacity style={[styles.Privilege_DoneButton]} onPress={()=> {*/}
                        {/*                    console.log("this.props.voucher.voucher_id", this.props.voucher._id);*/}
                        {/*                    console.log("this.props.userinfo.branch_id", this.props.userinfo.branch_id);*/}
                        {/*                    this.props.useMerchantVoucher(this.props.code,this.props.userinfo.branch_id)*/}
                        {/*                    // Actions.payment_success()*/}
                        {/*                }} >*/}
                        {/*                    <Text style={styles.textButton}>{I18n.t('privilege_success.done')}</Text>*/}
                        {/*                </TouchableOpacity>*/}
                        {/*            </View>*/}
                        {/*        </View>*/}
                        {/*    </View>*/}
                        {/*</View>*/}

                    <UnderlineComponent leftbtn={false} />
                        <View style={styles.scan_underlineContainer}>
                            <View style={{flex: 1}}>
                                <Text style={styles.Underline_textAL}>{this.props.userinfo.merchant_name}</Text>
                                <Text style={styles.Underline_textAIR}>{this.props.userinfo.branch_name}</Text>
                            </View>
                            {/* <View style={{flex:0.2,alignItems:'flex-end',flexDirection:'column-reverse'}}>
                        <TouchableOpacity onPress={()=> Actions.pop()}><Image source={require('../../../assets/images/Underline.png')} style={{width:30,height:30}} /></TouchableOpacity>
                    </View> */}
                        </View>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}



const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        voucher:state.voucher.voucher,
        userinfo:state.user.userinfo
    }
  }
export default connect(mapStateToProps,actions)(PrivilegeSuccessScreen)

