import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image, RefreshControl, SectionList, ScrollView, Dimensions} from 'react-native';
import {Container, Content, Tab, Tabs, Footer, Header, Left, Icon, Button, Title, Body, Right} from 'native-base';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import LinearGradient from 'react-native-linear-gradient';
import axios from 'axios';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';
import AsyncStorage from '@react-native-community/async-storage';

import moment from 'moment';
import FooterComponent from '../../Component/FooterComponent';
const Dim = Dimensions.get("window");

class HistoryScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            person: null,
            show1: true,
            AppBanner: '',
            branch_id: null,
            moreCheck: true,
            checkVoucher: false,

            payment_history: [],
            voucher_history: [],
            payment_history_all: [],
            voucher_history_all: [],
            payment_history_num: [],
            voucher_history_num: [],

            tabs_index: 0,
        };
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;

    };

    hideMenu(type) {
        this._menu.hide();
        var list_all = this.state.payment_history_all;
        if (this.state.tabs_index == 0) {
            var list_all = this.state.payment_history_all;
        } else {
            var list_all = this.state.voucher_history_all;
        }
        var list = [];
        if (type === 'more') {
            Actions.history_moredata();
        } else if (type === 'all') {
            this.setState({
                payment_history: this.state.payment_history_all
            });
            this.props.getVoucherHistory(this.state.branch_id, "", "");

        } else if (type === 'today') {
            let now = new Date();
            var sd = moment(now).format('YYYY-MM-DD 00:00:00');
            var ed = moment(now).format('YYYY-MM-DD 23:59:59');
            console.log(sd, ed);
            if (this.state.tabs_index == 0) {
                const {payment_history_all} = this.state;
                let list = [];
                for (let i = 0; i < payment_history_all.length; i ++) {
                    let dataDate =  moment(payment_history_all[i].created_at).format('YYYY-MM-DD 00:00:00');
                    if (dataDate === sd){
                        list.push(payment_history_all[i]);
                    }
                }
                this.setState({
                    payment_history: list,
                });
            } else {
                this.props.getVoucherHistory(this.state.branch_id, sd, ed);
            }
        } else if (type == 'yesterday') {
            let now = new Date();
            var sd = moment(now).add(-1, 'days').format('YYYY-MM-DD 00:00:00');
            var ed = moment(now).add(-1, 'days').format('YYYY-MM-DD 23:59:59');
            if (this.state.tabs_index === 0) {
                const {payment_history_all} = this.state;
                let list = [];
                for (let i = 0; i < payment_history_all.length; i ++) {
                    let dataDate =  moment(payment_history_all[i].created_at).format('YYYY-MM-DD 00:00:00');
                    if (dataDate === sd){
                        list.push(payment_history_all[i]);
                    }
                }
                this.setState({
                    payment_history: list,
                });
            } else {
                this.props.getVoucherHistory(this.state.branch_id, sd, ed);
            }
        }
    };

    showMenu = () => {
        this._menu.show();
        this.setState({
            show1: false,
        });
    };

    async componentDidMount() {
        this.importAppBanner();
        // this.setState({person: data.results[0], loading: false })
        this.setState({loading: false});
        var branch_id = await AsyncStorage.getItem('branch_id');
        this.setState({branch_id: branch_id});
        // 269
        let now = new Date();
        var sd = moment(now).format('YYYY-MM-DD 00:00:00');
        var ed = moment(now).format('YYYY-MM-DD 23:59:59');
        // console.log(date)
        this.props.getPaymentHistory(branch_id);
        this.props.getVoucherHistory(branch_id, "", "");
    }

    static getDerivedStateFromProps(props, state) {
        if (props.payment_history && props.payment_history !== state.payment_history && state.moreCheck) {
            return {
                payment_history: props.payment_history,
                payment_history_all: props.payment_history,
                payment_history_num: props.payment_history_num,
                moreCheck: false,
            };
        }
        if (props.voucher_history && props.voucher_history !== state.voucher_history) {
            return {
                voucher_history: props.voucher_history,
                voucher_history_all: props.voucher_history,
                voucher_history_num: props.voucher_history_num,
                checkVoucher: false,
            };
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.dateTimeCheck !== prevProps.dateTimeCheck){
            let startDate = this.props.dateTime.startDate;
            let endDate = this.props.dateTime.endDate;
            if (this.state.tabs_index === 0) {
                const {payment_history_all} = this.state;
                let list = [];
                for (let i = 0; i < payment_history_all.length; i ++) {
                    let dataDate =  moment(payment_history_all[i].created_at).format('YYYY-MM-DD 00:00:00');
                    if (dataDate >= startDate && dataDate <= endDate){
                        list.push(payment_history_all[i]);
                    }
                }
                this.setState({
                    payment_history: list,
                });
            } else {
                console.log(this.state.branch_id, startDate, endDate)
                this.props.getVoucherHistory(this.state.branch_id, startDate, endDate);
            }
        }
    }

    async importAppBanner() {
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: JSON.stringify({query: `query {  getMerchantAppBanner {    small    large  }}`}),
        })
            .then(response => response.json())
            .then(data => this.setState({AppBanner: data.data.getMerchantAppBanner.small}));
    }

    onRefreshPayment() {
        this.setState({
            payment_history: [],
            payment_history_all: [],
            payment_history_num: [],
            moreCheck: true,
        },() => {
            this.props.getPaymentHistory(this.state.branch_id);
        })
    };

    onRefreshVoucher() {
        this.setState({
            voucher_history: [],
            voucher_history_all: [],
            voucher_history_num: [],
            checkVoucher: true,
        },() => {
            this.props.getVoucherHistory(this.state.branch_id, "", "");
        })
    };

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Header transparent>
                            <Left style={styles.Header_LeftP}>
                                <Button transparent onPress={()=> Actions.pop()}>
                                    <Icon name='arrow-back' style={{color:'#43B7E8'}} />
                                </Button>
                            </Left>
                            <Body style={{justifyContent:'center',alignItems:'center',flex:1}}>
                                <Title style={styles.Header_TitleText}>{I18n.t('header.history')}</Title>
                            </Body>
                            <Right style={styles.Header_RightP}>
                                <Button transparent onPress={()=> Actions.home()}>
                                    <Image source={require('../../../assets/images/headerHome.png')}
                                           style={styles.Header_AOT_iconRight}  />
                                </Button>
                                <Button transparent onPress={this.showMenu}>
                                    <Image source={require('../../../assets/images/dropdown.png')}
                                           style={styles.Header_AOT_iconRight}  />
                                </Button>
                                <Menu ref={this.setMenuRef}>
                                    <MenuItem onPress={() => this.hideMenu('all')}>{I18n.t('date.all')}</MenuItem>
                                    <MenuDivider/>
                                    <MenuItem onPress={() => this.hideMenu('today')}>{I18n.t('date.today')}</MenuItem>
                                    <MenuDivider/>
                                    <MenuItem
                                        onPress={() => this.hideMenu('yesterday')}>{I18n.t('date.yesterday')}</MenuItem>
                                    <MenuDivider/>
                                    <MenuItem
                                        onPress={() => this.hideMenu('more')}>{I18n.t('date.select_date')}</MenuItem>
                                </Menu>
                            </Right>

                        </Header>
                        {/*<HeaderComponent home={true} leftbtn={true} title={true} history={true} Dropdown={true}/>*/}
                        <Tabs onChangeTab={({i}) => this.setState({tabs_index: i})}>
                            <Tab heading={I18n.t('header.payment')}>
                                <ScrollView
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.moreCheck}
                                            onRefresh={() => this.onRefreshPayment()}
                                            // colors={'#2B3D90'} //android
                                            tintColor={'#448FEF'}
                                            progressBackgroundColor={'#448FEF'}
                                        />
                                    }
                                >
                                    {
                                        this.state.payment_history.map((item, index) => {
                                            return (
                                                <TouchableOpacity key={index} style={styles.item}
                                                                  onPress={() => Actions.history_detail({data: item})}>
                                                    <View style={styles.hty0}>
                                                        {/*
                                                            this.state.loading || !this.state.person ?
                                                                <Text style={styles.hty2text_title1}>Loading</Text> :
                                                                <View style={styles.hty2}>
                                                                    <Text
                                                                        style={styles.hty2text_title1}>{item.user_firstname + ' ' + item.user_lastname}</Text>
                                                                    <Text
                                                                        style={styles.hty2text_title2}>{`${item.price} THB `}</Text>
                                                                </View>
                                                        */}
                                                        <View style={styles.hty2}>
                                                            <Text style={styles.hty2text_title1}>{item.user.user_title_name} {item.user.user_firstname} {item.user.user_lastname}</Text>
                                                            <Text style={styles.hty2text_title2}>{`${parseFloat(item.price).toLocaleString(undefined, {minimumFractionDigits: 2})} THB `}</Text>
                                                        </View>

                                                        <View style={[styles.hty2]}>

                                                            <View
                                                                style={[styles.hty3]}>
                                                                <Text
                                                                    style={styles.hty2text_title3}>{`ID: ${item.order_number}`}  </Text>
                                                            </View>
                                                            <View style={[styles.hty3]}>
                                                                <Text
                                                                    style={styles.hty2text_title3}>{moment(item.created_at).format('DD-MM-YYYY HH:mm:ss')}</Text>
                                                            </View>
                                                            <View
                                                                style={[styles.hty3]}>
                                                                <Text style={styles.hty2text_title4}>{item.status}</Text>
                                                            </View>

                                                        </View>
                                                    </View>
                                                    <View style={styles.hty1}>
                                                        <Image source={require('../../../assets/images/arrow.png')}
                                                               style={styles.htyImg1}/>
                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                </ScrollView>
                            </Tab>
                            <Tab heading={I18n.t('header.voucher')}>
                                <ScrollView
                                    refreshControl={
                                        <RefreshControl
                                            refreshing={this.state.checkVoucher}
                                            onRefresh={() => this.onRefreshVoucher()}
                                            // colors={'#2B3D90'} //android
                                            tintColor={'#448FEF'}
                                            progressBackgroundColor={'#448FEF'}
                                        />
                                    }
                                >
                                    {
                                        this.state.voucher_history.map((item, index) => {
                                            return (
                                                <TouchableOpacity key={index} style={styles.itemVoucher} onPress={() => Actions.history_detail_voucher({data: item})}>
                                                    <View style={{width: Dim.width * 0.55, paddingHorizontal: Dim.width * 0.02}}>
                                                        <Text style={styles.hty2text_title1}>{item.customer_first_name + ' ' + item.customer_last_name}</Text>
                                                        <Text style={[styles.hty2text_titleV2]}>{item.voucher_name}</Text>
                                                        <Text style={styles.hty2text_title3}>{`ID: ${item.transaction_id}`}</Text>
                                                        <Text style={styles.hty2text_title3}>{item.time_used_at}</Text>
                                                    </View>
                                                    <View style={{width: Dim.width * 0.35, justifyContent: 'center', alignItems: 'flex-end', paddingHorizontal: Dim.width * 0.02}}>
                                                        <Text style={[styles.hty2text_title1, {color: '#000000'}]}>{item.data.total ? parseInt(item.data.total).toLocaleString() + ' THB' : ''}</Text>
                                                        <Text style={styles.hty2text_title4}>{item.status}</Text>
                                                    </View>
                                                    <View style={{width: Dim.width * 0.1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: Dim.width * 0.02}}>
                                                        <Image source={require('../../../assets/images/arrow.png')} style={styles.htyImg1}/>
                                                    </View>
                                                </TouchableOpacity>
                                                // <TouchableOpacity key={index} style={styles.itemVoucher}
                                                //                   onPress={() => Actions.history_detail_voucher({data: item})}>
                                                //     <View style={[styles.hty0, {borderWidth: 1, borderColor: 'red'}]}>
                                                //         <View style={styles.htyV2}>
                                                //             <Text
                                                //                 style={styles.hty2text_title1}>{item.customer_first_name + ' ' + item.customer_last_name}</Text>
                                                //             <Text numberOfLines={1}
                                                //                   style={[styles.hty2text_titleV2, {marginTop: 5}]}>{item.voucher_name}</Text>
                                                //         </View>
                                                //         <View style={styles.hty2}>
                                                //             <View style={styles.hty3}>
                                                //                 <Text
                                                //                     style={styles.hty2text_title3}>{`ID: ${item.transaction_id}`}</Text>
                                                //                 <Text
                                                //                     style={styles.hty2text_title3}>{item.time_used_at}</Text>
                                                //             </View>
                                                //             <Text style={styles.hty2text_title4}>{item.status}</Text>
                                                //         </View>
                                                //     </View>
                                                //     <View style={styles.hty1}>
                                                //         <Image source={require('../../../assets/images/arrow.png')}
                                                //                style={styles.htyImg1}/>
                                                //     </View>
                                                // </TouchableOpacity>
                                            );
                                        })
                                    }
                                </ScrollView>
                            </Tab>
                        </Tabs>
                        <Footer style={styles.history_footerContainer}>
                            <View style={{height: '100%', width: '100%', alignContent: 'flex-end'}}>
                                <View style={{
                                    height: '80%',
                                    width: '100%',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Image source={{uri: this.state.AppBanner}} style={styles.history_ads_banner}/>
                                </View>
                                <Text style={styles.history_footertextAL}>{}</Text>
                                <Text style={styles.history_footerAIR}>{this.props.userinfo ? this.props.userinfo.branch_name : ""}</Text>
                            </View>
                        </Footer>

                        <View style={styles.DropdownPopupDropdown}>
                            {/*<View style={{width: 1, height: 1, paddingRight: 3, backgroundColor: 'red'}}>*/}

                            {/*</View>*/}
                            {/*<TouchableOpacity style={styles.DropdownPopup} onPress={() => this.showMenu()}>*/}
                            {/*    <Image source={require('../../../assets/images/dropdown.png')}*/}
                            {/*           style={styles.Header_AOT_iconRight}/>*/}
                                {/* <Image source={require('../../../assets/images/dropdown.png')}
                                style={styles.Header_AOT_iconRight1111}  /> */}
                            {/*</TouchableOpacity>*/}
                        </View>
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        payment_history: state.history.payment,
        voucher_history: state.history.voucher,
        payment_history_num: state.history.history_num,
        voucher_history_num: state.history.voucher_num,
        userinfo: state.user.userinfo,
        dateTime: state.history.dateTime,
        dateTimeCheck: state.history.dateTimeCheck,
    };
};
export default connect(mapStateToProps, actions)(HistoryScreen);
