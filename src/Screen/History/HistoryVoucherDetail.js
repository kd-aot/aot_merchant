import React, { Component } from "react";
import { View,TouchableOpacity,Text } from "react-native";
import { connect } from "react-redux";
import * as actions from "../../Actions";
import { Actions } from "react-native-router-flux";
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import { Container } from "native-base";
import LinearGradient from 'react-native-linear-gradient';
import UnderlineComponent from '../../Component/UnderlineComponent'
import FooterComponent from '../../Component/FooterComponent';

class HistoryVoucherDetail extends Component {
    constructor(props){
        super(props)
    }
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} historyVoucher={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9}}>
                            <View style={styles.history_detailBox}>
                                <Text style={styles.history_detailText}>HS3211234</Text>
                                    <View style={styles.history_detailbox1}>
                                        <Text style={styles.history_detailText1}>Datetime</Text>
                                        <View style={styles.history_detailbox2}>
                                            <Text style={styles.history_detailText2}>6/6/2222</Text>
                                            <Text style={styles.history_detailText2}>13:12:32</Text>
                                        </View>
                                    </View>
                                    <View style={styles.history_detailbox1}>
                                        <Text style={styles.history_detailText1}>Status</Text>
                                        <Text style={styles.history_detailText1}>Status:differentCOLOR</Text>
                                    </View>
                                    <View style={styles.history_detail_liner} />
                                    <View style={styles.history_detailbox3}>
                                        <View style={styles.history_detailbox4}>
                                            <Text style={styles.history_detailText3}>Cname Lname</Text>
                                        </View>
                                        <View style={styles.history_detailbox4}>
                                            <Text style={styles.history_detailTextV3}>Free All Beverages</Text>
                                        </View>
                                        <View style={styles.history_detailboxV4}>
                                            <Text style={styles.history_detailText1}>Voucher Used</Text>
                                            <Text style={styles.history_detailText6}>100% Discount</Text>
                                        </View>
                                        <View style={styles.history_detailboxV5}>
                                            <Text style={styles.history_detailText2}>Code : ABC123456789</Text>
                                        </View>
                                    </View>
                                    <View style={styles.history_detail_liner} />
                                    <View style={styles.history_detailbox4}>
                                        <Text style={styles.history_detailText1}>To </Text>
                                        <Text style={styles.history_detailText6}>AOT Limousine</Text>
                                    </View>
                            </View>
                        </View>
                    <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(HistoryVoucherDetail)

