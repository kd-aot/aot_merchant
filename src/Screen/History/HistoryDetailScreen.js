import React, {Component} from 'react';
import {View, TouchableOpacity, Text, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import HeaderComponent from '../../Component/HeaderComponent';
import {Container} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import UnderlineComponent from '../../Component/UnderlineComponent';
import moment from 'moment';
import FooterComponent from '../../Component/FooterComponent';
const Dim = Dimensions.get("window");

class HistoryDetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        console.log(this.props.data);
        // var branch_id = AsyncStorage.getItem('branch_id')
        // this.props.getHistoryPaymentList(branch_id,this.props.data.order_number)
    }

    render() {
        console.warn('data', this.props.data);
        var used_at_date = this.props.data ? moment(this.props.data.created_at).format('DD-MM-YYYY HH:mm:ss') : "";
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} history={true}/>
                    <View style={{flex: 1, padding: Dim.width * 0.05}}>
                        <View style={{backgroundColor: '#FFFFFF', borderRadius: Dim.width * 0.05, padding: Dim.width * 0.05}}>
                            <View style={{borderBottomWidth: 1, borderColor: '#C4C4C6', paddingBottom: Dim.height * 0.02}}>
                                <Text style={[styles.history_detailText, {marginBottom: Dim.height * 0.01}]}>{this.props.data ? this.props.data.order_number : ""}</Text>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: Dim.height * 0.003}}>
                                    <Text style={styles.history_detailText1}>Datetime</Text>
                                    <Text style={styles.history_detailText2}>{used_at_date}</Text>
                                </View>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', marginVertical: Dim.height * 0.003}}>
                                    <Text style={[styles.history_detailText1]}>Status</Text>
                                    <Text style={[styles.history_detailText, {color: this.props.data ? this.props.data.status === 'SUCCESS' ? '#43B7E8' : 'red' : 'red'}]}>{this.props.data ? this.props.data.status : ''}</Text>
                                </View>
                            </View>
                            <View style={{borderBottomWidth: 1, borderColor: '#C4C4C6', paddingVertical: Dim.height * 0.02}}>
                                <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                                    <View style={{width: '53%'}}>
                                        <Text style={styles.history_detailText3}>{this.props.data ? this.props.data.user.user_firstname + ' ' + this.props.data.user.user_lastname: ""}</Text>
                                    </View>
                                    <View style={{width: '40%', justifyContent: 'center', alignItems: 'flex-end'}}>
                                        <Text style={styles.history_detailText4}>{this.props.data ? parseFloat(this.props.data.total).toLocaleString(undefined, {minimumFractionDigits: 2}) : 0} THB</Text>
                                    </View>
                                </View>
                                <View style={{alignItems: 'flex-end', marginTop: Dim.width * 0.01}}>
                                    <Text style={styles.history_detailText2}>Credit Card : {this.props.data ? this.props.data.payment.masked_pan : ''}</Text>
                                </View>
                                <View style={{marginTop: Dim.width * 0.07}}>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                        <View style={{width: '60%', alignItems: 'flex-end', justifyContent: 'center'}}>
                                            <Text style={styles.history_detailText2}>Price (Exclude VAT)</Text>
                                        </View>
                                        <View style={{width: '40%', alignItems: 'flex-end', justifyContent: 'center'}}>
                                            <Text style={styles.history_detailText5}>{this.props.data ? parseFloat(this.props.data.amount ? this.props.data.amount : this.props.data.price).toFixed(2).toLocaleString(undefined, {minimumFractionDigits: 2}) : 0} THB</Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                        <View style={{width: '60%', alignItems: 'flex-end', justifyContent: 'center'}}>
                                            <Text style={styles.history_detailText2}>VAT</Text>
                                        </View>
                                        <View style={{width: '40%', alignItems: 'flex-end', justifyContent: 'center'}}>
                                            <Text style={styles.history_detailText5}>{this.props.data ? parseFloat(this.props.data.vat).toFixed(2).toLocaleString(undefined, {minimumFractionDigits: 2}) : 0} THB</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{marginTop: Dim.width * 0.07}}>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                        <Text style={styles.history_detailText1}>AOT Point Used</Text>
                                        <Text style={styles.history_detailText5}>{this.props.data ? this.props.data.point_used : 0} AOT Points</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{paddingVertical: Dim.height * 0.02}}>
                                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                                    <Text style={styles.history_detailText1}>To</Text>
                                    <Text style={styles.history_detailText5}>{this.props.userinfo ? this.props.userinfo.branch_name : ""}</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                    <View style={{position: 'absolute', width: Dim.width, bottom: Dim.height * 0.05, padding: 10}}>
                        <UnderlineComponent leftbtn={false} branch_name={this.props.userinfo ? this.props.userinfo.branch_name : ''}
                                            merchant_name={this.props.userinfo ? this.props.userinfo.merchant_name: ''}/>
                    </View>
                    {/*<View style={styles.home_container}>
                        <View style={{flex: 0.9}}>
                            <View style={styles.history_detailBox}>
                                <Text style={styles.history_detailText}>{this.props.data.order_number}</Text>
                                <View style={styles.history_detailbox1}>
                                    <Text style={styles.history_detailText1}>Datetime</Text>
                                    <View style={styles.history_detailbox2}>
                                        <Text style={styles.history_detailText2}>{used_at_date}</Text>

                                        // <Text style={styles.history_detailText2}>{used_at_time}</Text>
                                    </View>
                                </View>
                                <View style={styles.history_detailbox1}>
                                    <Text style={styles.history_detailText1}>Status</Text>
                                    <Text
                                        style={[styles.history_detailText1, {color: this.props.data.status === 'SUCCESS' ? '#43B7E8' : 'red'}]}>{this.props.data.status}</Text>
                                </View>
                                <View style={styles.history_detail_liner}/>
                                <View style={styles.history_detailbox3}>
                                    <View style={styles.history_detailbox4}>
                                        <Text
                                            style={styles.history_detailText3}>{this.props.data.user.user_firstname + ' ' + this.props.data.user.user_lastname}</Text>
                                        <Text style={styles.history_detailText4}>{parseFloat(this.props.data.total).toLocaleString(undefined, {minimumFractionDigits: 2})} THB</Text>
                                    </View>
                                    <View style={styles.history_detailbox5}>
                                        <Text style={styles.history_detailText2}>Credit Card : {this.props.data.payment.masked_pan}</Text>
                                    </View>
                                    <View style={styles.history_detailbox6}>
                                        <View style={styles.history_detailbox7}>
                                            <View style={styles.history_detailbox8}>
                                                <Text style={styles.history_detailText1}>Price (Exclude VAT)</Text>
                                                <Text style={styles.history_detailText1}>VAT</Text>
                                            </View>
                                            <View style={styles.history_detailbox9}>
                                                <Text
                                                    style={styles.history_detailText5}>{parseFloat(this.props.data.amount ? this.props.data.amount : this.props.data.price).toFixed(2).toLocaleString(undefined, {minimumFractionDigits: 2})} THB</Text>
                                                <Text
                                                    style={styles.history_detailText5}>{parseFloat(this.props.data.vat).toFixed(2).toLocaleString(undefined, {minimumFractionDigits: 2})} THB</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.history_detailbox4}>
                                        <Text style={styles.history_detailText1}>AOT Point Used</Text>
                                        <Text style={styles.history_detailText6}>{this.props.data.point_used} AOT
                                            Points</Text>
                                    </View>
                                </View>
                                <View style={styles.history_detail_liner}/>
                                <View style={styles.history_detailbox4}>
                                    <Text style={styles.history_detailText1}>To </Text>
                                    <Text
                                        style={styles.history_detailText6}>{this.props.userinfo ? this.props.userinfo.branch_name : ""} </Text>
                                </View>
                            </View>
                        </View>
                        <UnderlineComponent leftbtn={false} branch_name={this.props.userinfo.branch_name}
                                            merchant_name={this.props.userinfo.merchant_name}/>
                    </View>*/}
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
        userinfo: state.user.userinfo,
    };
};
export default connect(mapStateToProps, actions)(HistoryDetailScreen);

