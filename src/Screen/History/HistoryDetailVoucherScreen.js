import React, { Component } from "react";
import { View,TouchableOpacity,Text } from "react-native";
import { connect } from "react-redux";
import * as actions from "../../Actions";
import { Actions } from "react-native-router-flux";
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import HeaderComponent from '../../Component/HeaderComponent';
import { Container } from "native-base";
import LinearGradient from 'react-native-linear-gradient';
import UnderlineComponent from '../../Component/UnderlineComponent'
import moment from 'moment'
import FooterComponent from '../../Component/FooterComponent';
class HistoryDetailVoucherScreen extends Component {
    constructor(props){
        super(props)
    }



    render() {
        console.warn('data',this.props.data)
        var used_at_date = moment(this.props.data.used_at, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY')
        var used_at_time = moment(this.props.data.used_at, 'YYYY-MM-DD HH:mm:ss').format('HH:mm:ss')

        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} historyVoucher={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9}}>
                            <View style={styles.history_detailBoxV}>
                                <Text style={styles.history_detailText}>{this.props.data.transaction_id}</Text>
                                    <View style={styles.history_detailbox1}>
                                        <Text style={styles.history_detailText1}>Datetime</Text>
                                        <View style={styles.history_detailbox2}>
                                            <Text style={styles.history_detailText2}>{used_at_date}</Text>
                                            <Text style={styles.history_detailText2}>{used_at_time}</Text>
                                        </View>
                                    </View>
                                    <View style={styles.history_detailbox1}>
                                        <Text style={styles.history_detailText1}>Status</Text>
                                        <Text style={[styles.history_detailText1,{color:this.props.data.status == 'Success'?'#43B7E8':'red'}]}>{this.props.data.status}</Text>
                                    </View>
                                    <View style={styles.history_detail_liner} />
                                        <View style={{paddingVertical:10}}>
                                            <Text style={[styles.hty2text_title1]}>{this.props.data.customer_first_name+' '+this.props.data.customer_last_name}</Text>
                                            <Text style={styles.history_detailText}>{this.props.data.data.voucher_name}</Text>
                                            <View style={{flexDirection:'row'}}>
                                                <Text style={[styles.history_detailText1,{flex:1}]}>Voucher Used</Text>
                                                <Text style={[styles.history_detailText1,{color:this.props.data.status == 'Success'?'#43B7E8':'red',flex:1,textAlign:'right'}]}>{this.props.data.data.point_used+' point'}</Text>
                                            </View>
                                            <Text style={[styles.history_detailText1]}>{'Code: '+this.props.data.data.voucher_code}</Text>
                                        </View>
                                    {/* <View style={styles.history_detailbox3}>
                                        <View style={styles.history_detailbox4}>
                                            <Text style={styles.history_detailText3}>{this.props.data.customer_first_name+' '+this.props.data.customer_last_name}</Text>
                                            <Text style={styles.history_detailText4}>{`${this.props.data.data.price_used} THB`}</Text>
                                        </View>

                                        <View style={styles.history_detailbox6}>
                                            <View style={styles.history_detailbox7}>
                                                <View style={styles.history_detailbox8}>
                                                    <Text style={styles.history_detailText1}>Price (Exclude VAT)</Text>
                                                    <Text style={styles.history_detailText1}>VAT</Text>
                                                </View>
                                                <View style={styles.history_detailbox9}>
                                                    <Text style={styles.history_detailText5}>1,855.14 THB</Text>
                                                    <Text style={styles.history_detailText5}>129.14 THB</Text>
                                                </View>
                                            </View>
                                        </View>
                                        <View style={styles.history_detailbox4}>
                                                <Text style={styles.history_detailText1}>Voucher Used</Text>
                                                <Text style={styles.history_detailText6}>80 AOT Points</Text>
                                        </View>
                                        <View style={styles.history_detailboxV5}>
                                            <Text style={styles.history_detailText2}>Code : ABC123456789</Text>
                                            <Text style={styles.history_detailText5}>-16 THB</Text>
                                        </View>
                                    </View> */}
                                    {/* <View style={styles.history_detailboxV6}>
                                        <Text style={styles.history_detailTextV1}>Total</Text>
                                        <Text style={styles.history_detailTextV2}>143 THB</Text>
                                    </View>
                                    <View style={styles.history_detailbox5}>
                                        <Text style={styles.history_detailText2}>Credit Card : XXXX 3823</Text>
                                    </View> */}
                                    <View style={styles.history_detail_liner} />
                                    <View style={styles.history_detailbox4}>
                                        <Text style={styles.history_detailText1}>To </Text>
                                        <Text style={styles.history_detailText6}>{this.props.data.data.to_branch_name}</Text>
                                    </View>
                            </View>
                        </View>
                    <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone
    }
  }
export default connect(mapStateToProps,actions)(HistoryDetailVoucherScreen)

