import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import * as actions from '../../Actions';
import {Actions} from 'react-native-router-flux';
import styles from '../../Style/style';
import I18n from '../../../assets/languages/i18n';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import HeaderComponent from '../../Component/HeaderComponent';
import {Container, Icon} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import FooterComponent from '../../Component/FooterComponent';
import DateTimePickerModal from "react-native-modal-datetime-picker";

const Dim = Dimensions.get('window');

class HistoryMoredataScreen extends Component {
    a = true;

    constructor(props) {
        super(props);
        this.state = {
            startDate: moment().format("YYYY-MM-DD 00:00:00"),
            endDate: moment().format("YYYY-MM-DD 00:00:00"),
            markDate: {},
            isDatePickerVisibleFrom: false,
            isDatePickerVisibleTo: false,
            setDatePickerVisibility: false,
        };
    }

    componentDidMount() {
        var now_date = new Date();
        var now_date_str = moment(now_date).format('YYYY-MM-DD');
        var value = {};
        value[now_date_str] = {
            disabled: true,
            startingDay: true,
            color: '#43B7E8',
            endingDay: true,
            textColor: '#ffffff',
        };

        this.setState({
            startDate: now_date_str,
            endDate: now_date_str,
            markDate: value,
        });
    }

    onSelectDate(date) {
        console.log('day',date)
        if (this.state.startDate === '' && this.state.endDate === '') {
            var value = {};
            value[date.dateString] = {
                disabled: true,
                startingDay: true,
                color: '#43B7E8',
                endingDay: true,
                textColor: '#ffffff',
            };
            this.setState({startDate: date.dateString, markDate: value});
        } else if (this.state.startDate !== '' && this.state.endDate === '') {
            var date1 = moment(this.state.startDate, 'YYYY-MM-DD');
            var date2 = moment(date.dateString, 'YYYY-MM-DD');
            if (date2 < date1) {
                var value = {};
                value[date.dateString] = {
                    disabled: true,
                    startingDay: true,
                    color: '#43B7E8',
                    endingDay: true,
                    textColor: '#ffffff',
                };
                this.setState({startDate: date.dateString, markDate: value});
            } else {
                var value = {};
                value[this.state.startDate] = {
                    disabled: true,
                    startingDay: true,
                    color: '#43B7E8',
                    textColor: '#ffffff',
                };
                var diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                var currentDate = moment(this.state.startDate, 'YYYY-MM-DD');

                for (var i = 1; i < diffDays; i++) {
                    currentDate = moment(currentDate).add(1, 'days');
                    var srt_currentDate = moment(currentDate).format('YYYY-MM-DD');
                    value[srt_currentDate] = {selected: true, color: '#43B7E8', textColor: '#ffffff'};
                }
                value[date.dateString] = {selected: true, endingDay: true, color: '#43B7E8', textColor: '#ffffff'};
                console.log("value", value);
                this.setState({endDate: date.dateString, markDate: value});
            }
        } else {
            var value = {};
            value[date.dateString] = {
                disabled: true,
                startingDay: true,
                color: '#43B7E8',
                endingDay: true,
                textColor: '#ffffff',
            };
            this.setState({startDate: date.dateString, endDate: '', markDate: value});

        }
    }

    onSearch() {
        this.props.sendDateTime({
            startDate: this.state.startDate ? moment(this.state.startDate).format('YYYY-MM-DD 00:00:00') : moment().format('YYYY-MM-DD 00:00:00'),
            endDate: this.state.endDate ? moment(this.state.endDate).format('YYYY-MM-DD 00:00:00') : moment(this.state.startDate).format('YYYY-MM-DD 00:00:00'),
        });
        Actions.pop();
        // Actions.pop({refresh:{refreshData: new Date().toLocaleString()}})
        // Actions.pop({refresh: {startDate: this.state.startDate, endDate: this.state.endDate}});
        // Actions.pop({startDate:this.state.startDate, endDate: this.state.endDate})
    }

    showDatePickerFrom = () => {
        this.setState({isDatePickerVisibleFrom: true})
    }

    hideDatePickerFrom = () => {
        this.setState({isDatePickerVisibleFrom: false})
    }

    showDatePickerTo = () => {
        this.setState({isDatePickerVisibleTo: true})
    }

    hideDatePickerTo = () => {
        this.setState({isDatePickerVisibleTo: false})
    }

    handleConfirmFrom = (date) => {
        let date_start = moment(date).format("YYYY-MM-DD");
        console.log(date_start)
        var value = {};
        value[date_start] = {
            disabled: true,
            startingDay: true,
            color: '#43B7E8',
            endingDay: true,
            textColor: '#ffffff',
        };
        this.setState({startDate: date_start, endDate: date_start, markDate: value});
        this.hideDatePickerFrom();
    };

    handleConfirmTo = (date) => {
        var date1 = moment(this.state.startDate, 'YYYY-MM-DD');
        var date2 = moment(date, 'YYYY-MM-DD');
        var value = {};
        value[this.state.startDate] = {
            disabled: true,
            startingDay: true,
            color: '#43B7E8',
            textColor: '#ffffff',
        };
        var diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        var currentDate = moment(this.state.startDate, 'YYYY-MM-DD');

        for (var i = 1; i < diffDays; i++) {
            currentDate = moment(currentDate).add(1, 'days');
            var srt_currentDate = moment(currentDate).format('YYYY-MM-DD');
            value[srt_currentDate] = {selected: true, color: '#43B7E8', textColor: '#ffffff'};
        }
        value[moment(date).format('YYYY-MM-DD')] = {selected: true, endingDay: true, color: '#43B7E8', textColor: '#ffffff'};
        console.log("value", value, moment(date).format('YYYY-MM-DD'));
        this.setState({endDate: moment(date).format('YYYY-MM-DD'), markDate: value});
        this.hideDatePickerTo();
    };

    render() {
        var month_th = ['ม.ค.', 'ก.พ.', 'มี.ค', 'เม.ย.', 'พ.ค', 'มิ.ย.', 'ก.ค', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        var month_en = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

        var date_start = '';
        var day_start = '';
        var month_start = '';
        var monthtxt_start = '';
        var year_start = '';
        var yeartxt_start = '';

        var date_end = '';
        var day_end = '';
        var month_end = '';
        var monthtxt_end = '';
        var year_end = '';
        var yeartxt_end = '';
        if (this.state.startDate != '') {
            date_start = moment(this.state.startDate, 'YYYY-MM-DD HH:mm:ss');
            day_start = date_start.date();
            month_start = date_start.month();
            monthtxt_start = I18n.locale === 'th' ? month_th[month_start] : month_en[month_start];
            year_start = date_start.year();
            yeartxt_start = I18n.locale === 'th' ? parseInt(year_start) + 543 : parseInt(year_start);

        }

        if (this.state.endDate != '') {
            date_end = moment(this.state.endDate, 'YYYY-MM-DD HH:mm:ss');
            day_end = date_end.date();
            month_end = date_end.month();
            monthtxt_end = I18n.locale === 'th' ? month_th[month_end] : month_en[month_end];
            year_end = date_end.year();
            yeartxt_end = I18n.locale === 'th' ? parseInt(year_end) + 543 : parseInt(year_end);

        }
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9', '#DFE9F5']} style={{flex: 1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} historyMoreDT={true}/>
                    <ScrollView>
                        <View style={styles.historyMDT_container}>
                            <Calendar
                                markedDates={this.state.markDate}
                                markingType={'period'}
                                style={{
                                    width: Dim.width / 100 * 80,
                                    borderRadius: 10,
                                    paddingVertical: 3,
                                }}
                                theme={{
                                    textDayFontSize: 10,
                                    textMonthFontSize: 12,
                                    textDayHeaderFontSize: 10,
                                }}
                                onDayPress={(day) => this.onSelectDate(day)}
                            />
                            <View style={[styles.history_MoreDTdropdown, {marginTop: 10}]}>
                                <TouchableOpacity
                                    style={{backgroundColor: '#ffffff', padding: 5, flexDirection: 'row', justifyContent: "center", alignItems: "center"}}
                                    onPress={this.showDatePickerFrom}
                                >
                                    <Text style={[styles.startto_search, {flex: 1}]}>Start</Text>
                                    <Text
                                        style={[styles.date_search, {flex: 1.5}]}>{`${day_start} ${monthtxt_start} ${yeartxt_start}`}</Text>
                                    <Icon name={'md-arrow-dropdown'} style={{color: '#333333'}}/>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={{backgroundColor: '#ffffff', padding: 5, flexDirection: 'row', marginTop: 3, justifyContent: "center", alignItems: "center"}}
                                    onPress={this.showDatePickerTo}
                                >
                                    <Text style={[styles.startto_search, {flex: 1}]}>To</Text>
                                    <Text
                                        style={[styles.date_search, {flex: 1.5}]}>{`${day_end} ${monthtxt_end} ${yeartxt_end}`}</Text>
                                    <Icon name={'md-arrow-dropdown'} style={{color: '#333333'}}/>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={styles.history_calendarbtn} onPress={() => {
                                this.onSearch();
                            }}>
                                <Text style={styles.history_calendarbtnText}>CONFIRM</Text>
                            </TouchableOpacity>
                        </View>
                        <DateTimePickerModal
                            isVisible={this.state.isDatePickerVisibleFrom}
                            mode="date"
                            value={new Date()}
                            onConfirm={this.handleConfirmFrom}
                            onCancel={this.hideDatePickerFrom}
                        />
                        <DateTimePickerModal
                            isVisible={this.state.isDatePickerVisibleTo}
                            mode="date"
                            value={new Date()}
                            onConfirm={this.handleConfirmTo}
                            onCancel={this.hideDatePickerTo}
                        />
                    </ScrollView>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        );
    }
}

const mapStateToProps = state => {
    return {
        checkIphone: state.check.checkIphone,
    };
};
export default connect(mapStateToProps, actions)(HistoryMoredataScreen);

