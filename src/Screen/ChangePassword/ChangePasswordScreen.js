import React, { Component } from "react"
import { Text, View , TouchableOpacity ,ScrollView,Dimensions} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

const Dim = Dimensions.get("window")
EyeSize = Dim.width/100*7
var EyeIcon = "eye"


class ChangePasswordScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
            oldpwd:''
        }
        // I18n.locale = 'th'
        // Actions.ChangePasswordCheckScreen()
    }
    componentDidMount() {
        console.log(I18n.locale);
    }

    onSubmit() {
        var data = {
            oldpwd : this.state.oldpwd,
        }
        console.log(data)
        this.props.Change_Password(data)
        Actions.ChangePasswordCheckScreen({oldpwdData: this.state.oldpwd});
    }
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} change={true} />
                    <View style={styles.change_password_container}>
                        <ScrollView style={{flex:0.9}}>
                            <View style={styles.ChangePassword_Box1}>
                                <Text style={styles.ChangePassword_Text}>{I18n.t('change_password.ChangePassword_Text')}</Text>
                                <Form style={{alignItems:'center',justifyContent:'center'}}>
                                <View style={{width:'100%'}}>
                                    <Item rounded style={styles.Password_InputBox}>
                                        <Input style={styles.ChangePassword_PlaceHolderText} value={this.state.oldpwd} onChangeText={(text) => {this.setState({oldpwd:text})}} placeholderTextColor='#C4C4C6' placeholder={I18n.t('change_password.existing_password')} />
                                    </Item>
                                </View>
                                    {
                                        this.state.oldpwd
                                        ?   <TouchableOpacity style={styles.history_calendarbtn} onPress={()=> this.onSubmit()} >
                                                <Text style={styles.history_calendarbtnText}>{I18n.t('change_password.next')}</Text>
                                            </TouchableOpacity>
                                        :   <View style={[styles.history_calendarbtn, {backgroundColor: "#C8CBCD"}]} >
                                                <Text style={styles.history_calendarbtnText}>{I18n.t('change_password.next')}</Text>
                                            </View>
                                    }
                                </Form>
                            </View>
                        </ScrollView>
                    <UnderlineComponent />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}

const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,

    }
  }
export default connect(mapStateToProps,actions)(ChangePasswordScreen)
