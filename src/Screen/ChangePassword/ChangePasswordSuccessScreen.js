import React, { Component } from "react"
import { Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

class ChangePasswordSuccessScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,
        }
        // I18n.locale = 'th'
    }
    onProfile(){
        console.warn('onProfile',this.props.userinfo.is_merchant_hq)
        if(this.props.userinfo.is_merchant_hq == true){
            Actions.Manager_profile()
        }else{
            Actions.Staff_profile()
        }

    }
    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9}}>
                            <View style={{flex:1,backgroundColor:'white', borderRadius: 20,alignItems:'center',justifyContent:'center' }}>
                            <Text style={styles.PaymentS_TextpaymentSuccess}>{I18n.t('change_password_success.title_1')} </Text>
                            <Image  source={require('../../../assets/images/paymentSuccess.png')} style={styles.PaymentS_image} />
                            <Text style={styles.reservationSS_TextPaymentID}>{I18n.t('change_password_success.detail_1')}</Text>
                            <Text style={styles.reservationSS_TextPaymentID}>{I18n.t('change_password_success.detail_2')}</Text>
                                <View style={styles.PaymemtS_button} >
                                    <TouchableOpacity style={styles.enterBarcode_nextSub} onPress={()=> this.onProfile()}>
                                        <Text style={styles.history_calendarbtnText}>{I18n.t('change_password_success.done')}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}


const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        userinfo:state.user.userinfo
    }
  }
export default connect(mapStateToProps,actions)(ChangePasswordSuccessScreen)
