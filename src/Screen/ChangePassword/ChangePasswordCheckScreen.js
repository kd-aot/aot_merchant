import React, { Component } from "react"
import { Alert, Text, View, StyleSheet , TextInput  , TouchableOpacity ,Image,Dimensions} from 'react-native';
import { Container, Content, Body , Header ,Right,Button,Item,Input,Form} from 'native-base'
import { Actions } from "react-native-router-flux"
import { connect } from "react-redux"
import Icon from 'react-native-vector-icons/dist/Octicons';
import I18n from '../../../assets/languages/i18n'
import * as actions from "../../Actions"
import styles from '../../Style/style'
import UnderlineComponent from '../../Component/UnderlineComponent'
import HeaderComponent from '../../Component/HeaderComponent'
import LinearGradient from 'react-native-linear-gradient';
import FooterComponent from '../../Component/FooterComponent';

class ChangePasswordCheckScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            show1:true,
            show2:true,
            EyeIcon1:"eye-closed",
            EyeIcon2:"eye-closed",

            Oldpassword:"",
            Newpassword:"",
            NewpasswordCheck:"",
            CheakPassword:""
        }
        // I18n.locale = 'th'
    }
    showPassword1(){
        this.setState({show1:false})
        this.setState({EyeIcon1:"eye"})
        if(this.state.show1==false){
            this.setState({show1:true});
            this.setState({EyeIcon1:"eye-closed"})
        };
    }
    showPassword2(){
        this.setState({show2:false})
        this.setState({EyeIcon2:"eye"})
        if(this.state.show2==false){
            this.setState({show2:true});
            this.setState({EyeIcon2:"eye-closed"})
        };
    }
    checkInput(){
        if( this.state.Newpassword === this.state.NewpasswordCheck ){
            this.setState({ CheakPassword:""})
            if( this.state.Newpassword.length === 0 ){
                this.setState({ CheakPassword:"Error."})
            }else{
                console.warn(this.state.Newpassword)
                this.onSubmit()
            }
        }else{
            this.setState({ CheakPassword:"Passwords do not match." })
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (props.oldpwd) {
          return {
            Oldpassword: props.oldpwd
          };
        }
        return null;
      }

    onSubmit() {
        const { Newpassword, NewpasswordCheck } = this.state;
        if (Newpassword !== NewpasswordCheck) {
            Alert.alert(
                "Your password does not matched",
                "",
                [
                    { text: "OK"}
                ],
                { cancelable: false }
            );
        } else {
            if (Newpassword.length < 8) {
                Alert.alert(
                    "Password must contain at least 8 characters and numbers",
                    "",
                    [
                        { text: "OK" }
                    ],
                    { cancelable: false }
                );
            } else {
                if(/\d+/g.test(Newpassword)) {
                    var data = {
                        //     // oldpwd : this.state.Oldpassword.oldpwd,
                            oldpwd : this.props.oldpwdData,
                            newpwd : this.state.NewpasswordCheck
                    };
                    this.props.Change_Password2(data)
                } else {
                    Alert.alert(
                        "Password must contain at least 8 characters and numbers",
                        "",
                        [
                            { text: "OK" }
                        ],
                        { cancelable: false }
                    );
                }
            }
        }
       //
    //    console.log(" check "+ data )
    //    console.log( data )
    //    this.props.Change_Password2(data)
}

    // Actions.ChangePasswordSuccessScreen()

    render() {
        return (
            <Container>
                <LinearGradient colors={['#F7F9FD', '#ECF1F9','#DFE9F5']} style={{flex:1}}>
                    <HeaderComponent home={true} leftbtn={true} title={true} change={true} />
                    <View style={styles.home_container}>
                        <View style={{flex:0.9}}>
                            <View style={{flex:0.6,alignItems:'center',justifyContent:'center'}}>
                                <Form style={{alignItems:'center',justifyContent:'space-between'}}>
                                <View style={{width:'100%',alignItems:'center',justifyContent:'center'}}>
                                    <Item rounded style={this.state.CheakPassword === "" ? styles.Text_Input : styles.Text_InputError }>
                                        <Input style={styles.ChangePassword_PlaceHolderText} placeholderTextColor='#C4C4C6' placeholder='New password' value={this.state.Newpassword} onChangeText={(text) => {this.setState({Newpassword:text})}} secureTextEntry={this.state.show1} />
                                        <TouchableOpacity onPress={()=>{this.showPassword1()}} style={{flex:0.15}}>
                                            <Icon name={this.state.EyeIcon1} size={EyeSize} />
                                        </TouchableOpacity>
                                    </Item>
                                    <Item rounded style={this.state.CheakPassword === "" ? styles.Text_Input : styles.Text_InputError }>
                                        <Input style={styles.ChangePassword_PlaceHolderText} placeholderTextColor='#C4C4C6' placeholder='Confirmation password' onChangeText={(text) => {this.setState({NewpasswordCheck:text})}}  secureTextEntry={this.state.show2} />
                                        <TouchableOpacity onPress={()=>{this.showPassword2()}} style={{flex:0.15}}>
                                            <Icon name={this.state.EyeIcon2} size={EyeSize} />
                                        </TouchableOpacity>
                                    </Item>
                                    <View style={styles.ChangePassword_AlertTextContainer}>
                                        <Text style={styles.ChangePassword_AlertText}>{this.state.CheakPassword}</Text>
                                    </View>
                                </View>
                                    {
                                        this.state.Newpassword && this.state.NewpasswordCheck
                                        ?   <TouchableOpacity style={styles.history_calendarbtn} onPress={()=>{this.onSubmit()}} >
                                                <Text style={styles.history_calendarbtnText}>DONE</Text>
                                            </TouchableOpacity>
                                        :   <View style={[styles.history_calendarbtn, {backgroundColor: "#C8CBCD"}]} >
                                                <Text style={styles.history_calendarbtnText}>DONE</Text>
                                            </View>
                                    }
                                </Form>
                            </View>
                        </View>
                        <UnderlineComponent leftbtn={false} />
                    </View>
                    <FooterComponent/>
                </LinearGradient>
            </Container>
        )
    }
}
const mapStateToProps = state => {
    return {
        checkIphone:state.check.checkIphone,
        oldpwd:state.oldpwd.oldpwd
    }
  }
export default connect(mapStateToProps,actions)(ChangePasswordCheckScreen)
