import axios from "axios"
import { Alert } from 'react-native'
import { API_IP, CONFIG } from '../Constant/constant'
import {
    HISTORY_DATE_TIME_SETTING,
    HISTORY_PAYMENT,
    HISTORY_PAYMENT_ERROR,
    HISTORY_VOUCHER,
    PAYMENT_DETAIL,
} from './type';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from "react-native-router-flux"

export const getPaymentHistory = (branch_id) => {
    return async dispatch => {
        console.log('getPaymentHistory',branch_id)
        var token = await AsyncStorage.getItem('usertoken')
        console.log(token)
    await fetch('http://ec2-18-141-41-228.ap-southeast-1.compute.amazonaws.com:50000/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify({query:`query { 
            getPaymentHistoryList(
                branch_id: ${branch_id}, ){
                    count payload {
                    order_number type
                    price discount_price vat
                    total amount point_received point_used status created_at user {
                    user_firstname user_lastname user_title user_title_name
                    } payment {
                    transaction_id amount invoice_number transaction_reference masked_pan
                    } point_transaction {
                    transaction_id prefix
                    amount provider_from {
                    value }
                    provider_to { value
            } }
            } }
            }`
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log('getPaymentHistory',data.data.getPaymentHistoryList.payload);
            return dispatch({ type: HISTORY_PAYMENT, payload: data.data.getPaymentHistoryList.payload})

        })
        .catch(function(error) {
            console.log(error)
            console.log('There has been a problem with your fetch operation: ' + error.message);
            // Alert.alert("ไม่พบข้อมู");
            });
    }
}

export const getHistoryPaymentList = (branch_id,order_number) => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
    await fetch('https://aot-service.staging.kdlab.dev/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify({query:`query { getPaymentHistory(
            branch_id: ${branch_id},
            order_number: ${order_number} )
            {
            order_number type
            price discount_price vat
            total point_received point_used status created_at user {
            user_firstname user_lastname user_title user_title_name
            } point_transaction {
            transaction_id prefix
            amount provider_from {
            value }
            provider_to { value
            } }
            } }
            `
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log("getHistoryPaymentList : ",data);
            if(data.getPaymentHistory != undefined){
                return dispatch({ type: PAYMENT_DETAIL, payload: data.getPaymentHistory})
            }else{
                // Alert.alert('getHistoryPaymentList Fail')
            }
        })
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            Alert.alert(error.message);
            });
    }
}

export const getVoucherHistory = (branch_id, start_date, end_date) => {
    return async dispatch => {
        console.warn(branch_id, start_date, end_date)
        var token = await AsyncStorage.getItem('usertoken')
    await fetch('https://aot-service.staging.kdlab.dev/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify({query:`query {
            fetchMerchantAppHistoryVoucher(branch_id:${branch_id} , start_date: "${start_date}", end_date: "${end_date}"){
            success message count payload {
            voucher_name transaction_id used_at date_used_at time_used_at customer_first_name customer_last_name status
            type data {
            ...on MerchantAppPrivillegeHistoryDetailResponse { transaction_id
            used_at
            status
            first_name last_name voucher_name voucher_code point_used price_used to_branch_name type
            }
            ...on MerchantAppReservationHistoryDetailResponse {
            transaction_id used_at
            status first_name last_name voucher_name voucher_code credit_card to_branch_name price
            amount
            vat discount_price total point_used type
            } }
            } }
            }`
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log('getVoucherHistory',data.data.fetchMerchantAppHistoryVoucher.payload);
            if(data.data != undefined){
                return dispatch({ type: HISTORY_VOUCHER, payload: data.data.fetchMerchantAppHistoryVoucher.payload})
            }
        })
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            Alert.alert(error.message);
            });
    };
};

export const sendDateTime = (data) => {
    return async dispatch => {
        console.log(data);
        dispatch({type: HISTORY_DATE_TIME_SETTING, payload: {data: data, time: new Date().toLocaleString()}})
    }
};

