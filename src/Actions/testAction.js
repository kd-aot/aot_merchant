import axios from "axios"
import { API_IP, CONFIG } from '../Constant/constant'
import { TEST, TEST_ERROR } from './type'

export const testfunc = option => {
    return async dispatch => {
      var data = new FormData()
      axios
        .post(API_IP + "PATH API", data, CONFIG)
        .then(function (response) {
          if(response.data.code == 200){
              var result = 'ข้อมูลที่จะส่งไป'
            return dispatch({ type: TEST, payload: result})
          }else{
            return dispatch({ type: TEST_ERROR, payload: 'error text'})
          }
        })
        .catch(function (error) {
            return dispatch({ type: TEST_ERROR, payload: 'internet error'})
        })
    }
}


