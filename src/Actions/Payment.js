import {Alert} from 'react-native';
import {API_IP, CONFIG} from '../Constant/constant';
import {PAYMENT, ORDER_NUMBER, CHECK_OPEN_CAMERA} from './type';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';

export const scanPayment = (qrcode) => {
    return async dispatch => {
        return dispatch({type: PAYMENT, payload: qrcode})
            .then(Actions.payment({qrCode: qrcode}));
    };
};

export const sendPayment = (price, code, branch_id) => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken');
        var myHeaders = new Headers();
        myHeaders.append('Authorization', token);
        myHeaders.append('Content-Type', 'application/json');

        var graphql = JSON.stringify({
            query: 'mutation($price: Float $code: String $merchant_id: Int $branch_id: Int){ paymentRequestAotPass(price: $price, code: $code merchant_id: $merchant_id branch_id:$branch_id) {success payload {order_id order_number type point_received point_used price discount_price total status user_id fullname } message }}',
            variables: {'price': parseFloat(price), 'code': code, 'branch_id': parseInt(branch_id)},
        });
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: graphql,
            redirect: 'follow',
        };

        console.log(requestOptions);

        fetch('http://ec2-18-141-41-228.ap-southeast-1.compute.amazonaws.com:50000/', requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.data.paymentRequestAotPass.success === true) {
                    // Alert.alert(result.data.paymentRequestAotPass.payload.order_number);
                    console.log(result.data.paymentRequestAotPass.payload);
                    return dispatch({type: ORDER_NUMBER, payload: result.data.paymentRequestAotPass})
                        // .then(Actions.payment_success({dataPayment: result.data.paymentRequestAotPass.payload}));
                } else {
                    Actions.payment_fail();
                    // Actions.payment_success({type: 'replace'});
                }
            })
            .catch(error => console.log('error', error));
    };
};

export const checkOpenScanQrCode = (data) => {
    return async dispatch => {
        dispatch({type: CHECK_OPEN_CAMERA, payload: {data: data, time: new Date().toLocaleString()}})
    }
};
