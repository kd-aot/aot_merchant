import axios from 'axios';
import {Alert} from 'react-native';
import {API_IP, CONFIG} from '../Constant/constant';
import {PRIVILEGE, RESERVATION, VOUCHER_ERROR, USEVOUCHER} from './type';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';

export const useMerchantVoucher = (voucher_id, branch_id) => {
    console.log(voucher_id, branch_id);
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken');
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({
                query: `mutation 
        usemerchantvoucher { 
            useMerchantAppVoucher (
                voucher_id: "${voucher_id}"
                branch_id:  ${branch_id}
                ){
                success
                message 
                }
            }`,
            }),
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                console.log(data.data.useMerchantAppVoucher);
                if (data.data.useMerchantAppVoucher.success === true) {
                    console.log(data.data.useMerchantAppVoucher);
                    return dispatch({type: USEVOUCHER, payload: data.data.useMerchantAppVoucher.payload})
                        .then(Actions.privilege_success_status({code: voucher_id, typePtg: null}));
                } else {
                    Actions.privilege_fail();
                    // return dispatch({type: USEVOUCHER, payload: data.data.useMerchantAppVoucher.payload})
                    //     .then(Actions.privilege_success_status({code: voucher_id}));
                }
            })
            .catch(function (error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
                // Alert.alert(error.message);
            });
    };
};

export const useMerchantVoucherReservation = (voucher_id, branch_id) => {
    console.log(voucher_id, branch_id);
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken');
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({
                query: `mutation 
        usemerchantvoucher { 
            useMerchantAppVoucher (
                voucher_id: "${voucher_id}"
                branch_id:  ${branch_id}
                ){
                success
                message 
                }
            }`,
            }),
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                console.log(data.data.useMerchantAppVoucher);
                if (data.data.useMerchantAppVoucher.success == true) {
                    console.log(data.data.useMerchantAppVoucher);
                    return dispatch({type: USEVOUCHER, payload: data.data.useMerchantAppVoucher.payload})
                        .then(Actions.reservation_success({code: voucher_id}));
                } else {
                    Actions.privilege_fail();
                    // return dispatch({type: USEVOUCHER, payload: data.data.useMerchantAppVoucher.payload})
                    //     .then(Actions.reservation_success({code: voucher_id}))
                    // return dispatch({type: USEVOUCHER, payload: data.data.useMerchantAppVoucher.payload})
                    //     .then(Actions.privilege_success_status({code: voucher_id}));
                }
            })
            .catch(function (error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
                // Alert.alert(error.message);
            });
    };
};

export const getVoucherDetail = (code, branch_id) => {
    console.log('getVoucherDetail');
    console.log(code, ' ', branch_id);
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken');
        var url = 'https://aot-service.staging.kdlab.dev/';
        await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({
                query: `query { 
            fetchMerchantAppVoucherDetailByCode (
                code:  "${code}",
                branch_id: ${branch_id} 
                ){
                success 
                message 
                payload {
                    ...on MerchantAppPrivillegeDetailResponse{ 
                        type
                        _id
                        title 
                        short_description 
                        description 
                        condition_description 
                        redeem_start 
                        redeem_end 
                        use_start
                        use_end
                        point
                        price
                        condition {
                            limit
                            limit_per_member 
                        }
                        image_cover image_thumbnail    
                        category { 
                            _id
                            name 
                            position 
                            is_active 
                            icon
                            }
                        is_active 
                        is_highlight 
                        is_pin
                        sites {
                            _id
                            iata_code }
                        ecom_id }
                    ...on MerchantAppReservationDetailResponse{ 
                        type
                        name
                        model 
                        first_name 
                        last_name 
                        payment_id 
                        created_at phone country passport flight_number credit_card total
                    amount vat point_used from
                    to
                    booking_date }
                    } }
                    }`,
            }),
        })
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                console.log(data.data.fetchMerchantAppVoucherDetailByCode);
                if (data.data.fetchMerchantAppVoucherDetailByCode.payload == undefined) {
                    Actions.privilege_fail({type: "replace"});
                }
                if (data.data.fetchMerchantAppVoucherDetailByCode.payload.type == 'PRIVILLAGE') {
                    return dispatch({type: PRIVILEGE, payload: data.data.fetchMerchantAppVoucherDetailByCode.payload})
                        .then(Actions.privilege_success({code: code}));
                } else if (data.data.fetchMerchantAppVoucherDetailByCode.payload.type == 'RESERVATION') {
                    return dispatch({type: RESERVATION, payload: data.data.fetchMerchantAppVoucherDetailByCode.payload})
                        .then(Actions.reservation({code: code}));
                }
            });
        // .catch(function(error) {
        //     console.log('There has been a problem with your fetch operation: ' + error.message);
        //     });
    };
};

export const useMerchantVoucherPTG = (code, branch_id) => {
    console.log(code, branch_id);
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken');
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({
                query: `mutation { 
                            useMerchantVoucherCoupon (
                                voucher_code: "${code}"
                                branch_id: ${branch_id} )
                            {
                                success
                                message 
                            }
                        }`,
            }),
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.data.useMerchantVoucherCoupon.success) {
                    Actions.privilege_success_status({typePtg: "ptg", code: code})
                } else {
                    Actions.privilege_fail({type: "replace"});
                    // Actions.privilege_success_status({typePtg: "ptg", code: code})
                }
            })
            .catch(function (error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
            });
    };
};

