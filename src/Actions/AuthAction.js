import axios from "axios"
import { Alert } from 'react-native'
import { API_IP, CONFIG } from '../Constant/constant'
import {USER_ERROR, USER, OLDPWD, LOGIN_STATUS_FAIL} from './type';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from "react-native-router-flux"

export const Login2 = option => {
    return async dispatch => {
        await fetch(API_IP, {
            method: 'POST',
            headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                        },
            body: JSON.stringify({query:`mutation {  
                userMerchantAppLogin(
                    user_email:"${option.user_email}", 
                    user_password:"${option.user_password}"){    
                        user_id    
                        user_firstname    
                        user_lastname    
                        refresh_token    
                        user_emails { email_address }  
                    } 
            }`})
            })
            .then(response =>
                response.json()
            )
            .then(async data => {
                if(data.data.error == undefined){
                    // if(data.data.userMerchantAppLogin == undefined){
                        var token = data.data.userMerchantAppLogin.refresh_token
                        // console.log("uqix", token)
                        await AsyncStorage.setItem("usertoken",token)
                        await fetch('https://aot-service.staging.kdlab.dev/', {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'Authorization': token,
                            },
                            body: JSON.stringify({query:`query {  
                                userMerchantProfile {    
                                    user_id
                                    user_title
                                    user_title_name
                                    user_username
                                    user_firstname
                                    user_middlename    
                                    user_lastname    
                                    user_avatar    
                                    user_emails {      
                                        email_address    
                                    }    
                                    user_phones {      
                                        phone_number    
                                    }
                                    merchant_name    
                                    merchant_id    
                                    is_merchant_hq    
                                    branch_id    
                                    branch_name  
                                }}`})
                            })
                            .then(r => r.json())
                            .then(data => {
                                console.log(data)
                                var t = data.data.userMerchantProfile
                                if(t.is_merchant_hq == true){
                                    Actions.branch_select()
                                }else{
                                    Actions.home({type: "reset"})
                                }
                            })
                    }
                }
            ).catch(function(error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
                dispatch({type: LOGIN_STATUS_FAIL, payload: {time: new Date().toLocaleString()}})
                if(error == `TypeError: null is not an object (evaluating 'data.data.userMerchantAppLogin.refresh_token')`){
                    Alert.alert('wrong username/password');
                }else{
                    Alert.alert('wrong username/password');
                }
            });
    }
}


export const getuserinfo = option => {
    console.log("getinfo",option)
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
        console.log('a' ,token)
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({query:`query {  
                userMerchantProfile {    
                    user_id
                    user_title
                    user_title_name
                    user_username
                    user_firstname
                    user_middlename    
                    user_lastname    
                    user_avatar    
                    user_emails {      
                        email_address    
                    }    
                    user_phones {      
                        phone_number    
                    }
                    merchant_name    
                    merchant_id    
                    is_merchant_hq    
                    branch_id    
                    branch_name  
                }}`})
            })
            .then(r => r.json())
            .then(async data => {
                console.log(data)
                var branch_id_async = await AsyncStorage.getItem('branch_id');
                console.log("branch_id_async", branch_id_async);
                var branch_id = data.data.userMerchantProfile.branch_id
                if(!branch_id_async) await AsyncStorage.setItem('branch_id', branch_id.toString())
                return dispatch({ type: USER, payload: data.data.userMerchantProfile})
            })
        }
}




export const Log_UserOut = () => {
    return async dispatch => {
        // AsyncStorage.setItem("usertoken","")
        AsyncStorage.getAllKeys()
            .then(keys => {
                console.log(keys)
                AsyncStorage.multiRemove(keys)
            }).then(() => alert('success'));
        Actions.login()
    }
}

export const Forgot_Password = option => {
    console.warn(option)
    return async dispatch => {
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({query:`mutation { 
                userCMSForgotPassword(
                    email: "${option.user_name}") {
                success }
                }`})
            })
            .then(r => r.json())
            // .then(data => {
            //     console.log('forgot respones : ' , data)
            // })
            .then(async data => {
                console.log('forgot respones : ' , data)
                if(data.data.userCMSForgotPassword.success == true){
                        console.log("Forgot success", data)
                        Actions.forgot_password_success()
                    }
                })
            .catch(function(error) {
                console.log('error  : ' + error);
                if(error.message == `TypeError: null is not an object (evaluating 'data.data.userCMSForgotPassword.success`){
                    Alert.alert('No user found');
                }else{
                    Alert.alert('No user found')
                }
            });
        }
}


export const Change_Password = (data) => {
    console.log("This OlDpwd2" , data)
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
    await fetch('https://aot-service.staging.kdlab.dev/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({query:`mutation {  
            updateUserPassword(
                old_password: "${option.oldpwd}", 
                new_password: "${option.newpwd}"
                ) {    
                    error_message    
                    is_success  
                }}`
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if (data.data.updateUserPassword.error_message == "Incorrect") {
                Alert.alert("Change password fail")
            }else{
                Actions.ChangePasswordSuccessScreen()
            }
        })
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            Alert.alert(error.message);
            });
    }
}

export const Change_Password2 = (option) => {
    console.log("Change_Password2")
    console.log(option)
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
    await fetch('https://aot-service.staging.kdlab.dev/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': token
        },
        body: JSON.stringify({query:`mutation {  
            updateUserPassword(
                old_password: "${option.oldpwd}", 
                new_password: "${option.newpwd}"
                ) {    
                    error_message    
                    is_success  
                }}`
            })
        })
        .then(response => response.json())
        .then(data => {console.log(data);
            if (data.data.updateUserPassword.error_message == "Incorrect") {
                Alert.alert("Change password fail")
            }else{
                Actions.ChangePasswordSuccessScreen({type: 'replace'})
            }
        })
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
            Alert.alert(error.message);
            });
    }
}

export const change_imgProfile = (source) => {
    console.log(source)
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
        console.log('a' ,token)
        await fetch('https://aot-service.staging.kdlab.dev/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({query:`
                mutation { updateUserCMSProfileImage(
                    user_avatar: "${source}") { status
                    message
                    } }
            `})
            })
            .then(r => r.json())
            .then(async data => {
                console.log("upload status :",data)
                console.log("upload status :",data.data.updateUserCMSProfileImage)
                if(data.data.updateUserCMSProfileImage == null){
                    Alert.alert("Uploaded image has valid size")
                }else{
                    Alert.alert("Uploaded")
                }

                // Alert.alert(data.data.updateUserCMSProfileImage.status)
            })
        }
}
