import { Alert } from 'react-native'
import { API_IP, CONFIG } from '../Constant/constant'
import { 
    BRANCH_LIST, 
    BRANCH_LIST_ERROR,
    BRANCH_DETAIL,
    BRANCH_DETAIL_ERROR
} from './type'
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from "react-native-router-flux"

export const getBranchAll = () => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
        await fetch(API_IP, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token,
            },
            body: JSON.stringify({query:`query { 
                fetchMerchantAppProfileBranchList(limit:10,offset:1) {
                    count payload {
                    id name status
                    } }
                }`})
            })
            .then(r => r.json())
            .then(data => {
                var temp = data.data
                var data_list = temp.fetchMerchantAppProfileBranchList.payload
                return dispatch({ type: BRANCH_LIST, payload: data_list})
            })
        }
    }

export const getBranchDetail = branch_id => {
    return async dispatch => {
        var token = await AsyncStorage.getItem('usertoken')
        await fetch(API_IP, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': token
            },
            body: JSON.stringify({query:`query 
            listUser { fetchUserByBranchId(branch_id:${branch_id}) {
                count payload {
                    user_id 
                    user_firstname 
                    user_middlename 
                    user_lastname 
                    user_avatar 
                    user_email 
                    user_phone
                } }
                }`})
            })
            .then(r => r.json())
            .then(data => {
                console.log('BRANCH_DETAIL',data.data.fetchUserByBranchId.payload)
                return dispatch({ type: BRANCH_DETAIL, payload: data.data.fetchUserByBranchId.payload})
            })
        }
    }