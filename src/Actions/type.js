export const CHECKTYPEIPHONE = 'check_type_iphone'
export const CHECKTYPEIPHONE_ERROR = 'check_type_iphone_error'

export const TEST = 'test'
export const TEST_ERROR = 'test_error'

export const USER = 'user'
export const USER_ERROR = 'user_error'

export const OLDPWD = 'oldpwd'
export const OLDPWD_ERROR = 'oldpwd_error'

export const BRANCH_LIST = 'branch_list'
export const BRANCH_LIST_ERROR = 'branch_list_error'
export const BRANCH_DETAIL = 'branch_detail'
export const BRANCH_DETAIL_ERROR = 'branch_detail_error'

export const PAYMENT = "payment"
export const PAYMENT_ERROR = "payment_error"

export const PAYMENT_DETAIL = "payment_detail"
export const PAYMENT_DETAIL_ERROR = "payment_detail_error"


export const ORDER_NUMBER = "order_number_action"
export const ORDER_ERROR = "order_error"

export const PRIVILEGE = "privilege"
export const RESERVATION = "reservation"
export const VOUCHER_ERROR = "voucher_error"

export const HISTORY_PAYMENT = 'history_payment'
export const HISTORY_PAYMENT_ERROR = 'history_payment_error'
export const HISTORY_VOUCHER = 'history_voucher'
export const HISTORY_VOUCHER_ERROR = 'history_voucher_error'
export const HISTORY_DATE_TIME_SETTING = "historyDateTimeSetting";

export const USEVOUCHER = "user_voucher";

export const CHECK_OPEN_CAMERA = "check_open_camera";

export const LOGIN_STATUS_FAIL = "login_status_fail";

