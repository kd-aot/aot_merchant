import {Dimensions, Platform, StyleSheet} from 'react-native'
import {
  MAIN_COLOR,
  SUB_COLOR
} from "./color"

import { MAIN_FONT,SUB_FONT } from './font'

import DeviceInfo from 'react-native-device-info'
import { color } from 'react-native-reanimated'

const Dim = Dimensions.get("window");

export default StyleSheet.create({
    submit_button:{
        width: Dim.width/100*70,
        height: Dim.height/100*5,
        backgroundColor:MAIN_COLOR,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:Dim.width/100*2
    },
    txt_btn:{
        fontSize:Dim.width/380*24,
        fontFamily:MAIN_FONT
    },
    PrivilegeSbox:{
        flex:1,
        backgroundColor:'white',
        borderRadius:Dim.height/100*6,
        alignItems:'center',
        justifyContent:'flex-start',
        // backgroundColor:'red'
    },
    view_center:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    login_ads_container:{
        flex:0.3,
        backgroundColor:'red',
        alignItems:'center',
        justifyContent:'center'
    },
    login_ads_banner:{
        height: Dim.width/100*30,
        width: Dim.width/100*90,
        marginBottom:Dim.height/100*3,
        backgroundColor:'blue'
    },
    login_ads_banner_container:{
        flex:0.195,
        justifyContent:'center',
        paddingTop:Dim.height/100*11,
        zIndex:-1
    },
    login_container:{
        flex:1,
        margin:Dim.width/100*5,
        marginBottom:Dim.height/100*3,
        marginTop:Dim.height/100*4,
    },
    LoginLanguageBox:{
        width:Dim.width>650 ? Dim.width/100*18 : Dim.width/100*23,
        height:Dim.height>800 ? Dim.height/100*3 : Dim.height/100*5,
        marginTop:Dim.height>800 ? 10 : 1,
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-evenly',
        borderRadius:Dim.width ,
        borderWidth:1,
        borderColor:'#43B7E8',

    },
    LoginInput:{
        flex:0.25,
        paddingTop:Dim.height/100*4,
        width:Dim.width/100*80,
        alignSelf:'center',
        // backgroundColor:'red'
    },
    LoginFlag:{
        height:Dim.height/100*3,
        width:Dim.width/100*7
    },
    LoginLangTextEN:{
        fontFamily:SUB_FONT,
        fontSize:Dim.width>650 ? Dim.width/100*2 : Dim.width/100*3,
        color:'#65B4E3'
    },
    LoginLangTextTH:{
        fontFamily:MAIN_FONT,
        fontSize:Dim.width/100*4,
        color:'#65B4E3'
    },
    LoginAOTLogo:{
        height:Dim.width * 0.3,
        width:Dim.width * 0.3,
        resizeMode: "contain",
    },
    login_forgotEN:{
        fontSize:Dim.width/100*4,
        color: '#9C9C9C' ,
        fontFamily:SUB_FONT
    },
    login_forgotTH:{
        fontSize:Dim.width/100*4,
        color: '#9C9C9C' ,
        fontFamily:MAIN_FONT
    },
    login_text_merchant:{
        fontSize:Dim.width/380*28,
        color: '#354A9B' ,
        fontFamily:SUB_FONT,
        fontWeight:'bold'
    },
    loginItemMargin:{
        marginTop:Dim.height/100*2,
        marginBottom:Dim.height/100*2,
        backgroundColor:'white',
        height:Dim.height/14,
        borderRadius:Dim.width,

    },
    login_placeholderText:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        paddingLeft:Dim.width/18
    },
    login_botton:{
        padding: 3,
        height: '65%',
        borderRadius: Dim.height/100*3 ,
        backgroundColor:'#65B4E3',
        alignItems:'center',
        justifyContent:'center',
        marginTop:Dim.height/100*1,
    },
    login_loginBtn:{
        paddingTop:Dim.height/100*3,
        height:Dim.height/100*13,
        alignItems:'center'
    },
    home_container:{
        flex:1,
        margin:Dim.width/100*5,
        marginBottom:Dim.height/100*3,
        marginTop:Dim.height/100*0
    },
    whiteBox:{
        // flex:1,
        backgroundColor:'white',
        borderRadius: 20,
        alignItems:'center',
        justifyContent:'center' ,
        marginTop: Dim.width > 650 ? Dim.height/100*3 : 0
    },
    home_scanEN:{
        fontSize:Dim.width>650 ? Dim.width/380*18 : Dim.width/380*22,
        color:'#354A9B',
        fontWeight:'bold',
        fontFamily:SUB_FONT,
    },
    home_scanTH:{
        fontSize:Dim.width>650 ? Dim.width/380*18 : Dim.width/380*22,
        color:'#354A9B',
        fontWeight:'bold',
        fontFamily:MAIN_FONT,
    },
    home_box:{
        height:'40%',
        margin:5,backgroundColor:'white',
        borderRadius:20,
        flexDirection:'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    home_box_iconview:{
        flex:0.4,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:20
    },
    home_ads_banner:{
        height: Dim.height > 800 ?  Dim.height/100*40 : Dim.width/100*72,
        width: Dim.width > 600 ? Dim.width/100*60 : Dim.width/100*82,
    },
    scan_container:{
        flex:1,
        // marginBottom:Dim.height/100*3,
        marginTop:Dim.height/100*2
    },
    scan_underlineContainer:{
        flex:0.08,
        margin:Dim.width/100*5,
        // marginBottom:Dim.height/100*1,
        marginTop:Dim.height/100*3,
    },
    scan_button:{
        flex:1,
        justifyContent:'flex-end',

    },
    enterBarcode_textInput:{
        fontFamily:SUB_FONT,
        fontSize: 14,
        textAlign:'center'
    },
    enterBarcode_next:{
        padding:5,
        width:'50%',
        marginTop:Dim.height/100*4,
    },
    enterBarcode_nextSub:{
        padding: 3,
        height: Dim.height/100*6 ,
        borderRadius: Dim.width ,
        backgroundColor:'#65B4E3',
        alignItems:'center',
        justifyContent:'center'
    },
    enterBarcode_text:{
        fontWeight:'400',
        // paddingTop:Dim.height/100*3,
        // paddingBottom:Dim.height/100*1.5,
        fontSize:Dim.width/380*20,
        fontFamily:SUB_FONT
    },
    enterBarcode_text2:{
        fontWeight:'400',
        paddingTop:Dim.height/100*1.5,
        paddingBottom:Dim.height/100*1.5,
        fontSize:Dim.width/380*20,
        fontFamily:SUB_FONT
    },
    enterBarcode_item:{
        margin:10,
        width:Dim.width/1.25,
        backgroundColor:'white',
        justifyContent:'flex-end',
        borderRadius:Dim.width,
        // backgroundColor:'red'
    },
    enterBarcode_item2:{
        padding: 3,
        height: Dim.height/100*6,
        width:Dim.width/1.4,
        borderRadius: Dim.width,
        marginTop:Dim.height/100*3,
        backgroundColor:'#DFE9F5',
        alignItems:'center',
        justifyContent:'center'
    },
    enterBarcode_TextInput:{
        textAlign:'right',
        width:'80%',
        marginRight: Dim.width/100*5,
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*28,
        color: "#000000"
    },
    enterBarcode_block:{
        height:Dim.height/20
    },
    enterBarcode_Picker:{
        width: Dim.width>650 ? Dim.width/100*60 : Dim.width/100*66 ,
        borderRadius: Dim.width,
    },
    enterBarcode_Pickertext:{
        fontSize:Dim.width>650 ? Dim.width/380*12 : Dim.width/380*18,
    },
    Underline_textAL:{
        fontSize:Dim.width/100*4,
        fontFamily:SUB_FONT,
        color:'#65B4E3',
        fontWeight:'bold'
    },
    Underline_textAIR:{
        fontSize:Dim.width/380*12,
        fontFamily:SUB_FONT,
        color:'#65B4E3'
    },
    PaymentS_TextpaymentSuccess:{
        fontSize:Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'black',
        fontWeight:'bold',
        marginTop:Dim.height/100*2,
        textAlign:'center'
    },
    Payment_failf1:{
        flex: Dim.height>850 ?  0.8 : 0.9,
        backgroundColor:'white',
        borderRadius: 20,
        alignItems:'center',
        justifyContent:'center'
    },
    PaymentS_TextPaymentID:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        marginBottom:5,
        textAlign:'center'
    },
    PaymentS_TextPrice:{
        fontSize:Dim.width>650 ? Dim.width/380*30 : Dim.width/380*35,
        fontFamily:SUB_FONT,
        color:'black',
        fontWeight:'bold',
        marginTop:Dim.height/100*1,
        textAlign:'center'
    },
    PaymentS_TextBig:{
        fontSize:Dim.width > 650 ? Dim.width/380*18 : Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold',
        alignSelf: "center",
    },
    PaymentS_image:{
        height: Dim.height * 0.15,
        width: Dim.height * 0.15,
        resizeMode: "contain",
        margin: 5
    },
    PaymentS_icon:{
        height:Dim.height/100*3,
        width:Dim.height/100*2,
        margin:Dim.height/100*2
    },
    PaymemtS_button:{
        // flex:0.7,
        width:'85%',
        marginVertical:Dim.height/100*2,
    },
    PaymemtF_button:{
        width:'85%',
        marginTop:Dim.height/100*3.5,
        marginBottom:Dim.height/100*5,
        padding: 3,height: '8%',
        borderRadius: 30 ,
        backgroundColor:'#65B4E3',
        alignItems:'center',
        justifyContent:'center' ,
    },
    Privilege_box:{
        // width:Dim.width/100*80,
        // height:Dim.height> 800 ? Dim.height/100*45 : Dim.height/100*50,
        // backgroundColor:'red'
    },
    Privilege_off:{
        height:Dim.height/100*13,
        width:Dim.width/100*80,
        marginTop:Dim.height/100,
        marginBottom:Dim.height/100,

    },
    Privilege_Barcode:{
        height:Dim.height/100*8,
        width:Dim.width/100*60,
        marginTop:Dim.height>800 ? Dim.height * 0.03 : Dim.height * 0.035,
    },
    Privilege_textoff:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        marginBottom:Dim.height/100*1,
    },
    Privilege_button:{
        width:'50%',
        padding: 0,
        height:'20%',
        borderRadius: Dim.height ,
        alignItems:'center',
        justifyContent:'center' ,
        borderWidth:1,
        borderColor:'#43B7E8',
    },
    Privilege_buttonText:{
        fontSize:Dim.width/100*4,
        fontFamily:SUB_FONT,
        color:'#43B7E8',
        fontWeight:'600'
    },
    Privilege_DoneButton:{
        // width:'85%',
        marginTop:Dim.height * 0.02 ,
        // marginBottom:Dim.height>800 ? Dim.height/100*5 : Dim.height/100*3 ,
        padding: 3,
        height: Dim.height * 0.07,
        borderRadius: 30 ,
        backgroundColor:'#65B4E3',
        alignItems:'center',
        justifyContent:'center' ,
    },
    Privilege_modalContainer:{
        flex:1,
        backgroundColor:'#000000AA',
        borderWidth: 1,
    },
    Privilege_modaloff:{
        height:Dim.height/100*21,
        width: "100%",
        resizeMode: "cover",
    },
    Privilege_modalBox:{
        height:'85%',
        marginTop:Dim.height/100*2,
        backgroundColor:'white',
        borderRadius: 20,
    },
    Privilege_modalClose:{
        height:Dim.width/100*5,
        width:Dim.width/100*5,
        position: "absolute",
    },
    textButton:{
        color:'white',
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'500'
    },
    textButtonScan:{
        color:'white',
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*14,
    },
    textButtonEN:{
        color:'white',
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'500'
    },
    textButtonTH:{
        color:'white',
        fontFamily:MAIN_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'500'
    },
    Privilege_modalTextH2sub:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        paddingBottom:Dim.width/100*3,
    },
    Privilege_modalTextH2:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#43B7E8',
        paddingBottom:Dim.width/100*5,
    },
    Privilege_modalTextH1:{
        fontSize:Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'#000000',
    },
    Privilege_modalTextNormal:{
        fontSize:Dim.width/380*15,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        paddingRight:Dim.width/100*5,
        zIndex: -1
    },
    Privilege_modalScrollContainer:{
        flex:1,
        // padding:Dim.width/100*8,
        // paddingTop:Dim.width/100*3,
        // paddingRight:Dim.width/100*2,
        zIndex:1
    },
    Header_AOT_icon_container:{
        height:Dim.height/100*4,
        width:Dim.width/100*30,
        marginLeft:Dim.width/100*5,
        flexDirection: 'row',
        alignContent:'flex-end'
    },
    Header_AOT_icon:{
        height:Dim.width * 0.1,
        width:Dim.width * 0.1,
        resizeMode: "contain"
    },
    Header_history_Add:{
        color:'#65B4E3',
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'600'
    },
    Header_AOT_iconRight:{
        height:Dim.height/100*3,
        width:Dim.height/100*3,

    },
    Header_AOT_iconRight1111:{
        height:Dim.height/100*3,
        width:Dim.height/100*3,
        backgroundColor:'red'
    },
    Header_RightP:{
        paddingRight:Dim.width/100*3,
        flex:1
    },
    Header_LeftP:{
        paddingLeft:Dim.width/100*3,
        flex:1
    },
    Header_AOT_text:{
        fontSize:Dim.height/100*2,
        fontFamily:SUB_FONT,
        color:'#354A9B',
        fontWeight:'bold',
        paddingTop:Dim.width/100*1,
        paddingLeft:Dim.width/100*2
    },
    Header_TitleText:{
        fontSize:Dim.height/100*2,
        width:Dim.width/100*44,
        fontFamily:SUB_FONT,
        color:'#354A9B',
        fontWeight:'bold',
        textAlign: 'center'
    },
    Header_textdone:{
        fontSize:Dim.height/100*2,
        fontFamily:SUB_FONT,
        color:'#65B4E3',
        fontWeight:'bold',
    },
    reservation_textModal:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'300'
    },
    reservation_textH1:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        width:Dim.height/100*7,
    },
    reservation_text3:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
    },
    reservation_textH2:{
        fontSize:Dim.width/380*22,
        width:Dim.width/100*65,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    reservation_textH2_model:{
        fontSize:Dim.width/380*22,
        // width:Dim.width/100*65,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    reservation_textTime:{
        fontSize: Dim.height > 800 ? Dim.width/380*56 : Dim.width/380*40,
        fontFamily:SUB_FONT,
        fontWeight:'bold'
    },
    reservation_arrow:{
        width:Dim.height/100*5.5,
    },
    reservation_detailbotton:{
        width:'50%',
        padding: 2,
        // height:'50%',
        borderRadius: 30 ,
        alignItems:'center',
        justifyContent:'center' ,
    },
    reservation_liner:{
        borderBottomColor: '#C4C4C6',
        borderBottomWidth: 1,
        paddingTop:Dim.height/100*4,
    },
    reservation_liner2:{
        borderBottomColor: '#C4C4C6',
        borderBottomWidth: 1,
        paddingTop:Dim.height/100*2,
    },
    reservation_detail:{
        // height:Dim.height>800 ? '35%' : '30%',
        borderRadius: Dim.height>800 ? 30 : 20,
        borderWidth:1,
        borderColor:'black',
        marginTop:Dim.height/100*2,
    },
    reservation_main_box:{
        // flex:1,
        // paddingTop:Dim.height>800 ? '10%' : '5%'
        padding: 10,
    },
    reservation_buttonText:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#43B7E8',
    },
    reservation_modalTextH1:{
        fontSize:Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'#000000',
    },
    reservation_modalTextH2:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
    },
    reservation_modalTextH1big:{
        fontSize:Dim.width/380*26,
        fontFamily:SUB_FONT,
        color:'#000000',
        fontWeight:'bold'
    },
    reservation_modalTextWhite:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
    },
    reservation_modalTextWhite16SB:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        fontWeight:'400'
    },
    reservation_modalText:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
    },
    reservation_modalTextBold:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    reservation_modalTextBold18:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    reservation_modalliner:{
        borderBottomColor: '#C4C4C6',
        borderBottomWidth: 1,
        marginTop:Dim.height * 0.03,
    },
    reservation_modalScrollContainer:{
        flex:1,
        height:Dim.height*1.5,
        padding:Dim.width/100*8,
        zIndex:1
    },
    reservation_modalDetail:{
        // height:Dim.height/100*60,
        marginTop:Dim.width/100*4,
    },
    reservation_modalDetailSub1:{
        height:Dim.height/100*10,
        justifyContent:'center',
        alignItems:'center'
    },
    reservation_modalDetailSub2:{
        // height:Dim.height/100*10,
        // backgroundColor:'blue',
    },
    reservation_modalDetailSub2line:{
        // height:Dim.height/100*3,
        backgroundColor:'white',
        justifyContent:'space-between',
        flexDirection:'row'
    },
    reservation_modalDetailSub2line2:{
        // height:Dim.height/100*4,
        backgroundColor:'white',
        justifyContent:'space-between',
        flexDirection:'row'
    },
    reservation_modalTextS26:{
        fontSize:Dim.width/380*28,
        fontFamily:SUB_FONT,
        color:'#000000',
        fontWeight:'bold'
    },
    reservation_modalDetail2:{
        // height:Dim.height/100*14,
        // marginTop:Dim.height/100*2,
        // backgroundColor:'red'
    },
    reservation_modalDetail2sub1:{
        // height:Dim.height/100*5,
        flexDirection:'row',
        // alignItems:'flex-end'
    },
    reservation_DT:{
        // height:Dim.height/100*15,
        borderRadius: 30,
        borderWidth:1,
        borderColor:'black',
        // marginTop:Dim.height/100*12,
    },
    reservation_modalConfirmBtn:{
        height:Dim.height/100*13,
        alignItems:'center',
        justifyContent:'center' ,
    },
    reservation_DoneButton:{
        width:'85%',
        marginTop:Dim.height/100*6,
        marginBottom:Dim.height/100*5,
        padding: 3,
        height: Dim.height/100*5,
        borderRadius: 30 ,
        backgroundColor:'#65B4E3',
        alignItems:'center',
        justifyContent:'center' ,
    },
    reservationSS_TextBig:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'400',
        marginBottom:Dim.height/100*1.5,
    },
    reservationSS_TextPaymentID:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        textAlign:'center'
    },
    history_footerContainer:{
        height:Dim.height/100*30,
        paddingBottom:Dim.height/100*2.7,
    },
    history_footertextAL:{
        fontSize:Dim.width/100*4,
        fontFamily:SUB_FONT,
        color:'#65B4E3',
        fontWeight:'bold',
        marginLeft:Dim.height/100*2.3
    },
    history_footerAIR:{
        fontSize:Dim.width/380*12,
        fontFamily:SUB_FONT,
        color:'#65B4E3',
        marginLeft:Dim.height/100*2.3,
        marginBottom:Dim.height/100*6.3
    },
    history_ads_banner:{
        height:  Dim.width > 600 ? Dim.width/100*23 : Dim.width/100*30,
        width:  Dim.width > 800 ? Dim.width/100*95 : Dim.width/100*90,
        backgroundColor:'blue'
    },
    hty0:{
        // flex:0.9
    },
    hty1:{
        // flex:0.1,
        alignItems:'center',
        justifyContent:'center'
    },
    hty2:{
        // height:Dim.height * 0.07,
        flexDirection:'row',
        marginTop:5,
        justifyContent:'space-between'
    },
    htyV2:{
        // height:Dim.height/100*7,
        marginTop:2,
        justifyContent:'space-between',
    },
    hty3:{
        // height:Dim.height/100*4,
        width:'30%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
    },
    htyImg1:{
        height:Dim.height/100*3.5,
        width:Dim.width/100*3.5,
    },
    historyMDT_container:{
        flex: 1,
        padding: Dim.width * 0.05,
        // height: Dim.height,
        // width: Dim.width,
        alignItems:'center',
    },
    history_ContainerOFcalendarContainer :{
        height: Dim.height/100*48,
        width: Dim.width/100*90,
        backgroundColor: 'white',
        marginTop:Dim.height/100*3,
        alignItems:'center',
        borderRadius:30

    },
    history_calendarContainer:{
        height: Dim.height/100*28,
        width: Dim.width/100*78,
    },
    history_MoreDTdropdown:{
        // height: Dim.height/100*13,
        width: Dim.width/100*90,
        // backgroundColor:'red'
    },
    history_calendarbtn:{
        width:Dim.width/100*65,
        height:Dim.height/100*6,
        borderRadius: 30 ,
        alignItems:'center',
        justifyContent:'center' ,
        backgroundColor:'#65B4E3',
        marginTop: 10,
    },
    history_calendarbtnText:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'white',
        fontWeight:'600'
    },
    history_detailBox:{
        flex:0.75,
        backgroundColor:'white',
        borderRadius: 20,
        marginTop:Dim.height/100*3,
        padding:Dim.height/100*2,
        paddingTop:Dim.height/100*3
    },
    history_detailBoxV:{
        flex:0.9,
        backgroundColor:'white',
        borderRadius: 20,
        marginTop:Dim.height/100*3,
        padding:Dim.height/100*2,
        paddingTop:Dim.height/100*3
    },
    history_detailText:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        fontWeight:'700',
    },
    history_detailText1:{
        fontSize:Dim.width/380*14,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'600',
    },
    history_detailText2:{
        fontSize:Dim.width/380*14,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        fontWeight:'600',
    },
    history_detailText3:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'700',
    },
    history_detailTextV3:{
        fontSize:Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'black',
        fontWeight:'bold'
    },
    history_detailText4:{
        fontSize:18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    history_detailText5:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#000000',
        fontWeight:'bold'
    },
    history_detailText6:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        fontWeight:'bold'
    },
    history_detailbox1:{
        flexDirection:'row',
        width:"100%",
        height:Dim.height/100*3,
        justifyContent:'space-between'
    },
    history_detailbox2:{
        flexDirection:'row',
        width:"50%",
        height:"100%",
        justifyContent:'space-between'
    },
    history_detailbox3:{
        width:"100%",
        height:Dim.height/100*25,
    },
    history_detailbox4:{
        flexDirection:'row',
        width:"100%",
        height:Dim.height/100*5,
        justifyContent:'space-between',
        alignItems:'flex-end',
        marginTop:Dim.height/100*2,
    },
    history_detailboxV4:{
        flexDirection:'row',
        width:"100%",
        height:Dim.height/100*4,
        justifyContent:'space-between',
        alignItems:'flex-end',

    },
    history_detailbox5:{
        flexDirection:'row',
        width:"100%",
        height:Dim.height/100*3,
        backgroundColor:'white',
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    history_detailboxV5:{
        flexDirection:'row',
        width:"100%",
        height:Dim.height/100*3,
        backgroundColor:'white',
        justifyContent:'space-between',
        alignItems:'flex-start',
    },
    history_detailboxV6:{
        height:Dim.height/100*8,
        borderRadius: Dim.width/100*2,
        borderWidth:1,
        borderColor:'black',
        marginTop:Dim.height/100*3,
        paddingLeft:Dim.width/100*3,
        paddingRight:Dim.width/100*3,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    history_detailTextV1:{
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'400'
    },
    history_detailTextV2:{
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*22,
        fontWeight:'bold'
    },
    history_detailbox6:{
        width:"100%",
        height:Dim.height/100*8,
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    history_detailbox7:{
        flexDirection:'row',
        width:Dim.width/100*65,
        height:Dim.height/100*8,
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    history_detailbox8:{
        width:Dim.width/100*40,
        height:Dim.height/100*10,
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    history_detailbox9:{
        width:Dim.width/100*35,
        height:Dim.height/100*10,
        justifyContent:'flex-end',
        alignItems:'flex-end',
    },
    history_detail_liner:{
        borderBottomColor: '#C4C4C6',
        borderBottomWidth: 1,
        paddingTop:Dim.height/100*2,
    },
    forgotText:{
        fontSize:Dim.width/380*22,
        fontWeight:'400',
        fontFamily:SUB_FONT,
        paddingBottom:Dim.height/100*2,
    },
    forgetPasswordbox:{
        height:Dim.height/2 ,
        alignItems:'center',
        justifyContent:'center',
        // backgroundColor:'red'
    },
    forgot_SSText2:{
        fontSize:Dim.width/380*16,
        fontWeight:'400',
        fontFamily:SUB_FONT,
        color:'#4A4A4A',
        flexDirection: 'row',textAlign:'center',
        paddingBottom:Dim.height/100*2
    },
    forgot_SSText3:{
        fontSize:Dim.width/380*16,
        fontWeight:'400',
        fontFamily:SUB_FONT,
        color:'#43B7E8'
    },
    forgot_SuccessBox:{
        flex: Dim.height>900 ? 0.8 : 0.75,
        backgroundColor:'white',
        borderRadius: 20,
        margin:Dim.height/100*3,
        padding:Dim.height/100*2,
        paddingTop: Dim.height>800 ? Dim.height/200 : Dim.height/100*3,
        justifyContent:'center',
        alignItems:'center'
    },
    PasswordForgotSS:{
        height: Dim.height > 900 ? Dim.height/100*24 : Dim.height / 100 * 26,
        width:  Dim.width > 600 ? Dim.width/100*30 : Dim.width / 100 * 40,
        margin: Dim.height/100 * 4,
    },
    ProfileBox:{
        width:Dim.width>600 ? Dim.width/100*20 : Dim.width/100*30,
        height:Dim.width>600 ? Dim.width/100*20 : Dim.width/100*30,
        borderRadius: Dim.width/100*30/2,
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        marginTop:Dim.height/100*2
    },
    ProfileBox2:{
        width: Dim.width>600 ? Dim.width/100*20 : Dim.width/100*30,
        height:Dim.width>600 ? Dim.width/100*20 : Dim.width/100*30,
        borderRadius: Dim.width/100*30/2,
        justifyContent:'flex-end',
        alignItems:'flex-end',
        position:'absolute',
        marginTop:Dim.height/100*2
    },
    profileBigbox:{
        marginTop:Dim.height/100*2,
        marginBottom:Dim.height/100*2,
        width:Dim.width,
        height:Dim.height/100*36,
        justifyContent:'space-between'
    },
    profileBigboxStaff:{
        marginTop:Dim.height/100*4,
        marginBottom:Dim.height/100*4,
        width:Dim.width,
        height:Dim.height/100*18,
        justifyContent:'space-between'
    },
    profileBox2:{
        backgroundColor:'white',
        width:Dim.width,
        height:Dim.height/100*8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center',
        paddingRight:Dim.width/100*5,
        paddingLeft:Dim.width/100*5,
    },
    profileMiniBox:{
        justifyContent:'space-around',
        flexDirection:'row',
        alignItems:'center',
    },
    profileTextMenu:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        marginLeft:Dim.width/100*4
    },
    profileTextMenu2:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        marginRight:Dim.width/100*4
    },
    ProfileImage:{
        width:Dim.width>600?Dim.width/100*17 : Dim.width/100*26,
        height:Dim.width>600?Dim.width/100*17 :Dim.width/100*26,
        borderRadius: Dim.width/100*28/2,
        backgroundColor:'white',
    },
    ProfileSubImage:{
        width: Dim.width>600 ? Dim.width/100*7 : Dim.width/100*9,
        height:Dim.width>600 ? Dim.width/100*7 : Dim.width/100*9,
        borderRadius: Dim.width,
        backgroundColor:'white',
        position:'absolute'
    },
    history_logoutbtnRed:{
        width:Dim.width/100*70,
        height:Dim.height/100*5,
        borderRadius: 30 ,
        alignItems:'center',
        justifyContent:'center' ,
        backgroundColor:'red',
    },
    ProfileIconbox:{
        width:Dim.width/100*8,
        height:Dim.height/100*4,
        alignItems:'center',
        justifyContent:'center' ,
    },
    ProfileIcon:{
        width:Dim.height/100*3,
        height:Dim.height/100*3.5,
        color:'red'
    },
    ProfileIcon2:{
        width:Dim.height/100*3,
        height:Dim.height/100*4,
        color:'red'
    },
    ProfileIcon3:{
        width:Dim.height/100*3,
        height:Dim.height/100*3,
        color:'red'
    },
    profileTextLanguage:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        color:'#000000',
        fontWeight:'400',
        marginRight:Dim.width/100*4
    },
    profileLanguageBox:{
        width:Dim.width/100*7,
        height:Dim.width/100*7
    },
    arrow:{
        transform: [{ rotate: '180deg'}],
        fontSize:Dim.width/100*7,
        color:'#C4C4C6'
    },
    profileName:{
        fontSize:Dim.width/380*22,
        fontFamily:SUB_FONT,
        color:'#000000',
        paddingTop:Dim.height/100*2
    },
    profileRole:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        fontWeight:'400'
    },
    profile_branchNshop:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#65B4E3',
    },
    profile_ChangeText:{
        fontSize:Dim.width/100*4,
        fontWeight:'400',
        fontFamily:SUB_FONT,
        margin:Dim.width/100*4,
    },
    Text_Input:{
        width:Dim.width/100*75,
        height:Dim.height/100*7,
        marginBottom:Dim.height/100*4,
        backgroundColor:'white',
        borderRadius:Dim.width,
    } ,
    Text_InputError:{
        width:Dim.width/100*75,
        height:Dim.height/100*7,
        marginBottom:Dim.height/100*4,
        borderRadius:Dim.width,
        borderWidth:1,
        borderColor:'red',
    } ,
    ChangePassword_Text:{
        fontSize:Dim.width/380*16,
        fontFamily:SUB_FONT,
        color:'#9C9C9C',
        paddingBottom:Dim.height/100*3
    },
    ChangePassword_Box1:{
        height:Dim.width>650?Dim.height/2.3:Dim.height/2.25,
        alignItems:'center',
        justifyContent:'center',
        paddingTop:Dim.width>650? Dim.height/17 : Dim.height/100*4,
        // backgroundColor:'red'
    },
    ChangePassword_PlaceHolderText:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        paddingLeft:Dim.width/18
    },
    ChangePassword_AlertText:{
        fontSize:Dim.width/380*12,
        fontFamily:SUB_FONT,
        paddingLeft:Dim.width/18,
        color:'red'
    },
    Password_InputBox:{
        width:Dim.width/100*75,
        height:Dim.height/100*7,
        backgroundColor:'white',
        marginBottom:Dim.height/100*4,
        borderRadius:Dim.width
    },
    ChangePassword_AlertTextContainer:{
        width:Dim.width/1.2,
        paddingBottom:Dim.height/60
    },
    BranchSelectFooter:{
        flex:0.8,
        flexDirection:'column-reverse',
        paddingBottom:Dim.height/100*1.5
    },
    selectBranchBox:{
        width:'100%',
        height:Dim.height/100*8,
        backgroundColor:'white',
        borderRadius:Dim.height/100*1.5 ,
        justifyContent:'center' ,
    },
    selectBranchBox2:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:'100%',
        paddingRight:Dim.width/100*4,
        paddingLeft:Dim.width/100*4,
        alignItems:'center'
    },
    selectBranchBoxContainer:{
        width:'100%',
        height:Dim.height/100*54,
        justifyContent:'space-around',
    },
    forgot_BranchBoxText:{
        fontSize:Dim.width/380*16,
        fontWeight:'400',
        fontFamily:SUB_FONT,
        color:'#43B7E8',
        width:Dim.width/100*50,
    },
    selectBottomView:{
        width:'100%',
        height:Dim.height/100*12,
        justifyContent:'center',
        alignItems:'center'
    },
    selectIconBox:{
        width:Dim.width/100*8,
        height:Dim.height/100*4,
        justifyContent:'center',
        alignItems:'center',
    },
    branchDetail_Text1:{
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*22,
        fontWeight:'400',
        color:'#65B4E3'
    },
    branchDetail_Text2:{
        fontFamily:SUB_FONT,
        fontSize:Dim.width/380*18,
        fontWeight:'400',
        color:'#65B4E3'
    },
    branchDetail_Box:{
        width:'100%',
        height:Dim.height/100*7,
        marginTop:Dim.height/100*3,
    },
    workHistoryBox:{
        height: Dim.height/100*10,
        width: Dim.width,
        backgroundColor: "#F2F8FF",
        alignItems: "center",
        justifyContent: "center",
    },
    workHistoryWhiteSpace:{
        height:Dim.height/100*1,
        width:Dim.width,
        backgroundColor:'white'
    },
    workHistoryBox2:{
        height: Dim.height/100*4,
        width: Dim.width/100*80,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection:'row',
    },
    workHistoryTextName:{
        fontSize:Dim.width/380*16,
        fontWeight:'bold',
        fontFamily:SUB_FONT,
    },
    workHistoryTextStatus:{
        fontSize:Dim.width/100*4,
        fontWeight:'500',
        fontFamily:SUB_FONT,
    },
    workHistoryText2:{
        fontSize: Dim.width>650 ? Dim.width/380*10 : Dim.width/380*12,
        fontFamily:SUB_FONT,
        color:"#9C9C9C"
    },
    home_icon:{
        width:Dim.width>650 ? Dim.width/100*12 : Dim.width/100*17,
        height:Dim.width>650 ? Dim.width/100*12 : Dim.width/100*17
    },
    CBranchBox:{
        width:Dim.width/100*85.5,
        height:Dim.height/100*14,
        backgroundColor:'#FFFFFF',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderRadius: 30 ,
        marginBottom:Dim.height/100*2
    },
    CBranchBox1:{
        width:Dim.width/100*70 ,
        height:Dim.height/100*14,
        marginLeft:Dim.width/100*15,
    },
    CBranchBox11:{
        width:Dim.width/100*65,
        height:Dim.height/100*5,
        marginTop:Dim.height/100*3,
    },
    CBranchBox2:{
        width:Dim.width/100*20,
        height:Dim.height/100*14,
    },
    CBtext:{
        fontSize:Dim.width/380*18,
        fontFamily:SUB_FONT,
        fontWeight:'500',
        color:"#65B4E3"
    },
    CBcheckbox:{
        width:Dim.width/100*12,
        height:Dim.height/100*6,
        marginTop:Dim.height/100*2.5
    },
    ProfileFooter:{
        flex:0.1,
        flexDirection:'row',
    },
    ProfileFooterH:{
        flex:0.1,
        flexDirection:'row',
        paddingLeft:Dim.width/100*5,
        paddingBottom:Dim.height/100*2
    },
    ProfileFooter2:{
        flex:0.8,
        flexDirection:'column',
        paddingTop:Dim.height/100*2.2
    },
    item: {
        backgroundColor: "#F2F8FF",
        padding:Dim.height/100*1.5,
        marginVertical: Dim.width/100*2,
        flexDirection:'row'
    },
    itemVoucher: {
        backgroundColor: "#F2F8FF",
        // paddingLeft:Dim.height/100*1.5 ,
        // paddingRight:Dim.height/100*1.5 ,
        // padding: Dim.height/100*1,
        marginVertical: Dim.width/100*2,
        flexDirection:'row'
    },
    htytextheader: {
        marginTop:Dim.height/100*4,
        marginLeft:Dim.width/100*2,
        fontSize:Dim.width/100*4,
        fontFamily: SUB_FONT,
        backgroundColor: "white"
    },
    hty2text_title1: {
        fontSize: 16,
        fontFamily: SUB_FONT,
        fontWeight:'bold',
        color:'#4A4A4A',
        // width:Dim.width/2.1,
    },
    hty2text_title2: {
        fontSize:Dim.width/380*25,
        fontFamily: SUB_FONT,
        fontWeight:'bold'
    },
    hty2text_titleV2: {
        fontSize:15,
        fontFamily: SUB_FONT,
        fontWeight:'bold'
    },
    hty2text_title3: {
        fontSize:Dim.width/100*4,
        fontFamily: SUB_FONT,
        fontWeight:'bold',
        color:'#9C9C9C'
    },
    hty2text_title4: {
        fontSize:Dim.width/380*18,
        fontFamily: SUB_FONT,
        fontWeight:'500',
        color:'#9C9C9C'
    },
    DropdownPopup: {
        backgroundColor:'white',
        width:Dim.width/8,
        height:Dim.width/8,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:Dim.width,
        borderColor:'#43B7E8',
        borderWidth:1,
    },
    DropdownPopupDropdown: {
        width:Dim.width/100*30,
        height:Dim.height>800 ? Dim.height/100*33 : Dim.height/100*38,
        position:'absolute',
        marginLeft:Dim.width/1.55,
        marginTop:Dim.height/100*30,
        justifyContent:'space-between',
        alignItems:'flex-end',
    },
    LogoutButton: {
        width: Dim.width/100*7,
        height: Dim.width/100*7
    },
    change_password_container: {
        flex: 1,
        padding: Dim.width * 0.05,
        justifyContent: "center",
        alignItems: "center",
    },
})
