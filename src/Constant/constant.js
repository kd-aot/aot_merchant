export const API_IP_UAT = 'URL UAT'
export const API_IP = 'https://aot-service.staging.kdlab.dev/'
export const CONFIG = {
    headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
    },
    timeout: 60000
}
