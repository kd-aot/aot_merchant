import { 
    BRANCH_LIST,
    BRANCH_LIST_ERROR, 
    BRANCH_DETAIL,
    BRANCH_DETAIL_ERROR
} from '../Actions/type'
const INITIAL_STATE = {
   branch_list:null,
   branch_detail:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case BRANCH_LIST:
        return {
          ...state,
          branch_list: action.payload,
          error: null,
        }
      case BRANCH_LIST_ERROR:
        return {
          ...state,
          branch_list: null,
          error: null,
        }
        case BRANCH_DETAIL:
            return {
                ...state,
                branch_detail: action.payload,
                error: null,
            }
        case BRANCH_DETAIL_ERROR:
        return {
            ...state,
            branch_detail: null,
            error: null,
        }
      default:
        return state
    }
}