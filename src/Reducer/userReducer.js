import {
    LOGIN_STATUS_FAIL,
    USER,
    USER_ERROR,
} from '../Actions/type';
const INITIAL_STATE = {
   userinfo:null,
   error:null,
    loginFail: null,
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case USER:
          console.log('userreducer' ,action.payload)
        return {
          ...state,
          userinfo: action.payload,
          error: null,
        }
      case USER_ERROR:
        return {
          ...state,
          userinfo: null,
          error: null,
        }
        case LOGIN_STATUS_FAIL:
            return {
                ...state,
                loginFail: action.payload.time
            }
      default:
        return state
    }
}
