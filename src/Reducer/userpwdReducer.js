import { 
    OLDPWD,
    OLDPWD_ERROR 
} from '../Actions/type'
const INITIAL_STATE = {
   oldpwd:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case OLDPWD:
          console.log('PWDreducer' ,action.payload)
        return {
          ...state,
          oldpwd: action.payload,
          error: null,
        }
      case OLDPWD_ERROR:
        return {
          ...state,
          oldpwd: null,
          error: null,
        }
      default:
        return state
    }
}