import { combineReducers } from "redux"
import checkReducer from './checkReducer'
import testReducer from './testReducer'
import userReducer from './userReducer'
import oldpwdReducer  from './userpwdReducer'
import branchReducer from './branchReducer'
import historyReducer from './historyReducer'
import voucherReducer from './voucherReducer'

import paymentDetailReducer from './paymentDetailReducer.js'
import paymentReducer from './paymentReducer'
import orderReducer from './orderReducer'


export default combineReducers({
    check: checkReducer,
    test: testReducer,
    user : userReducer,
    oldpwd : oldpwdReducer,
    voucher : voucherReducer,
    history : historyReducer,
    branch : branchReducer ,

    pay : paymentReducer,
    paydetail : paymentDetailReducer,
    order : orderReducer
  })