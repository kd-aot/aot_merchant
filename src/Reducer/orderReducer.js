import {
    ORDER_NUMBER ,
    ORDER_ERROR ,
} from '../Actions/type'

const INITIAL_STATE = {
    order_number:null,
    order_random:null,
    error:null
 }
 export default(state = INITIAL_STATE, action) => {
     switch (action.type) {
       case ORDER_NUMBER:
         return {
           ...state,
           order_number: action.payload,
           order_random:Math.floor((Math.random() * 1000) + 1),
           error: null,
         }
       case ORDER_ERROR:
         return {
           ...state,
           order_number: null,
           order_random:null,
           error: null,
         }
       default:
         return state
     }
 }