import {
    CHECK_OPEN_CAMERA,
    PAYMENT,
    PAYMENT_ERROR,
} from '../Actions/type';

const INITIAL_STATE = {
    payment:null,
    error:null,
    openCamera: null,
    openCameraTime: null,
 }
 export default(state = INITIAL_STATE, action) => {
     switch (action.type) {
       case PAYMENT:
         return {
           ...state,
           payment: action.payload,
           error: null,
         }
       case PAYMENT_ERROR:
         return {
           ...state,
           payment: null,
           error: null,
         }
         case CHECK_OPEN_CAMERA:
             return {
                 ...state,
                 openCamera: action.payload.data,
                 openCameraTime: action.payload.time,
             }
       default:
         return state
     }
 }
