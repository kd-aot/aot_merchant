import { 
    TEST,
    TEST_ERROR 
} from '../Actions/type'
const INITIAL_STATE = {
   test:null,
   error:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case TEST:
        return {
          ...state,
          test: action.payload,
          error: null,
        }
      case TEST_ERROR:
        return {
          ...state,
          test: null,
          error: null,
        }
      default:
        return state
    }
}