import {
    PAYMENT_DETAIL ,
    PAYMENT_DETAIL_ERROR ,
} from '../Actions/type'

const INITIAL_STATE = {
    paymentDetail:null,
    error:null
 }
 export default(state = INITIAL_STATE, action) => {
     switch (action.type) {
       case PAYMENT_DETAIL:
         return {
           ...state,
           paymentDetail: action.payload,
           error: null,
         }
       case PAYMENT_DETAIL_ERROR:
         return {
           ...state,
           paymentDetail: null,
           error: null,
         }
       default:
         return state
     }
 }