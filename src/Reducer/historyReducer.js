import {
    HISTORY_DATE_TIME_SETTING,
    HISTORY_PAYMENT,
    HISTORY_PAYMENT_ERROR,
    HISTORY_VOUCHER,
    HISTORY_VOUCHER_ERROR,
} from '../Actions/type';

const INITIAL_STATE = {
    payment: null,
    voucher: null,
    payment_num: null,
    voucher_num: null,
    error: null,
    history: [],
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case HISTORY_PAYMENT:
            return {
                ...state,
                payment: action.payload,
                payment_num: Math.floor((Math.random() * 1000) + 1),
                error: null,
            };
        case HISTORY_PAYMENT_ERROR:
            return {
                ...state,
                payment: null,
                error: null,
            };
        case HISTORY_VOUCHER:
            return {
                ...state,
                voucher: action.payload,
                voucher_num: Math.floor((Math.random() * 1000) + 1),
                error: null,
            };
        case HISTORY_VOUCHER_ERROR:
            return {
                ...state,
                voucher: null,
                error: null,
            };
        case HISTORY_DATE_TIME_SETTING:
            return {
                ...state,
                dateTime: action.payload.data,
                dateTimeCheck: action.payload.time,
            };
        default:
            return state;
    }
}
