import { 
    CHECKTYPEIPHONE,
    CHECKTYPEIPHONE_ERROR ,
    TOKEN
} from '../Actions/type'
const INITIAL_STATE = {
   checkIphone:null,
   error:null,
   checkToken:null
}
export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
      case CHECKTYPEIPHONE:
        return {
          ...state,
          checkIphone: action.payload,
          error: null,
        }
      case CHECKTYPEIPHONE_ERROR:
        return {
          ...state,
          checkIphone: null,
          error: null,
        }
      default:
        return state
    }
}
