import {
    PRIVILEGE,
    RESERVATION, USEVOUCHER,
    VOUCHER_ERROR,
} from '../Actions/type';

const INITIAL_STATE = {
    voucher:null,
    error:null,
    usevoucher: null,
 }
 export default(state = INITIAL_STATE, action) => {
     switch (action.type) {
       case PRIVILEGE:
           console.log('VOUCHER' ,action.payload)
         return {
           ...state,
           voucher: action.payload,
           error: null,
         }
        case RESERVATION:
            console.log('RESERVATION' ,action.payload)
          return {
            ...state,
            voucher: action.payload,
            error: null,
          }
       case VOUCHER_ERROR:
         return {
           ...state,
           voucher: null,
           error: null,
         }
         case USEVOUCHER:
             return {
                 ...state,
                 usevoucher: action.payload,
                 error: null,
             }
       default:
         return state
     }
 }
